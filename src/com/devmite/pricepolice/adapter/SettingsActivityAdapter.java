package com.devmite.pricepolice.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.view.MySwitch;

public class SettingsActivityAdapter extends BaseAdapter {

	private List<String> list;
	private LayoutInflater inflater;
	
	public SettingsActivityAdapter(Context context, List<String> mListe) {
		inflater = LayoutInflater.from(context);
		this.list = mListe;
	}

	public List<String> getList() {
		return list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
	    int type = getItemViewType(position);
	    if (v == null) {
	        // Inflate the layout according to the view type
	        if (type == 0) {
	            // Inflate the layout with image
	            v = inflater.inflate(R.layout.two_lines_layout, parent, false);
	        }
	        else {
	            v = inflater.inflate(R.layout.two_lines_with_toggle, parent, false);
	        }
	    }
	    //
	    String str = list.get(position);
	 
	    TextView title = (TextView) v.findViewById(R.id.title);
	    TextView subtitle = (TextView) v.findViewById(R.id.subtitle);
	 
	    if (type == 1) {
	        MySwitch toggle = (MySwitch) v.findViewById(R.id.toggle_notif);
	    }
	 
		// surname.setText(c.surname);
		// name.setText(c.name);
		// email.setText(c.email);
	 
	    return v;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

}
