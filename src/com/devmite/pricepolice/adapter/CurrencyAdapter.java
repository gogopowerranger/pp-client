package com.devmite.pricepolice.adapter;

import com.devmite.pricepolice.app.App;

import android.content.Context;
import android.widget.ArrayAdapter;

public class CurrencyAdapter extends ArrayAdapter<String> {
	private final String[] currencyCodes;

	public CurrencyAdapter(Context context, int resource, String[] currencyCodes) {
		super(context, resource, currencyCodes);
		this.currencyCodes = currencyCodes;
	}

	public String[] getCurrencyCodes() {
		return currencyCodes;
	}

	public int findIndex(String currencyCode, int defaultValue) {
		for (int i = 0; i < currencyCodes.length; i++) {
			if (currencyCodes[i].equalsIgnoreCase(currencyCode)) {
				return i;
			}
		}
		return defaultValue;
	}

	public String getItem(int position) {
		String currencyCode = currencyCodes[position];
		return currencyCode.toUpperCase(App.getLocale());
	};
}