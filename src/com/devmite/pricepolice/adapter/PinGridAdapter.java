package com.devmite.pricepolice.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.util.ImageLoader;
import com.devmite.pricepolice.util.Utils;
import com.devmite.pricepolice.view.SquareImageView;

public class PinGridAdapter extends BaseAdapter {
	private List<PinResponse> items = new ArrayList<PinResponse>();

	private final Context context;
	private final LayoutInflater inflater;
	private final ImageLoader imageLoader;

	public PinGridAdapter(Context context, List<PinResponse> items) {
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.items = items;

		this.imageLoader = App.getImageLoader();
	}

	public PinGridAdapter(Context context) {
		this(context, new ArrayList<PinResponse>());
	}

	public void setItems(List<PinResponse> items) {
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int i) {
		return items.get(i);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void addItem(PinResponse item) {
		items.add(item);
	}

	public void clearItems() {
		items.clear();
	}

	/********* Create a holder Class to contain inflated xml file elements *********/
	public static class ViewHolder {

		public SquareImageView image;
		public TextView textDesc;
		public TextView textURL;
		public TextView textFollowers;
		public TextView textPrice;
		public TextView textPriceLatest;

	}

	public View getView(int i, View view, ViewGroup viewGroup) {
		View v = view;
		ViewHolder holder;

		if (view == null) {
			/****** Inflate tabitem.xml file for each row ( Defined below ) *******/
			v = inflater.inflate(R.layout.grid_item, viewGroup, false);

			// /****** View Holder Object to contain tabitem.xml file elements
			// ******/
			holder = new ViewHolder();
			holder.image = (SquareImageView) v.findViewById(R.id.image_pin);
			holder.textDesc = (TextView) v.findViewById(R.id.text_desc);
			holder.textFollowers = (TextView) v.findViewById(R.id.text_followers);
			holder.textPrice = (TextView) v.findViewById(R.id.text_price);
			holder.textPriceLatest = (TextView) v.findViewById(R.id.text_price_latest);
			//
			// /************ Set holder with LayoutInflater ************/
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		PinResponse item = items.get(i);

		holder.textDesc.setText(item.getLabel());

		BigDecimal price = new BigDecimal(item.getPrice());
		holder.textPrice.setVisibility(View.VISIBLE);
		holder.textPrice.setText(Utils.formatPrice(item.getCurrency(), price));

		if (!Utils.isBlank(item.getLatestPrice())) {
			BigDecimal latestPrice = new BigDecimal(item.getLatestPrice());

			holder.textPriceLatest.setText(Utils.formatPrice(item.getCurrency(), latestPrice));
			holder.textPriceLatest.setVisibility(View.VISIBLE);
			if (latestPrice.compareTo(price) != 0) {
				holder.textPrice.setPaintFlags(holder.textPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			} else {
				holder.textPrice.setVisibility(View.INVISIBLE);
			}
		}

		if (item.getStatistics() != null) {
			holder.textFollowers.setText(String.valueOf(item.getStatistics().getFollowers()));
		}
		ImageView image = holder.image;
		// DisplayImage function from ImageLoader Class
		imageLoader.displayImageProcessed(item.getImageUrl(), image, new ImageProcessor());
		// imageLoader.displayImage(item.getImageUrl(), image);
		return v;
	}

	public class ImageProcessor implements ImageLoader.Processor {
		@Override
		public Bitmap process(Bitmap bitmap) {
			int radius = context.getResources().getDimensionPixelSize(R.dimen.rounded_corner_radius);
//			int radius = 2 * Math.round(context.getResources().getDisplayMetrics().density);
			Log.i("ws", "radius: " + radius);
			Bitmap squareBitmap = square(bitmap);
			return getRoundCornerBitmap(squareBitmap, radius);
		}
	}

	public static Bitmap getRoundCornerBitmap(Bitmap bitmap, int radius) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		final RectF rectF = new RectF(0, 0, w, h);
		
		canvas.drawRoundRect(rectF, radius, radius, paint);

		/**
		 * here to define your corners, this is for left bottom and right bottom
		 * corners
		 */
		final Rect clipRect = new Rect(0, 0, w, h - radius);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
		canvas.drawRect(clipRect, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, null, rectF, paint);

		bitmap.recycle();

		return output;
	}

	public static Bitmap square(Bitmap srcBmp) {
		if (srcBmp.getWidth() >= srcBmp.getHeight()) {

			return Bitmap.createBitmap(
					srcBmp,
					srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
					0,
					srcBmp.getHeight(),
					srcBmp.getHeight()
					);

		} else {

			return Bitmap.createBitmap(
					srcBmp,
					0,
					srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
					srcBmp.getWidth(),
					srcBmp.getWidth()
					);
		}
	}

}
