package com.devmite.pricepolice.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.fragment.GuideFragment;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.util.Utils;

public class GuideActivity extends FragmentActivity {
	private ViewPager pager;
	private PagerAdapter pagerAdapter;
	private int activePosition = -1;
	private SparseArray<GuideFragment> guideFragments;

	private ViewConfiguration vc;
	private int swipeMinDistance = 120;
	private int swipeThresholdVelocity = 200;
	private int swipeMaxOffPath = 250;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);

		this.pager = (ViewPager) findViewById(R.id.pager);
		this.pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		this.pager.setAdapter(pagerAdapter);
		this.guideFragments = new SparseArray<GuideFragment>();

		if (activePosition != -1) {
			pager.setCurrentItem(activePosition);
		}

		this.vc = ViewConfiguration.get(this);
		float density = getResources().getDisplayMetrics().density;
		swipeMinDistance = (int) (2 * density);
		swipeThresholdVelocity = (int) (16 * density);
		swipeMaxOffPath = (int) (400 * density);

		this.pager.setOnTouchListener(new OnSwipeTouchListener());
		Utils.saveBooleanPreferences(this, Config.SP_KEY_GUIDE_VIEWED, true);
	}

	@Override
	public void onBackPressed() {
		if (this.pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			this.pager.setCurrentItem(this.pager.getCurrentItem() - 1);
		}
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		/**
		 * Remove this if we don't want to peek into the next slide in the pager
		 */
		// @Override
		// public float getPageWidth(int position) {
		// return 0.96f;
		// }

		@Override
		public CharSequence getPageTitle(int position) {
			return String.valueOf(position);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			Log.i("ws", "Requesting page " + position);
			activePosition = position;
			GuideFragment fragment = guideFragments.get(position);
			if (fragment == null) {
				fragment = GuideFragment.newInstance(position);
				guideFragments.append(position, fragment);
			}

			return fragment;
		}

		@Override
		public int getCount() {
			return 5;
		}
	}

	class MyGestureDetector extends SimpleOnGestureListener {
		private boolean activated = false;

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			// Toast.makeText(GuideActivity.this, "Flinging at " +
			// activePosition + " gf null ? " + (lastGf == null),
			// Toast.LENGTH_SHORT)
			// .show();
			if (activePosition == 4) {
				if (!activated) {
					activated = true;
				} else
					try {
						if (Math.abs(e1.getY() - e2.getY()) > swipeMaxOffPath)
							return false;
						// right to left swipe
						if (e1.getX() - e2.getX() > swipeMinDistance && Math.abs(velocityX) > swipeThresholdVelocity) {
							Intent intent = new Intent();
							if (SessionManager.getInstance(GuideActivity.this).isUserLoggedIn()) {
								intent.setClass(GuideActivity.this, PrimaryActivity.class);
							} else {
								intent.setClass(GuideActivity.this, LoginActivity.class);
							}
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							GuideActivity.this.finish();
							return true;
						}
					} catch (Exception e) {
						// nothing
					}
			}
			return false;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}
	}

	class OnSwipeTouchListener implements OnTouchListener {
		private final GestureDetector gestureDetector = new GestureDetector(new MyGestureDetector());

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			return gestureDetector.onTouchEvent(event);
		}
	}
}
