//package com.devmite.pricepolice.activity;
//
//import java.net.MalformedURLException;
//import java.net.URL;
//
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.devmite.pricepolice.R;
//import com.devmite.pricepolice.app.App;
//import com.devmite.pricepolice.model.http.BaseRespone;
//import com.devmite.pricepolice.model.http.PinFollowRequest;
//import com.devmite.pricepolice.model.http.PinFollowResponse;
//import com.devmite.pricepolice.model.http.PinResponse;
//import com.devmite.pricepolice.util.ImageLoader;
//
//public class PinDetailActivity extends Activity implements OnClickListener {
//
//	private PinResponse pin;
//	private ImageLoader imageLoader;
//	private ImageView imagePin;
//	private Button followUnfollowButton;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_pin_detail);
//
//		Bundle args = getIntent().getExtras();
//		pin = (PinResponse) args.getParcelable("pin");
//		
//		Log.i("ws", pin.toString());
//
//		imagePin = (ImageView) findViewById(R.id.image_pin_detail);
//		imageLoader = ImageLoader.getInstance(this);
//		imageLoader.displayImage(pin.getImageUrl(), imagePin);
//
//		TextView title = (TextView) findViewById(R.id.title_detail);
//		title.setText(pin.getLabel());
//
//		TextView price = (TextView) findViewById(R.id.price_detail);
//		price.setText(pin.getCurrency() + pin.getPrice());
//
//		TextView site = (TextView) findViewById(R.id.site_detail);
//
//		URL aURL;
//		try {
//			aURL = new URL(pin.getPageUrl());
//			site.setText(aURL.getHost());
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		}
//
//		followUnfollowButton = (Button) findViewById(R.id.button_follow_unfollow);
//		followUnfollowButton.setOnClickListener(this);
//	}
//
//	@SuppressLint("ShowToast")
//	@Override
//	public void onClick(View v) {
//		if (v == followUnfollowButton) {
//			PinFollowRequest request = new PinFollowRequest();
//			request.setPinId(pin.getPinId());
//			App.getRestClient()
//					.getAPIService()
//					.follow(request,
//							new Callback<BaseRespone<PinFollowResponse>>() {
//								@Override
//								public void success(BaseRespone<PinFollowResponse> br, Response r) {
//									System.out.println(br.toString());
//									Toast.makeText(PinDetailActivity.this,
//											br.toString(), Toast.LENGTH_SHORT);
//								}
//
//								@Override
//								public void failure(RetrofitError e) {
//									Toast.makeText(PinDetailActivity.this,
//											e.toString(), Toast.LENGTH_SHORT);
//								}
//							});
//
//		}
//	}
//
//}
