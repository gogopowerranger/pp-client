//package com.devmite.pricepolice.activity;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.DialogInterface;
//import android.content.DialogInterface.OnCancelListener;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//
//import com.devmite.pricepolice.R;
//import com.devmite.pricepolice.helper.Config;
//import com.devmite.pricepolice.helper.http.HttpConnectionTask;
//import com.devmite.pricepolice.helper.http.IHttpResponseListener;
//
//public class UpdatePasswordActivity extends Activity implements
//		OnClickListener, IHttpResponseListener {
//
//	private Button updateButton;
//	private ProgressDialog progressDialog;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.activity_update_pw);
//
//		updateButton = (Button) findViewById(R.id.update_button);
//		updateButton.setOnClickListener(this);
//	}
//
//	@SuppressLint("NewApi")
//	@Override
//	public void onClick(View arg0) {
//		JSONObject jo = new JSONObject();
//		try {
//			jo.put("password", "password");
//
//			String url = "http://www.mocky.io/v2/54976c5131a09fa30bf9913e"; // no
//																			// change
//
//			final HttpConnectionTask httpConnectionTask = new HttpConnectionTask(
//					this, Config.API_UPDATE_PASSWORD, "GET");
//			httpConnectionTask.executeOnExecutor(
//					AsyncTask.THREAD_POOL_EXECUTOR, url);
//
//			progressDialog = new ProgressDialog(this);
//			progressDialog.setCancelable(true);
//			progressDialog.setCanceledOnTouchOutside(false);
//			progressDialog.setOnCancelListener(new OnCancelListener() {
//				@Override
//				public void onCancel(DialogInterface dialog) {
//					httpConnectionTask.cancel(true);
//				}
//			});
//			progressDialog.setMessage("please wait");
//			progressDialog.show();
//
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Override
//	public void resultSuccess(int type, String result) {
//		if (progressDialog != null) {
//			progressDialog.dismiss();
//		}
//
//		switch (type) {
//		case Config.API_UPDATE_PASSWORD: {
//			JSONObject jo;
//			try {
//				jo = new JSONObject(result);
//				String message = jo.getString("message");
//				String internalCode = jo.getString("internal_code");
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//		}
//		}
//	}
//
//	@Override
//	public void resultFailed(int type, String strError) {
//
//	}
//
//}
