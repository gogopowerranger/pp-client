package com.devmite.pricepolice.activity;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.GcmRegistrationService;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.SessionManager.CredentialsException;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.helper.http.IHttpResponseListener;
import com.devmite.pricepolice.model.http.AuthRequest.ByEmail;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.UserResponse;
import com.devmite.pricepolice.util.Utils;
import com.google.gson.Gson;

public class RegisterActivity extends Activity implements OnClickListener, IHttpResponseListener {
	private ProgressDialog progressDialog;
	private Button registerButton;
	private EditText editUsername, editEmail, editPassword, editConfirmPassword;
	private ValidationManager<RegisterActivity> validationManager;

	private GcmRegistrationService.Receiver gcmRegistrationReceiver = new GcmRegistrationService.Receiver();
	
	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(gcmRegistrationReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter gcmIf = new IntentFilter(Config.INTENT_BROADCAST_ACTION);
		LocalBroadcastManager.getInstance(this).registerReceiver(gcmRegistrationReceiver, gcmIf);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_register);

		editUsername = (EditText) findViewById(R.id.edit_username_reg);
		editEmail = (EditText) findViewById(R.id.edit_email_reg);
		editPassword = (EditText) findViewById(R.id.edit_password_reg);
		editConfirmPassword = (EditText) findViewById(R.id.edit_password_confirm_reg);
		registerButton = (Button) findViewById(R.id.button_register);
		registerButton.setOnClickListener(this);

		validationManager = new ValidationManager<RegisterActivity>(this);
	}

	@SuppressLint({ "NewApi", "ShowToast" })
	@Override
	public void onClick(View v) {
		if (v == registerButton) {
			String password = editPassword.getText().toString();
			String confirmPassword = editConfirmPassword.getText().toString();

			if (!password.equals(confirmPassword)) {
				this.validationManager.addError(editPassword.getId(), "Password confirmation does not match");
			}

			boolean proceed = validationManager.displayErrors();
			if (proceed) {
				ByEmail byEmailRequest = new ByEmail();
				byEmailRequest.setUsername(editUsername.getText().toString());
				byEmailRequest.setEmail(editEmail.getText().toString());
				byEmailRequest.setPassword(Utils.md5(password));

				progressDialog = new ProgressDialog(this);
				progressDialog.setCancelable(false);
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.setMessage(getResources().getString(R.string.dialog_please_wait));
				progressDialog.show();

				App.getRestClient().getAPIService()
						.register(byEmailRequest, new Callback<BaseRespone<UserResponse>>() {

							@Override
							public void success(BaseRespone<UserResponse> br, Response r) {
								UserResponse response = br.getData();
								Toast.makeText(RegisterActivity.this, R.string.registration_successful, Toast.LENGTH_SHORT);
								try {
									SessionManager.getInstance(RegisterActivity.this).setToken(response.getToken());
									SessionManager.getInstance(RegisterActivity.this).setUserId(response.getUserId());

									SharedPreferences sp = getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
									SharedPreferences.Editor spEditor = sp.edit();
									spEditor.putString(Config.SP_KEY_USER_EMAIL, response.getEmail());
									spEditor.putString(Config.SP_KEY_USER_USERNAME, response.getUsername());
									spEditor.putLong(Config.SP_KEY_USER_CREATE_TS, response.getCreateTs());
									spEditor.putLong(Config.SP_KEY_USER_UPDATE_TS, response.getUpdateTs());
									spEditor.commit();

									Intent gcmRegistrationIntent = new Intent(RegisterActivity.this, GcmRegistrationService.class);
									startService(gcmRegistrationIntent);

									App.getRestClient().getAPIService().primaryBoard(new Callback<BaseRespone<BoardResponse>>() {

										@Override
										public void success(BaseRespone<BoardResponse> br, Response r) {
											progressDialog.dismiss();
											try {
												BoardResponse board = br.getData();
												long boardId = board.getBoardId();
												Utils.saveLongPreferences(RegisterActivity.this, Config.KEY_PREF_PRIMARY_BOARD, boardId);

												Intent intent = new Intent(RegisterActivity.this, PrimaryActivity.class);
												intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
												intent.putExtra(Config.INTENT_KEY_CALLER, RegisterActivity.class.getName());
												RegisterActivity.this.startActivity(intent);
												RegisterActivity.this.finish();
											} catch (Exception e) {
												validationManager.handleError(new CredentialsException("Error while logging in"));
											}
										}

										@Override
										public void failure(RetrofitError re) {
											progressDialog.dismiss();
											try {
												CommonResponse error = (CommonResponse) re.getBodyAs(CommonResponse.class);
												validationManager.addError(error.getInternalCode(), error.getMessage());
												validationManager.displayErrors();
											} catch (Exception e) {
												Log.e("ws", re.getMessage(), e);
											}
										}
									});
								} catch (CredentialsException e) {
									if(progressDialog.isShowing()) {
										progressDialog.dismiss();
									}
									validationManager.handleError(e);
								}
							}

							@Override
							public void failure(RetrofitError e) {
								progressDialog.dismiss();

								CommonResponse error = (CommonResponse) e.getBodyAs(CommonResponse.class);
								validationManager.addError(error.getInternalCode(), error.getMessage());
								validationManager.displayErrors();
							}
						});
			}
		}
	}

	@Override
	public void resultSuccess(int type, String result) {

		if (progressDialog != null) {
			progressDialog.dismiss();
		}

		switch (type) {
		case Config.API_REGISTER: {
			try {
				JSONObject jo = new JSONObject(result);
				String message = jo.getString("message");
				String internalCode = jo.getString("internal_code");
				jo = jo.getJSONObject("data");

				Gson gson = new Gson();
				UserResponse response = gson.fromJson(jo.toString(),
						UserResponse.class);
				System.out.println(response.getUsername());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

		}
	}

	@Override
	public void resultFailed(int type, String strError) {
		Log.d("resultFailed ", strError);
	}

}
