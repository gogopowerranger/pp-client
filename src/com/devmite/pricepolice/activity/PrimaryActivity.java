package com.devmite.pricepolice.activity;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.fragment.NavigationDrawerFragment;
import com.devmite.pricepolice.fragment.PinDetailFragment;
import com.devmite.pricepolice.fragment.PinsFragment.OnPinDetailSelectedListener;
import com.devmite.pricepolice.fragment.PostPinFragment;
import com.devmite.pricepolice.fragment.PrimaryFragment;
import com.devmite.pricepolice.fragment.SearchFragment;
import com.devmite.pricepolice.fragment.SettingsFragment;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.GcmRegistrationService;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.model.NotificationAction;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.util.IntraBroadcastCode;

public class PrimaryActivity extends FragmentActivity
		implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnPinDetailSelectedListener {

	private GcmRegistrationService.Receiver gcmRegistrationReceiver = new GcmRegistrationService.Receiver();
	private LogoutBroadcastReceiver logoutBroadcastReceiver = new LogoutBroadcastReceiver();
	private NavigationDrawerFragment ndFragment;
	private CharSequence title;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT", FR_POST_PIN = "TAG_POST_PIN", FR_PINS = "TAG_PINS";
	private boolean justLoggedIn = false;
	private ValidationManager<Activity> validationManager = new ValidationManager<Activity>(this);
	private FragmentResume fragmentResume;

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(gcmRegistrationReceiver);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(logoutBroadcastReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter gcmIf = new IntentFilter(Config.INTENT_BROADCAST_ACTION);
		LocalBroadcastManager.getInstance(this).registerReceiver(gcmRegistrationReceiver, gcmIf);
		LocalBroadcastManager.getInstance(this).registerReceiver(logoutBroadcastReceiver, gcmIf);
	}

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_drawer);

		SessionManager.getInstance(this).checkLogin();

		String caller = getIntent().getStringExtra(Config.INTENT_KEY_CALLER);
		if (caller != null && (caller.equals(LoginActivity.class.getName()) || caller.equals(RegisterActivity.class.getName()))) {
			justLoggedIn = true;
		} else {
			Intent gcmRegistrationIntent = new Intent(this, GcmRegistrationService.class);
			startService(gcmRegistrationIntent);
		}

		fragmentResume = new FragmentResume();
		getSupportFragmentManager().addOnBackStackChangedListener(fragmentResume);

		ndFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
		title = getTitle();

		ndFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

		//redirect from notification
		String action = getIntent().getStringExtra("action");
		NotificationAction na = null;
		if (action != null && ((na = NotificationAction.fromString(action)) != null)) {
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setMessage(getResources().getString(R.string.dialog_please_wait));
			progressDialog.show();
			switch (na) {
			case PRICE_UPDATE:
			case NO_PRICE_UPDATE:
			case WONT_UPDATE:
				long pinId = getIntent().getLongExtra("pin_id", Long.MIN_VALUE);
				if (pinId != Long.MIN_VALUE) {
					App.getRestClient().getAPIService().pin(pinId, new Callback<BaseRespone<PinResponse>>() {

						@Override
						public void success(BaseRespone<PinResponse> br, Response r) {
							progressDialog.dismiss();
							onPinSelected(br.getData());
						}

						@Override
						public void failure(RetrofitError re) {
							progressDialog.dismiss();
							validationManager.handleError(re);
						}
					});
				}
				break;
			}
		}

		//redirect from share
		Intent receivedIntent = getIntent();
		String receivedAction = receivedIntent.getAction();
		String receivedType = receivedIntent.getType();
		if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND) && receivedType.startsWith("text/")) {
			String receivedText = receivedIntent.getStringExtra(Intent.EXTRA_TEXT);
			FragmentManager fm = getSupportFragmentManager();
			fm.beginTransaction().replace(R.id.container, PostPinFragment.newInstance(receivedText))
					.addToBackStack(FR_POST_PIN)
					.commit();

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		getSupportFragmentManager().removeOnBackStackChangedListener(fragmentResume);
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(title);
	}

	@Override
	public void onBackPressed() {
		if (!ndFragment.closeDrawer()) {
			super.onBackPressed();
		}
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// if (ndFragment.isDrawerOpen()) {
	// getMenuInflater().inflate(R.menu.drawer, menu);
	// restoreActionBar();
	// return true;
	// } else {
	// getMenuInflater().inflate(R.menu.global, menu);
	// return true;
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	//
	// default:
	// break;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		FragmentManager fm = getSupportFragmentManager();
		PrimaryFragment primaryFragment;

		switch (position) {
		case 0:
			primaryFragment = PrimaryFragment.newInstance(0);
			fm.beginTransaction()
					.addToBackStack(FR_PINS)
					.replace(R.id.container, primaryFragment)
					.commit();
			break;
		case 1:
			fm.beginTransaction()
					.replace(R.id.container, new PostPinFragment())
					.addToBackStack(FR_POST_PIN)
					.commit();
			break;
		case 2:
			primaryFragment = PrimaryFragment.newInstance(1);
			fm.beginTransaction()
					.addToBackStack(FR_PINS)
					.replace(R.id.container, primaryFragment)
					.commit();
			break;
		case 3:
			primaryFragment = PrimaryFragment.newInstance(2);
			fm.beginTransaction()
					.addToBackStack(FR_PINS)
					.replace(R.id.container, primaryFragment)
					.commit();
			break;
		case 4:
			fm.beginTransaction()
					.replace(R.id.container, new SearchFragment())
					.addToBackStack(TAG_FRAGMENT).commit();
			break;
		case 5:
			fm.beginTransaction()
					.replace(R.id.container, new SettingsFragment())
					.addToBackStack(TAG_FRAGMENT).commit();
			break;
		case 6:
			SessionManager.getInstance(this).logout2();
			break;
		default:
			primaryFragment = PrimaryFragment.newInstance(0);
			fm.beginTransaction()
					.replace(R.id.container, primaryFragment)
					.commit();
			break;
		}
	}

	@Override
	public void onPinSelected(PinResponse pr) {
		FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() > 0) {

			FragmentManager.BackStackEntry lastBackStackEntry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
			String lastBackStackEntryName = lastBackStackEntry.getName();
			Log.i("ws", "lastBackStackEntryName: " + lastBackStackEntryName);
			if (lastBackStackEntryName.equals(FR_POST_PIN)) {
				fm.beginTransaction().replace(R.id.container, PinDetailFragment.newInstance(pr)).addToBackStack(TAG_FRAGMENT).commit();
			} else {
				fm.beginTransaction().add(R.id.container, PinDetailFragment.newInstance(pr)).addToBackStack(TAG_FRAGMENT).commit();
			}
		} else {
			fm.beginTransaction().add(R.id.container, PinDetailFragment.newInstance(pr)).addToBackStack(TAG_FRAGMENT).commit();
		}
	}

	public void createNotification() {
		Intent intent = new Intent(this, PrimaryActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

		// Build notification
		// Actions are just fake
		Notification noti = new Notification.Builder(this)
				.setContentTitle("New mail from " + "test@gmail.com")
				.setContentText("Subject").setSmallIcon(R.drawable.icon)
				.setContentIntent(pIntent)
				.getNotification();
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// hide the notification after its selected
		noti.flags |= Notification.FLAG_AUTO_CANCEL;

		notificationManager.notify(0, noti);

	}

	private class FragmentResume implements FragmentManager.OnBackStackChangedListener {

		@Override
		public void onBackStackChanged() {
			Log.i("ws", "backstackchanged");
			FragmentManager manager = getSupportFragmentManager();

			if (manager != null) {
				int backStackEntryCount = manager.getBackStackEntryCount();
				Log.i("ws", "count: " + backStackEntryCount);
				if (backStackEntryCount > 0) {
					Fragment fragment = manager.getFragments().get(backStackEntryCount - 1);
					Log.i("ws", "Calling onResume on " + fragment.toString());
					fragment.onResume();
				} else {
					finish();
				}
			}

		}
	}

	private class LogoutBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (!action.equals(Config.INTENT_BROADCAST_ACTION)) {
				return;
			}

			int code = intent.getIntExtra(Config.INTENT_KEY_BROADCAST_EXTRA, 0);
			IntraBroadcastCode ibc = IntraBroadcastCode.fromCode(code);

			Log.i("ws", "LogoutBroadcastReceiver received: " + ibc.name());

			switch (ibc) {
			case GCM_UNREGISTERED:
				SessionManager.getInstance(PrimaryActivity.this).logout2();
				break;
			case LOGGED_OUT:
				Intent loginIntent = new Intent(PrimaryActivity.this, LoginActivity.class);
				loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				PrimaryActivity.this.startActivity(loginIntent);
				finish();
				break;
			case LOGOUT_UNSUCCESSFUL:
				Toast.makeText(PrimaryActivity.this, "Unable to logout for now.  Try again later.", Toast.LENGTH_LONG).show();
				break;
			}
		}
	}
}
