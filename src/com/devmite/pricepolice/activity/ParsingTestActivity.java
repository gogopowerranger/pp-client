//package com.devmite.pricepolice.activity;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Attribute;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemLongClickListener;
//import android.widget.ListView;
//import android.widget.SimpleAdapter;
//
//import com.devmite.pricepolice.R;
//import com.devmite.pricepolice.helper.Config;
//import com.devmite.pricepolice.model.Currency;
//import com.devmite.pricepolice.model.Price;
//import com.devmite.pricepolice.util.Utils;
//
//public class ParsingTestActivity extends Activity {
//	String URL = "http://www.lazada.co.id/kendra-signature-set-seprei-polaris-237443.html";
//
//	String[] sites = { "aliexpress.html", "amazon.html", "asos.html",
//			"berrybenka.html", "bhinneka.html", "bilna.html",
//			"bookdepository.html", "converse.html", "craftfoxes.html",
//			"custommade.html", "diapers.html", "ebay.html", "esteelauder.html",
//			"etsy.html", "fabfurnish.html", "forever21.html", "frys.html",
//			"gadgetsandgear.html", "gap.html", "gucci.html", "jcrew.html",
//			"lamido.html", "lazada.html", "leifshop.html",
//			"loutchiazcreations.html", "macys.html", "mignonkitchenco.html",
//			"modcloth.html", "muji.html", "nautica.html", "net-a-porter.html",
//			"newegg.html", "nordstrom.html", "oakley.html", "oldnavy.html",
//			"oliveandcocoa.html", "onekingslane.html", "onlineshoes.html",
//			"overstock.html", "palomasnest.html", "paulaschoice.html",
//			"perpetualkid.html", "pinkolive.html", "pixiemarket.html",
//			"polkadotclub.html", "popchartlab.html", "rakuten.html",
//			"sephora.html", "shanalogic.html", "shoporganic.html",
//			"shopplasticland.html", "stylebop.html", "target.html",
//			"thebodyshop.html", "thegrommet.html", "thinkgeek.html",
//			"thriftbooks.html", "topshop.html", "uncommongoods.html",
//			"urbanoutfitters.html", "victoriassecret.html", "wondermade.html",
//			"youngrepublic.html", "zalora.html", "zappos.html" };
//
//	private ArrayList<Map<String, String>> buildData() {
//		ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
//		list.add(putData("Aliexpress", "Aliexpress"));
//		list.add(putData("Amazon", "Amazon"));
//		list.add(putData("ASOS", "ASOS"));
//		list.add(putData("Berrybenka", "Berrybenka"));
//		list.add(putData("Bhinneka", "Bhinneka"));
//		list.add(putData("Bilna", "Bilna"));
//		list.add(putData("Book Depository", "Book Depository"));
//		list.add(putData("Converse", "Converse"));
//		list.add(putData("Craft Foxes", "Craft Foxes"));
//		list.add(putData("Custommade", "Custommade"));
//		list.add(putData("Diapers", "Diapers"));
//		list.add(putData("Ebay", "Ebay"));
//		list.add(putData("Estee Lauder", "Estee Lauder"));
//		list.add(putData("Etsy", "Etsy"));
//		list.add(putData("Fabfurnish", "fabfurnish"));
//		list.add(putData("Forever21", "Forever21"));
//		list.add(putData("Frys", "frys"));
//		list.add(putData("Gadgets and Gear", "gadgetsandgear"));
//		list.add(putData("Gap", "gap"));
//		list.add(putData("Gucci", "gucci"));
//		list.add(putData("J.Crew", "jcrew"));
//		list.add(putData("Lamido", "Lamido"));
//		list.add(putData("Lazada", "Lazada"));
//		list.add(putData("Leifshop", "Leifshop"));
//		list.add(putData("Loutchiaz Creations", "Loutchiaz Creations"));
//		list.add(putData("Macy's", "Macy's"));
//		list.add(putData("Mignonkitchenco", "mignonkitchenco"));
//		list.add(putData("Modcloth", "modcloth"));
//		list.add(putData("Muji", "Muji"));
//		list.add(putData("Nautica", "Muji"));
//		list.add(putData("Net-a-porter", "Muji"));
//		list.add(putData("Newegg", "Newegg"));
//		list.add(putData("Nordstrom", "nordstrom"));
//		list.add(putData("Oakley", "nordstrom"));
//		list.add(putData("Old Navy", "nordstrom"));
//		list.add(putData("Olive and Cocoa", "nordstrom"));
//		list.add(putData("One Kings Lane", "nordstrom"));
//		list.add(putData("Online Shoes", "Onlineshoes"));
//		list.add(putData("Overstock", "Overstock"));
//		list.add(putData("Paloma's Nest", "palomasnest"));
//		list.add(putData("Paula's Choice", "Paula's Choice"));
//		list.add(putData("Perpetual Kid", "perpetualkid"));
//		list.add(putData("Pink Olive", "perpetualkid"));
//		list.add(putData("Pixie Market", "perpetualkid"));
//		list.add(putData("Polkadot Club", "perpetualkid"));
//		list.add(putData("Pop Chart Lab", "abc"));
//		list.add(putData("Rakuten", "abc"));
//		list.add(putData("Sephora", "abc"));
//		list.add(putData("Shana Logic", "abc"));
//		list.add(putData("Shop Organic", "abc"));
//		list.add(putData("Shop Plastic Land", "abc"));
//		list.add(putData("Stylebop", "abc"));
//		list.add(putData("Target", "abc"));
//		list.add(putData("The Body Shop", "abc"));
//		list.add(putData("The Grommet", "abc"));
//		list.add(putData("Think Geek", "abc"));
//		list.add(putData("Thrift Books", "abc"));
//		list.add(putData("Topshop", "abc"));
//		list.add(putData("Uncommon Goods", "abc"));
//		list.add(putData("Urban Outfitters", "abc"));
//		list.add(putData("Victoria's Secret", "abc"));
//		list.add(putData("Wondermade", "abc"));
//		list.add(putData("Young Republic", "abc"));
//		list.add(putData("Zalora", "Zalora"));
//		list.add(putData("Zappos", "Zappos"));
//
//		return list;
//	}
//
//	private HashMap<String, String> putData(String name, String purpose) {
//		HashMap<String, String> item = new HashMap<String, String>();
//		item.put("name", name);
//		item.put("purpose", purpose);
//		return item;
//	}
//
//	@SuppressLint("ShowToast")
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.test);
//
//		ArrayList<Map<String, String>> list = buildData();
//		String[] from = { "name", "purpose" };
//		int[] to = { android.R.id.text1, android.R.id.text2 };
//
//		SimpleAdapter adapter = new SimpleAdapter(this, list,
//				android.R.layout.simple_list_item_1, from, to);
//
//		ListView listView = (ListView) findViewById(R.id.list_site);
//		listView.setAdapter(adapter);
//
//		// React to user clicks on item
//		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//			public void onItemClick(AdapterView<?> parentAdapter, View view,
//					int position, long id) {
//				new JSoupTask().execute(sites[position]);
//			}
//		});
//
//		listView.setOnItemLongClickListener(new OnItemLongClickListener() {
//
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
//					int pos, long id) {
//
//				Log.v("long clicked", "pos: " + pos);
//				goToWebviewActivity(pos);
//				return true;
//			}
//		});
//	}
//
//	private void goToWebviewActivity(int pos) {
//		Intent i = new Intent(this, WebPageActivity.class);
//		i.putExtra("page", sites[pos]);
//		startActivity(i);
//	}
//
//	private class JSoupTask extends AsyncTask<String, Void, List<Price>> {
//		String title;
//
//		@SuppressLint("DefaultLocale")
//		@Override
//		protected List<Price> doInBackground(String... params) {
//			try {
//				InputStream is = getAssets().open(params[0]);
//				int size = is.available();
//
//				byte[] buffer = new byte[size];
//				is.read(buffer);
//				is.close();
//
//				String str = new String(buffer);
//
//				Document doc = Jsoup.parse(str);
//
//				// -------------------- "price" attribute value
//				Elements withAttr = new Elements();
//				StringBuilder sb = new StringBuilder("");
//				List<String> listResult = new ArrayList<String>();
//
//				Currency currency;
//				Price price;
//
//				for (Element element : doc.getAllElements()) {
//					for (Attribute attribute : element.attributes()) {
//						String attValue = attribute.getValue();
//						attValue = attValue.toLowerCase();
//
//						if (attValue.contains("price")) {
//							withAttr.add(element);
//
//							String text = element.text();
//							if (Utils.isNumericRegex(text)) {
//								// avoid duplicates
//								if (!listResult.contains(text)) {
//									sb.append(text);
//									sb.append("\n----------------------\n");
//
//									listResult.add(text);
//								}
//							}
//						}
//					}
//				}
//
//				// -------------------- "price" attribute value
//
//				if (listResult.size() == 0) {
//
//					// -------------------- regex doang
//					// Elements elements =
//					// doc.select("div:matches(\\d+(\\.)?(\\,)?\\d*(\\.)?(\\,)?\\d+)");
//					String cssQuery = "*:matchesOwn([a-z|A-Z|\\s]{0,4}\\d+([.,]\\d{1,2})?)";
//					Elements elements = doc.select(cssQuery);
//
//					for (Element element : elements) {
//						String text = element.text();
//
//						for (int j = 0; j < Config.CURRENCY.length; j++) {
//							if (text.startsWith(Config.CURRENCY[j][0])
//									|| text.startsWith(Config.CURRENCY[j][1])) {
//								// avoid duplicates
//								if (!listResult.contains(text)) {
//									sb.append(text);
//									sb.append("\n----------------------\n");
//									listResult.add(text);
//									break;
//								}
//							}
//						}
//					}
//
//					// -------------------- regex doang
//
//				}
//
//				List<Price> listPrice = new ArrayList<Price>();
//
//				if (listResult.size() > 0) {
//					// separate value from currency
//					for (int i = 0; i < listResult.size(); i++) {
//						String text = listResult.get(i);
//						currency = new Currency();
//						price = new Price();
//						price.setDisplayed(text);
//						
//						boolean foundCurrency = false;
//
//						for (int j = 0; j < Config.CURRENCY.length; j++) {
//
//							if (text.startsWith(Config.CURRENCY[j][0])
//									|| text.startsWith(Config.CURRENCY[j][1])) {
//								currency.setSymbol(Config.CURRENCY[j][0]);
//								currency.setCodeISO(Config.CURRENCY[j][1]);
//
//								price.setCurrency(currency);
//
//								if (text.startsWith(Config.CURRENCY[j][0])) {
//									// remove (.) that sometimes trailing
//									// currency
//									char c = text.charAt(Config.CURRENCY[j][0]
//											.length());
//									if (c == '.') {
//										text = text
//												.substring(Config.CURRENCY[j][0]
//														.length() + 1);
//									} else {
//										text = text
//												.substring(Config.CURRENCY[j][0]
//														.length());
//									}
//
//								} else {
//									// remove (.) that sometimes trailing
//									// currency
//									char c = text.charAt(Config.CURRENCY[j][1]
//											.length());
//									if (c == '.') {
//										text = text
//												.substring(Config.CURRENCY[j][1]
//														.length() + 1);
//									} else {
//										text = text
//												.substring(Config.CURRENCY[j][1]
//														.length());
//									}
//								}
//
//								BigDecimal bigD = Utils.extractNumeric(text);
//								price.setValue(bigD);
//								listPrice.add(price);
//								foundCurrency = true;
//								break;
//							}
//						}
//						if (!foundCurrency) {
//							BigDecimal bigD = Utils.extractNumeric(text);
//							price.setValue(bigD);
//							listPrice.add(price);
//							break;
//						}
//					}
//				}
//
//				// remove 0.00
//
//				// return sb.toString();
//				return listPrice;
//
//				// // Connect to the web site
//				// // Document doc = Jsoup.connect(params[0]).get();
//				// title = doc.title();
//				//
//				// // Elements images = doc
//				// // .select("img[src~=(?i)\\.(png|jpe?g|gif)]");
//				// // for (Element image : images) {
//				// //
//				// // System.out.println("\nsrc : " + image.attr("src"));
//				// // System.out.println("height : " + image.attr("height"));
//				// // System.out.println("width : " + image.attr("width"));
//				// // System.out.println("alt : " + image.attr("alt"));
//				// //
//				// // }
//
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(List<Price> result) {
//			StringBuilder sb = new StringBuilder();
//			for (int i = 0; i < result.size(); i++) {
//				sb.append("text: ");
//				sb.append(result.get(i).getDisplayed());
//				sb.append("\n");
//				sb.append("value: ");
//				sb.append(result.get(i).getValue().toPlainString());
//				sb.append("\n----------------------\n");
//			}
//			AlertDialog.Builder builder1 = new AlertDialog.Builder(
//					ParsingTestActivity.this);
//			builder1.setMessage(sb.toString());
//			builder1.setCancelable(true);
//
//			AlertDialog alert11 = builder1.create();
//			alert11.show();
//		}
//
//	}
//
//}