package com.devmite.pricepolice.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.devmite.pricepolice.R;

public class WebPageActivity extends Activity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		
		Bundle extras = getIntent().getExtras();
		
		WebView webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("file:///android_asset/" +extras.getString("page"));
	}

}
