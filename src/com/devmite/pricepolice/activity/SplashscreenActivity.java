package com.devmite.pricepolice.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.util.Utils;

public class SplashscreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent intent = new Intent();
				if (!Utils.getBooleanPreferences(SplashscreenActivity.this, Config.SP_KEY_GUIDE_VIEWED, false)) {
					intent.setClass(SplashscreenActivity.this, GuideActivity.class);
				} else if (SessionManager.getInstance(SplashscreenActivity.this).isUserLoggedIn()) {
					intent.setClass(SplashscreenActivity.this, PrimaryActivity.class);
				} else {
					intent.setClass(SplashscreenActivity.this, LoginActivity.class);
				}
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				SplashscreenActivity.this.finish();
			}
		}, 200);
	}
}
