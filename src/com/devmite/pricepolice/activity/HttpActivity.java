package com.devmite.pricepolice.activity;

import android.app.Activity;
import android.app.ProgressDialog;

import com.devmite.pricepolice.helper.http.IHttpResponseListener;

public abstract class HttpActivity extends Activity implements
		IHttpResponseListener {
	protected ProgressDialog progressDialog;

}
