package com.devmite.pricepolice.activity;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.GcmRegistrationService;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.SessionManager.CredentialsException;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.model.http.AuthRequest;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.UserExtendedResponse;
import com.devmite.pricepolice.model.http.UserResponse;
import com.devmite.pricepolice.util.Utils;
import com.google.gson.Gson;

public class LoginActivity extends HttpActivity implements OnClickListener {

	private Button createAccountButton, loginButton, loginTwitterButton;
	private ImageButton loginFBButton;
	private EditText editUsername, editPassword;
	private ValidationManager<LoginActivity> validationManager;

	private GcmRegistrationService.Receiver gcmRegistrationReceiver = new GcmRegistrationService.Receiver();

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(gcmRegistrationReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter gcmIf = new IntentFilter(Config.INTENT_BROADCAST_ACTION);
		LocalBroadcastManager.getInstance(this).registerReceiver(gcmRegistrationReceiver, gcmIf);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		createAccountButton = (Button) findViewById(R.id.create_account);
		createAccountButton.setOnClickListener(this);

		loginButton = (Button) findViewById(R.id.button_login);
		loginButton.setOnClickListener(this);

		loginFBButton = (ImageButton) findViewById(R.id.login_fb);
		loginFBButton.setOnClickListener(this);

		loginTwitterButton = (Button) findViewById(R.id.login_twitter);
		loginTwitterButton.setOnClickListener(this);

		editUsername = (EditText) findViewById(R.id.edit_username);
		editPassword = (EditText) findViewById(R.id.edit_password);
		
		validationManager = new ValidationManager<LoginActivity>(this);

		Intent intent = getIntent();
		boolean redirected = intent.getBooleanExtra("redirected", false);
		if (redirected) {
			validationManager.addError(hashCode(), "Fatal error occurred");
			validationManager.displayErrors();
		}
	}

	private void goToRegisterActivity() {
		Intent i = new Intent(this, RegisterActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_post_pin, menu);
		return true;
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		if (v == createAccountButton) {
			goToRegisterActivity();
		} else if (v == loginButton) {
			String username = editUsername.getText().toString();
			String password = editPassword.getText().toString();

			if (StringUtil.isBlank(username)) {
				this.validationManager.addError(editUsername.getId(), getResources().getString(R.string.prompt_username_empty));
			}
			if (StringUtil.isBlank(password)) {
				this.validationManager.addError(editUsername.getId(), getResources().getString(R.string.prompt_username_empty));
			}

			boolean proceed = validationManager.displayErrors();
			if (proceed) {
				AuthRequest.ByEmail request = new AuthRequest.ByEmail();

				if (Utils.isEmail(username)) {
					request.setEmail(username);
				} else {
					request.setUsername(username);
				}
				request.setPassword(Utils.md5(password));

				progressDialog = new ProgressDialog(this);
				progressDialog.setCancelable(false);
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.setMessage(getResources().getString(R.string.dialog_please_wait));
				progressDialog.show();

				App.getRestClient().getAPIService().login(request, new Callback<BaseRespone<UserResponse>>() {

					@Override
					public void success(BaseRespone<UserResponse> br, Response r) {
						UserResponse response = br.getData();

						try {
							SessionManager.getInstance(LoginActivity.this).setToken(response.getToken());
							SessionManager.getInstance(LoginActivity.this).setUserId(response.getUserId());

							SharedPreferences sp = getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
							SharedPreferences.Editor spEditor = sp.edit();
							spEditor.putString(Config.SP_KEY_USER_EMAIL, response.getEmail());
							spEditor.putString(Config.SP_KEY_USER_USERNAME, response.getUsername());
							spEditor.putLong(Config.SP_KEY_USER_CREATE_TS, response.getCreateTs());
							spEditor.putLong(Config.SP_KEY_USER_UPDATE_TS, response.getUpdateTs());
							UserExtendedResponse uer = response.getExtended();
							if (uer != null) {
								spEditor.putString(Config.SP_KEY_USER_AVATAR, uer.getAvatar());
							}
							spEditor.commit();

							Intent gcmRegistrationIntent = new Intent(LoginActivity.this, GcmRegistrationService.class);
							startService(gcmRegistrationIntent);
							
							App.getRestClient().getAPIService().primaryBoard(new Callback<BaseRespone<BoardResponse>>() {

								@Override
								public void success(BaseRespone<BoardResponse> br, Response r) {
									progressDialog.dismiss();
									try {
										BoardResponse board = br.getData();
										long boardId = board.getBoardId();
										Utils.saveLongPreferences(LoginActivity.this, Config.KEY_PREF_PRIMARY_BOARD, boardId);

										Intent intent = new Intent(LoginActivity.this, PrimaryActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										intent.putExtra(Config.INTENT_KEY_CALLER, LoginActivity.class.getName());
										LoginActivity.this.startActivity(intent);
										LoginActivity.this.finish();
									} catch (Exception e) {
										validationManager.handleError(new CredentialsException("Error while logging in"));
									}
								}

								@Override
								public void failure(RetrofitError re) {
									progressDialog.dismiss();
									try {
										CommonResponse error = (CommonResponse) re.getBodyAs(CommonResponse.class);
										validationManager.addError(error.getInternalCode(), error.getMessage());
										validationManager.displayErrors();
									} catch (Exception e) {
										Log.e("ws", re.getMessage(), e);
									}
								}
							});
						} catch (CredentialsException e) {
							if (progressDialog.isShowing()) {
								progressDialog.dismiss();
							}
							validationManager.handleError(e);
						}
					}

					@Override
					public void failure(RetrofitError re) {
						progressDialog.dismiss();
						validationManager.handleError(re);
					}
				});
			}

		}
	}

	private void setPrimaryBoardId() throws CredentialsException {
		try {
			BaseRespone<BoardResponse> r = App.getRestClient().getAPIService().primaryBoard();
			BoardResponse br = r.getData();
			if (br != null) {
				long boardId = br.getBoardId();
				Utils.saveLongPreferences(this, Config.KEY_PREF_PRIMARY_BOARD, boardId);
			} else {
				throw new CredentialsException("");
			}
		} catch (RetrofitError re) {
			try {
				CommonResponse cr = (CommonResponse) re.getBodyAs(CommonResponse.class);
				throw new CredentialsException(cr.getMessage());
			} catch (Exception e) {
				throw new CredentialsException(e.getMessage());
			}
		}
	}

	@Override
	public void resultSuccess(int type, String result) {

		if (progressDialog != null) {
			progressDialog.dismiss();
		}

		switch (type) {
		case Config.API_LOGIN: {
			try {
				JSONObject jo = new JSONObject(result);
				String message = jo.getString("message");
				String internalCode = jo.getString("internal_code");
				jo = jo.getJSONObject("data");

				Gson gson = new Gson();
				UserResponse response = gson.fromJson(jo.toString(), UserResponse.class);
				System.out.println(response.getUsername());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

		}
	}

	@Override
	public void resultFailed(int type, String strError) {
		Log.d("resultFailed ", strError);
	}

}
