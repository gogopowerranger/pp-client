package com.devmite.pricepolice.app;

import java.util.Locale;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.devmite.pricepolice.helper.PriceService;
import com.devmite.pricepolice.rest.RestClient;
import com.devmite.pricepolice.util.ImageLoader;
import com.devmite.pricepolice.util.Utils;

public class App extends Application {
	private static Locale locale;
	private static RestClient restClient;
	private static Context context;

	private static Typeface sanchezTypeface, sansTypeface;
	private static ImageLoader imageLoader;
	private static PriceService currencyManager;

	@Override
	public void onCreate() {
		super.onCreate();
		locale = Locale.ROOT;
		context = getApplicationContext();
		restClient = new RestClient(context);

		sanchezTypeface = Typeface.createFromAsset(getAssets(), "Sanchezregular.otf");

		sansTypeface = Typeface.createFromAsset(getAssets(), "weblysleekuisl.ttf");

		Utils.overrideFont("DEFAULT", sansTypeface);
		Utils.overrideFont("DEFAULT_BOLD", sansTypeface);
		Utils.overrideFont("SANS_SERIF", sansTypeface);
		
		imageLoader = ImageLoader.getInstance(this);
	}

	public static Context getContext() {
		return context;
	}

	public static RestClient getRestClient() {
		return restClient;
	}

	public static Typeface getSanchezTypeface() {
		return sanchezTypeface;
	}

	public static ImageLoader getImageLoader() {
		return imageLoader;
	}
	
	public static Locale getLocale() {
		return locale;
	}
}
