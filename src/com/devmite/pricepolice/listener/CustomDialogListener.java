package com.devmite.pricepolice.listener;

public interface CustomDialogListener<R> {
	void updateDialogResult(R result);
}
