package com.devmite.pricepolice.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.util.Utils;

public class TwoLineSpinner extends LinearLayout {
	private LinearLayout layoutView;
	private ImageView imageView;
	private Spinner spinner;
	private TextView subtitleView;

	public TwoLineSpinner(Context context) {
		this(context, null);
	}

	public TwoLineSpinner(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.twoLineStuffAttr);
	}

	public TwoLineSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		setOrientation(VERTICAL);
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TwoLineStuff, defStyle, 0);

		final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rootView = inflater.inflate(R.layout.spinner_2line, this);

		layoutView = (LinearLayout) rootView.findViewById(R.id.__layout);

		imageView = (ImageView) rootView.findViewById(R.id.__image);
		int imageSource = a.getResourceId(R.styleable.TwoLineStuff_imageSrc, Integer.MIN_VALUE);
		if (imageSource != Integer.MIN_VALUE) {
			Bitmap image = BitmapFactory.decodeResource(getResources(), imageSource);
			setImageBitmap(image);
		}

		spinner = (Spinner) rootView.findViewById(R.id.__spinner);
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerAdapter);

		int entriesResourceId = a.getResourceId(R.styleable.TwoLineStuff_spinnerEntries, Integer.MIN_VALUE);
		if (entriesResourceId != Integer.MIN_VALUE) {
			String[] entries = context.getResources().getStringArray(entriesResourceId);
			spinnerAdapter.addAll(entries);
		}

		String prompt = a.getString(R.styleable.TwoLineStuff_spinnerPrompt);
		spinner.setPrompt(prompt);
		Log.i("ws", "prompt: " + prompt);

		subtitleView = (TextView) rootView.findViewById(R.id.__subtitle);
		String subtitleStr = a.getString(R.styleable.TwoLineStuff_subtitle);
		setSubtitle(subtitleStr);

		a.recycle();
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
	}

	public LinearLayout getLayoutView() {
		return layoutView;
	}

	public Spinner getSpinner() {
		return spinner;
	}

	public TextView getSubtitleView() {
		return subtitleView;
	}

	public void setAdapter(SpinnerAdapter adapter) {
		this.spinner.setAdapter(adapter);
	}

	public void setSubtitle(String subtitle) {
		this.subtitleView.setText(subtitle);

		if (Utils.isBlank(subtitle)) {
			this.subtitleView.setVisibility(View.GONE);
		} else {
			this.subtitleView.setVisibility(View.VISIBLE);
		}
	}

	public void setImageBitmap(Bitmap bitmap) {
		int min = bitmap.getWidth() > bitmap.getHeight() ? bitmap.getHeight() : bitmap.getWidth();
		Bitmap result = Bitmap.createBitmap(min, min, Config.ARGB_8888);
		Canvas canvas = new Canvas(result);

		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		final Shader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		paint.setShader(shader);

		final float half = min / 2;
		// canvas.drawBitmap(
		// bitmap,
		// bitmap.getWidth() < result.getWidth() ? bitmap.getWidth() / 2 -
		// result.getWidth() / 2 : result.getWidth() / 2
		// - bitmap.getWidth() / 2,
		// bitmap.getHeight() < result.getHeight() ? bitmap.getHeight() / 2 -
		// result.getHeight() / 2 : result.getHeight() / 2
		// - bitmap.getHeight() / 2, null);
		canvas.drawCircle(half, half, half, paint);

		this.imageView.setImageBitmap(result);
		this.imageView.setVisibility(View.VISIBLE);
	}
}
