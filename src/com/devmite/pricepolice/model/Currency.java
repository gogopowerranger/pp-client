package com.devmite.pricepolice.model;

public class Currency {

	private String symbol;
	private String codeISO;

	public Currency() {
		// TODO Auto-generated constructor stub
	}

	public Currency(String symbol, String codeISO) {
		super();
		this.symbol = symbol;
		this.codeISO = codeISO;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getCodeISO() {
		return codeISO;
	}

	public void setCodeISO(String codeISO) {
		this.codeISO = codeISO;
	}

	@Override
	public String toString() {
		return "Currency [symbol=" + symbol + ", codeISO=" + codeISO + "]";
	}

}
