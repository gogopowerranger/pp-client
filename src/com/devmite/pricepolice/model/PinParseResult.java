package com.devmite.pricepolice.model;

import java.util.List;

public class PinParseResult {

	private List<String> titles;
	private List<String> imageURLs;
	private List<Price> prices;
	private int currencyOrder = -1;

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public List<String> getImageURLs() {
		return imageURLs;
	}

	public void setImageURLs(List<String> imageURLs) {
		this.imageURLs = imageURLs;
	}

	public int getCurrencyOrder() {
		return currencyOrder;
	}

	public void setCurrencyOrder(int currency) {
		this.currencyOrder = currency;
	}

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + currencyOrder;
		result = prime * result + ((imageURLs == null) ? 0 : imageURLs.hashCode());
		result = prime * result + ((prices == null) ? 0 : prices.hashCode());
		result = prime * result + ((titles == null) ? 0 : titles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PinParseResult other = (PinParseResult) obj;
		if (currencyOrder != other.currencyOrder)
			return false;
		if (imageURLs == null) {
			if (other.imageURLs != null)
				return false;
		} else if (!imageURLs.equals(other.imageURLs))
			return false;
		if (prices == null) {
			if (other.prices != null)
				return false;
		} else if (!prices.equals(other.prices))
			return false;
		if (titles == null) {
			if (other.titles != null)
				return false;
		} else if (!titles.equals(other.titles))
			return false;
		return true;
	}

}
