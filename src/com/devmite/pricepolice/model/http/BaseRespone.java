package com.devmite.pricepolice.model.http;

public class BaseRespone<T> extends CommonResponse {

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		if (data != null) {
			return "BaseRespone [internalCode=" + internalCode + ", message="
					+ message + ", data=" + data + "]";
		} else {
			return "BaseRespone [internalCode=" + internalCode + ", message="
					+ message + "]";
		}
	}

}
