package com.devmite.pricepolice.model.http;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

/**
 *
 * @author William
 */
public class UserResponse implements Serializable, Parcelable {
	private static final long serialVersionUID = -7455101327408584817L;
	private long userId;
	private String username;
	@Nullable
	private String token;
	private String email;
	private String authType;
	private long createTs;
	private long updateTs;
	@Nullable
	private UserExtendedResponse extended;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public long getCreateTs() {
		return createTs;
	}

	public void setCreateTs(long createTs) {
		this.createTs = createTs;
	}

	public long getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(long updateTs) {
		this.updateTs = updateTs;
	}

	public UserExtendedResponse getExtended() {
		return extended;
	}

	public void setExtended(UserExtendedResponse extended) {
		this.extended = extended;
	}

	protected UserResponse(Parcel in) {
		userId = in.readLong();
		username = in.readString();
		token = in.readString();
		email = in.readString();
		authType = in.readString();
		createTs = in.readLong();
		updateTs = in.readLong();
		extended = in.<UserExtendedResponse>readParcelable(UserExtendedResponse.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(userId);
		dest.writeString(username);
		dest.writeString(token);
		dest.writeString(email);
		dest.writeString(authType);
		dest.writeLong(createTs);
		dest.writeLong(updateTs);
		dest.writeParcelable(extended, flags);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<UserResponse> CREATOR = new Parcelable.Creator<UserResponse>() {
		@Override
		public UserResponse createFromParcel(Parcel in) {
			return new UserResponse(in);
		}

		@Override
		public UserResponse[] newArray(int size) {
			return new UserResponse[size];
		}
	};
}