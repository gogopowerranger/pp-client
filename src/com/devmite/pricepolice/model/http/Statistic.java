package com.devmite.pricepolice.model.http;

public class Statistic {

	private int followers, pin_id;

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getPin_id() {
		return pin_id;
	}

	public void setPin_id(int pin_id) {
		this.pin_id = pin_id;
	}
	
	
}
