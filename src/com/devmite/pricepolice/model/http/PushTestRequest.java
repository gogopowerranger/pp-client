package com.devmite.pricepolice.model.http;

public class PushTestRequest {
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
