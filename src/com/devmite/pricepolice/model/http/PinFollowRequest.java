package com.devmite.pricepolice.model.http;

public class PinFollowRequest {
	private long pinId;

	public long getPinId() {
		return pinId;
	}

	public void setPinId(long pinId) {
		this.pinId = pinId;
	}
}
