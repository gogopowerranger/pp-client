package com.devmite.pricepolice.model.http;

public class PinFollowResponse {
	private long pinId;
	private long userId;
	private long createTs;
	private long updateTs;

	public long getPinId() {
		return pinId;
	}

	public void setPinId(long pinId) {
		this.pinId = pinId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCreateTs() {
		return createTs;
	}

	public void setCreateTs(long createTs) {
		this.createTs = createTs;
	}

	public long getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(long updateTs) {
		this.updateTs = updateTs;
	}

}
