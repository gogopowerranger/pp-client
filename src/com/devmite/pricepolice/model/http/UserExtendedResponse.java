package com.devmite.pricepolice.model.http;

import android.os.Parcel;
import android.os.Parcelable;

public class UserExtendedResponse implements Parcelable {
	private long userId;
	private String avatar;
	private int age;
	private String gender; // Only MALE, FEMALE, UNDEFINED
	private long lastSeen;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(long lastSeen) {
		this.lastSeen = lastSeen;
	}

	protected UserExtendedResponse(Parcel in) {
		userId = in.readLong();
		avatar = in.readString();
		age = in.readInt();
		gender = in.readString();
		lastSeen = in.readLong();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(userId);
		dest.writeString(avatar);
		dest.writeInt(age);
		dest.writeString(gender);
		dest.writeLong(lastSeen);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<UserExtendedResponse> CREATOR = new Parcelable.Creator<UserExtendedResponse>() {
		@Override
		public UserExtendedResponse createFromParcel(Parcel in) {
			return new UserExtendedResponse(in);
		}

		@Override
		public UserExtendedResponse[] newArray(int size) {
			return new UserExtendedResponse[size];
		}
	};
}