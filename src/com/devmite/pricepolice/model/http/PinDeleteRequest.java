package com.devmite.pricepolice.model.http;

public class PinDeleteRequest {

	private long pinId;

	public long getPinId() {
		return pinId;
	}

	public void setPinId(long pinId) {
		this.pinId = pinId;
	}

}
