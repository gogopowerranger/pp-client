package com.devmite.pricepolice.model.http;

public class CommonResponse {
	protected int internalCode;
	protected String message;

	public int getInternalCode() {
		return internalCode;
	}

	public void setInternalCode(int internalCode) {
		this.internalCode = internalCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "CommonResponse [internalCode=" + internalCode + ", message="
				+ message + "]";
	}

}
