package com.devmite.pricepolice.model.http;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

public class BoardResponse implements Parcelable {
	private long boardId;
	private long userId;
	private long createTs;
	private long updateTs;
	private String label;
	private boolean primary;
	private int visibility;
	@Nullable
	private List<PinResponse> pins;

	public long getBoardId() {
		return boardId;
	}

	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCreateTs() {
		return createTs;
	}

	public void setCreateTs(long createTs) {
		this.createTs = createTs;
	}

	public long getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(long updateTs) {
		this.updateTs = updateTs;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public List<PinResponse> getPins() {
		return pins;
	}

	public void setPins(List<PinResponse> pins) {
		this.pins = pins;
	}

	protected BoardResponse(Parcel in) {
		boardId = in.readLong();
		userId = in.readLong();
		createTs = in.readLong();
		updateTs = in.readLong();
		label = in.readString();
		primary = in.readByte() != 0x00;
		visibility = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(boardId);
		dest.writeLong(userId);
		dest.writeLong(createTs);
		dest.writeLong(updateTs);
		dest.writeString(label);
		dest.writeByte((byte) (primary ? 0x01 : 0x00));
		dest.writeInt(visibility);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<BoardResponse> CREATOR = new Parcelable.Creator<BoardResponse>() {
		@Override
		public BoardResponse createFromParcel(Parcel in) {
			return new BoardResponse(in);
		}

		@Override
		public BoardResponse[] newArray(int size) {
			return new BoardResponse[size];
		}
	};
}