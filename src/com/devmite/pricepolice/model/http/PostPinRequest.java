package com.devmite.pricepolice.model.http;


public class PostPinRequest {

	private long board_id;
	private String label;
	private String page_url;
	private String image_url;
	private String price_selector;
	private String currency_code;
	private String user_agent;
	private boolean receive_update;
	private String price;
	private long update_interval;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getBoard_id() {
		return board_id;
	}

	public void setBoard_id(long board_id) {
		this.board_id = board_id;
	}

	public String getPage_url() {
		return page_url;
	}

	public void setPage_url(String page_url) {
		this.page_url = page_url;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getPrice_selector() {
		return price_selector;
	}

	public void setPrice_selector(String price_selector) {
		this.price_selector = price_selector;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public String getUser_agent() {
		return user_agent;
	}

	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}

	public boolean isReceive_update() {
		return receive_update;
	}

	public void setReceive_update(boolean receive_update) {
		this.receive_update = receive_update;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public long getUpdate_interval() {
		return update_interval;
	}

	public void setUpdate_interval(long update_interval) {
		this.update_interval = update_interval;
	}

}
