package com.devmite.pricepolice.model.http;

import java.io.Serializable;

/**
 *
 * @author William
 */
public class AuthRequest {

	public static class ByEmail implements Serializable {
		private static final long serialVersionUID = -1087953984179119631L;
		private String username;
		private String email;
		private String password;
		private String deviceIdentifier = "Android";

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDeviceIdentifier() {
			return deviceIdentifier;
		}

		public void setDeviceIdentifier(String deviceIdentifier) {
			this.deviceIdentifier = deviceIdentifier;
		}

	}

	public static class ByFacebook implements Serializable {
		private static final long serialVersionUID = -4515578799115148517L;
	}
}
