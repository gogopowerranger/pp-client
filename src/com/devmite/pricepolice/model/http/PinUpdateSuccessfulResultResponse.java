package com.devmite.pricepolice.model.http;

import android.os.Parcel;
import android.os.Parcelable;

public class PinUpdateSuccessfulResultResponse extends PinUpdateResultResponse {
	private int idx;
	private String secondLatestPrice;

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getSecondLatestPrice() {
		return secondLatestPrice;
	}

	public void setSecondLatestPrice(String secondLatestPrice) {
		this.secondLatestPrice = secondLatestPrice;
	}
	
	protected PinUpdateSuccessfulResultResponse(Parcel in) {
        super(in);
        idx = in.readInt();
        secondLatestPrice = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(idx);
        dest.writeString(secondLatestPrice);
    }

    public static final Parcelable.Creator<PinUpdateSuccessfulResultResponse> CREATOR = new Parcelable.Creator<PinUpdateSuccessfulResultResponse>() {
        @Override
        public PinUpdateSuccessfulResultResponse createFromParcel(Parcel in) {
            return new PinUpdateSuccessfulResultResponse(in);
        }

        @Override
        public PinUpdateSuccessfulResultResponse[] newArray(int size) {
            return new PinUpdateSuccessfulResultResponse[size];
        }
    };


}
