package com.devmite.pricepolice.model.http;

public class PushRegistrationRequest {
	private String regId;

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}
}
