package com.devmite.pricepolice.model.http;

import java.util.List;

public class PinRequest {
	public static class Search {
		private String keyword;
		private int offset, limit;

		public String getKeyword() {
			return keyword;
		}

		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}

		public int getOffset() {
			return offset;
		}

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

	}

	public static class ByBoard {
		private long boardId;
		private Boolean latest, mostFollowed;
		private int offset, limit;

		public long getBoardId() {
			return boardId;
		}

		public void setBoardId(long boardId) {
			this.boardId = boardId;
		}

		public Boolean isLatest() {
			return latest;
		}

		public void setLatest(Boolean latest) {
			this.latest = latest;
		}

		public Boolean isMostFollowed() {
			return mostFollowed;
		}

		public void setMostFollowed(Boolean mostFollowed) {
			this.mostFollowed = mostFollowed;
		}

		public int getOffset() {
			return offset;
		}

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}
	}

	public static class ByPinIds {
		private List<Long> pinIds;

		public List<Long> getPinIds() {
			return pinIds;
		}

		public void setPinIds(List<Long> pinIds) {
			this.pinIds = pinIds;
		}
	}

	public static class Followed {
		private int offset, limit;

		public int getOffset() {
			return offset;
		}

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

	}

	public static class Public {
		private Boolean latest, mostFollowed;
		private int offset, limit;

		public Boolean isLatest() {
			return latest;
		}

		public void setLatest(Boolean latest) {
			this.latest = latest;
		}

		public Boolean isMostFollowed() {
			return mostFollowed;
		}

		public void setMostFollowed(Boolean mostFollowed) {
			this.mostFollowed = mostFollowed;
		}

		public int getOffset() {
			return offset;
		}

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

	}

}
