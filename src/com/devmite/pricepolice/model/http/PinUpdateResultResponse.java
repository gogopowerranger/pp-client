package com.devmite.pricepolice.model.http;

import android.os.Parcel;
import android.os.Parcelable;

public class PinUpdateResultResponse implements Parcelable {
	private long pinId;
	private long boardId;
	private String imageUrl;
	private String label;
	private String currency;
	private String initialPrice;
	private String latestPrice;
	private long createTs;

	public long getPinId() {
		return pinId;
	}

	public void setPinId(long pinId) {
		this.pinId = pinId;
	}

	public long getBoardId() {
		return boardId;
	}

	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getInitialPrice() {
		return initialPrice;
	}

	public void setInitialPrice(String initialPrice) {
		this.initialPrice = initialPrice;
	}

	public String getLatestPrice() {
		return latestPrice;
	}

	public void setLatestPrice(String latestPrice) {
		this.latestPrice = latestPrice;
	}

	public long getCreateTs() {
		return createTs;
	}

	public void setCreateTs(long createTs) {
		this.createTs = createTs;
	}

	protected PinUpdateResultResponse(Parcel in) {
		pinId = in.readLong();
		boardId = in.readLong();
		imageUrl = in.readString();
		label = in.readString();
		currency = in.readString();
		initialPrice = in.readString();
		latestPrice = in.readString();
		createTs = in.readLong();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(pinId);
		dest.writeLong(boardId);
		dest.writeString(imageUrl);
		dest.writeString(label);
		dest.writeString(currency);
		dest.writeString(initialPrice);
		dest.writeString(latestPrice);
		dest.writeLong(createTs);
	}

	public static final Parcelable.Creator<PinUpdateResultResponse> CREATOR = new Parcelable.Creator<PinUpdateResultResponse>() {
		@Override
		public PinUpdateResultResponse createFromParcel(Parcel in) {
			return new PinUpdateResultResponse(in);
		}

		@Override
		public PinUpdateResultResponse[] newArray(int size) {
			return new PinUpdateResultResponse[size];
		}
	};

}
