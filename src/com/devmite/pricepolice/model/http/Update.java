package com.devmite.pricepolice.model.http;

public class Update {

	private long create_ts;
	private int idx, pin_id;
	private String price;

	public long getCreate_ts() {
		return create_ts;
	}

	public void setCreate_ts(long create_ts) {
		this.create_ts = create_ts;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public int getPin_id() {
		return pin_id;
	}

	public void setPin_id(int pin_id) {
		this.pin_id = pin_id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}
