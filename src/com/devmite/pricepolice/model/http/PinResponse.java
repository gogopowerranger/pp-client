package com.devmite.pricepolice.model.http;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

public class PinResponse implements Parcelable {
	public static class Statistics implements Parcelable {
		private long pinId;
		private int followers;

		public long getPinId() {
			return pinId;
		}

		public void setPinId(long pinId) {
			this.pinId = pinId;
		}

		public int getFollowers() {
			return followers;
		}

		public void setFollowers(int followers) {
			this.followers = followers;
		}

		protected Statistics(Parcel in) {
			pinId = in.readLong();
			followers = in.readInt();
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(pinId);
			dest.writeInt(followers);
		}

		@SuppressWarnings("unused")
		public static final Parcelable.Creator<Statistics> CREATOR = new Parcelable.Creator<Statistics>() {
			@Override
			public Statistics createFromParcel(Parcel in) {
				return new Statistics(in);
			}

			@Override
			public Statistics[] newArray(int size) {
				return new Statistics[size];
			}
		};
	}

	private long pinId;
	private long userId;
	private long boardId;
	private long createTs;
	private long updateTs;
	private String label;
	private String pageUrl;
	private String userAgent;
	private String acceptLanguage;
	private String imageUrl;
	private String price;
	private String latestPrice;
	private long latestPriceTs;
	private String currency;
	private String note;
	private boolean receiveUpdate;
	private int updateIntervalHr;
	private int updateFailureCount;

	@Nullable
	private Statistics statistics;

	public long getPinId() {
		return pinId;
	}

	public void setPinId(long pinId) {
		this.pinId = pinId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getBoardId() {
		return boardId;
	}

	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}

	public long getCreateTs() {
		return createTs;
	}

	public void setCreateTs(long createTs) {
		this.createTs = createTs;
	}

	public long getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(long updateTs) {
		this.updateTs = updateTs;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getAcceptLanguage() {
		return acceptLanguage;
	}

	public void setAcceptLanguage(String acceptLanguage) {
		this.acceptLanguage = acceptLanguage;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLatestPrice() {
		return latestPrice;
	}

	public void setLatestPrice(String latestPrice) {
		this.latestPrice = latestPrice;
	}

	public long getLatestPriceTs() {
		return latestPriceTs;
	}

	public void setLatestPriceTs(long latestPriceTs) {
		this.latestPriceTs = latestPriceTs;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public boolean isReceiveUpdate() {
		return receiveUpdate;
	}

	public void setReceiveUpdate(boolean receiveUpdate) {
		this.receiveUpdate = receiveUpdate;
	}

	public int getUpdateIntervalHr() {
		return updateIntervalHr;
	}

	public void setUpdateIntervalHr(int updateIntervalHr) {
		this.updateIntervalHr = updateIntervalHr;
	}

	public int getUpdateFailureCount() {
		return updateFailureCount;
	}

	public void setUpdateFailureCount(int updateFailureCount) {
		this.updateFailureCount = updateFailureCount;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

	protected PinResponse(Parcel in) {
		pinId = in.readLong();
		userId = in.readLong();
		boardId = in.readLong();
		createTs = in.readLong();
		updateTs = in.readLong();
		label = in.readString();
		pageUrl = in.readString();
		userAgent = in.readString();
		acceptLanguage = in.readString();
		imageUrl = in.readString();
		price = in.readString();
		latestPrice = in.readString();
		latestPriceTs = in.readLong();
		currency = in.readString();
		note = in.readString();
		receiveUpdate = in.readByte() != 0x00;
		updateIntervalHr = in.readInt();
		updateFailureCount = in.readInt();
		statistics = (Statistics) in.readValue(Statistics.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(pinId);
		dest.writeLong(userId);
		dest.writeLong(boardId);
		dest.writeLong(createTs);
		dest.writeLong(updateTs);
		dest.writeString(label);
		dest.writeString(pageUrl);
		dest.writeString(userAgent);
		dest.writeString(acceptLanguage);
		dest.writeString(imageUrl);
		dest.writeString(price);
		dest.writeString(latestPrice);
		dest.writeLong(latestPriceTs);
		dest.writeString(currency);
		dest.writeString(note);
		dest.writeByte((byte) (receiveUpdate ? 0x01 : 0x00));
		dest.writeInt(updateIntervalHr);
		dest.writeInt(updateFailureCount);
		dest.writeValue(statistics);
	}

	public static final Parcelable.Creator<PinResponse> CREATOR = new Parcelable.Creator<PinResponse>() {
		@Override
		public PinResponse createFromParcel(Parcel in) {
			return new PinResponse(in);
		}

		@Override
		public PinResponse[] newArray(int size) {
			return new PinResponse[size];
		}
	};

	@Override
	public String toString() {
		return "PinResponse [pinId=" + pinId + ", userId=" + userId + ", boardId=" + boardId + ", createTs=" + createTs
				+ ", updateTs=" + updateTs + ", label=" + label + ", pageUrl=" + pageUrl + ", userAgent=" + userAgent
				+ ", acceptLanguage=" + acceptLanguage + ", imageUrl=" + imageUrl + ", price=" + price
				+ ", latestPrice=" + latestPrice + ", latestPriceTs=" + latestPriceTs + ", currency=" + currency
				+ ", note=" + note + ", receiveUpdate=" + receiveUpdate + ", updateIntervalHr=" + updateIntervalHr
				+ ", updateFailureCount=" + updateFailureCount + "]";
	}

}