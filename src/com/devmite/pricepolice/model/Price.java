package com.devmite.pricepolice.model;

import java.math.BigDecimal;

public class Price {

	private BigDecimal value;
	private Currency currency;
	private String displayed;
	private String selector;

	public Price() {
		// TODO Auto-generated constructor stub
	}

	public Price(BigDecimal value, Currency currency) {
		super();
		this.value = value;
		this.currency = currency;
	}

	public Price(BigDecimal value, Currency currency, String displayed) {
		super();
		this.value = value;
		this.currency = currency;
		this.displayed = displayed;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getDisplayed() {
		return displayed;
	}

	public void setDisplayed(String displayed) {
		this.displayed = displayed;
	}

	public String getSelector() {
		return selector;
	}

	public void setSelector(String selector) {
		this.selector = selector;
	}

	@Override
	public String toString() {
		return "Price [value=" + value + ", currency=" + currency + ", displayed=" + displayed + ", selector=" + selector + "]";
	}

}
