package com.devmite.pricepolice.model;

public enum NotificationAction {
	PRICE_UPDATE, WONT_UPDATE, NO_PRICE_UPDATE;

	public static NotificationAction fromString(String str) {
		for (NotificationAction na : NotificationAction.values()) {
			if (na.name().equals(str)) {
				return na;
			}
		}
		return null;
	}
}
