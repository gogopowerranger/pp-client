package com.devmite.pricepolice.model;

public class GridItem {
	private String name, imageUrl;
	private int drawableId;

	public GridItem(String name, int drawableId) {
		this.name = name;
		this.drawableId = drawableId;
	}

	public GridItem(String name, String imageUrl) {
		this.name = name;
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getDrawableId() {
		return drawableId;
	}

	public void setDrawableId(int drawableId) {
		this.drawableId = drawableId;
	}

}
