package com.devmite.pricepolice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devmite.pricepolice.R;

public class PrimaryFragment extends Fragment {
	private ViewPager pager;
	private PagerAdapter pagerAdapter;
	private int activePosition = -1;

	private SparseArray<PinsFragment> pinsFragments;

	public static PrimaryFragment newInstance(int position) {
		PrimaryFragment fragment = new PrimaryFragment();
		Bundle args = new Bundle();
		args.putInt("activePosition", position);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("ws", "onCreate called in PrimaryFragment");
		activePosition = getArguments().getInt("activePosition", 0);
		setRetainInstance(true);
		pinsFragments = new SparseArray<PinsFragment>(3);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_primary, container, false);

		pager = (ViewPager) rootView.findViewById(R.id.pager);
		pagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
		pager.setAdapter(pagerAdapter);

		if (activePosition != -1) {
			pager.setCurrentItem(activePosition);
		}

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		for (int i = 0; i < 3; i++) {
			PinsFragment pf = pinsFragments.get(i);
			if (pf != null) {
				pf.onResume();
			}
		}
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		/**
		 * Remove this if we don't want to peek into the next slide in the pager
		 */
		// @Override
		// public float getPageWidth(int position) {
		// return 0.96f;
		// }

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(PinsFragment.QueryType.PUBLIC.getTitleResource());
			case 1:
				return getString(PinsFragment.QueryType.PRIMARY_BOARD.getTitleResource());
			default:
				return getString(PinsFragment.QueryType.FOLLOWED.getTitleResource());
			}
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			activePosition = position;
			PinsFragment pf = pinsFragments.get(position);
			if (pf == null) {
				switch (position) {
				case 0:
					pf = PinsFragment.newInstance(PinsFragment.QueryType.PUBLIC);
					break;
				case 1:
					pf = PinsFragment.newInstance(PinsFragment.QueryType.PRIMARY_BOARD);
					break;
				default:
					pf = PinsFragment.newInstance(PinsFragment.QueryType.FOLLOWED);
					break;
				}
				pinsFragments.append(position, pf);
			}
			return pf;
		}

		@Override
		public int getCount() {
			return 3;
		}
	}
}
