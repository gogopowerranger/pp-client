package com.devmite.pricepolice.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.activity.LoginActivity;
import com.devmite.pricepolice.activity.PrimaryActivity;
import com.devmite.pricepolice.helper.SessionManager;

public class GuideFragment extends Fragment {
	private int pageIndex = 0;
	private View rootView;
	private ViewGroup viewContainer;

	public static GuideFragment newInstance(int pageIndex) {
		GuideFragment fragment = new GuideFragment();
		Bundle args = new Bundle();
		args.putInt("pageIndex", pageIndex);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pageIndex = getArguments().getInt("pageIndex", 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Animation fadeIn = new AlphaAnimation(0, 1);
		// fadeIn.setInterpolator(new DecelerateInterpolator());
		// fadeIn.setDuration(300L);
		this.viewContainer = container;
		this.rootView = inflater.inflate(R.layout.fragment_guide, container, false);

		ImageView imageView = (ImageView) this.rootView.findViewById(R.id.g_image);
		// imageView.setAnimation(fadeIn);

		int bitmapResource = Integer.MIN_VALUE;
		switch (pageIndex) {
		case 1: // home
			bitmapResource = R.drawable.g_share;
			break;
		case 2: // share
			bitmapResource = R.drawable.g_catch;
			break;
		case 3: // catch
			bitmapResource = R.drawable.g_detail;
			break;
		case 4: // detail
			bitmapResource = R.drawable.g_search;
			break;
		default:
			bitmapResource = R.drawable.g_home;
			break;
		}
		imageView.setImageResource(bitmapResource);

		Button closeButton = (Button) this.rootView.findViewById(R.id.g_close);
		closeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				if (SessionManager.getInstance(getActivity()).isUserLoggedIn()) {
					intent.setClass(getActivity(), PrimaryActivity.class);
				} else {
					intent.setClass(getActivity(), LoginActivity.class);
				}
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				getActivity().finish();
			}
		});

		return this.rootView;
	}

	public View getRootView() {
		return this.rootView;
	}
	
	public ViewGroup getViewContainer() {
		return this.viewContainer;
	}
}
