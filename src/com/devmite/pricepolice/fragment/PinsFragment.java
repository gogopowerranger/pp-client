package com.devmite.pricepolice.fragment;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.adapter.PinGridAdapter;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.helper.RefreshRegistry;
import com.devmite.pricepolice.helper.RefreshRegistry.Type;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.PinRequest;
import com.devmite.pricepolice.model.http.PinResponse;

public class PinsFragment extends Fragment {
	OnPinDetailSelectedListener listener;

	// Primary Activity must implement this interface
	public interface OnPinDetailSelectedListener {
		public void onPinSelected(PinResponse pr);
	}

	public static enum QueryType {
		PUBLIC(R.string.title_public_pins),
		PRIMARY_BOARD(R.string.title_primary_pins),
		FOLLOWED(R.string.title_followed_pins);

		private final int titleResource;

		private QueryType(int titleResource) {
			this.titleResource = titleResource;
		}

		public int getTitleResource() {
			return titleResource;
		}
		
		public static QueryType fromOrdinal(int ordinal) {
			for(QueryType queryType : values()) {
				if(queryType.ordinal() == ordinal) {
					return queryType;
				}
			}
			
			return PUBLIC;
		}
	}

	private class ProgressTracker {
		private AtomicLong callerTimestamp = new AtomicLong(0L);
		private LayoutAnimationController hideLac, showLac;

		public ProgressTracker() {
			Animation show = AnimationUtils.loadAnimation(getActivity(), R.anim.top_down);
			show.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {

				}

				@Override
				public void onAnimationRepeat(Animation animation) {

				}

				@Override
				public void onAnimationEnd(Animation animation) {

				}
			});

			Animation hide = AnimationUtils.loadAnimation(getActivity(), R.anim.top_up);
			hide.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {

				}

				@Override
				public void onAnimationRepeat(Animation animation) {

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					progressArea.setVisibility(View.GONE);
				}
			});

			hideLac = new LayoutAnimationController(hide);
			hideLac.setDelay(0.5f);
			showLac = new LayoutAnimationController(show);
			showLac.setDelay(0);
		}

		private void startProgressBar(long callerTimestamp) {
			
			this.callerTimestamp.set(callerTimestamp);
			progressBar.setProgress(0);
			progressArea.setLayoutAnimation(showLac);
			// progressArea.startLayoutAnimation();
			progressArea.startAnimation(showLac.getAnimation());
		}

		private void stopProgressBar(long callerTimestamp) {
			if (this.callerTimestamp.get() == callerTimestamp) {
				progressBar.setProgress(100);
				progressArea.setLayoutAnimation(hideLac);
				// progressArea.startLayoutAnimation();
				progressArea.startAnimation(hideLac.getAnimation());
			}
		}

		private void updateProgressBar(int percentage, long callerTimestamp) {
			if (this.callerTimestamp.get() == callerTimestamp) {
				progressBar.setProgress(percentage);
			}
		}
	}

	private static final int itemQueryLimit = 16;

	private ValidationManager<Activity> validationManager;
	private LinearLayout progressArea;
	private ProgressBar progressBar;
	private ProgressTracker progressTracker;

	private GridView grid;
	private PinGridAdapter gridAdapter;

	private boolean loading = false;

	private BoardResponse referenceBoard;

	private QueryType queryType;
	private final Actions actions = new Actions();
	
	public static PinsFragment newInstance(QueryType queryType) {
		PinsFragment fragment = new PinsFragment();
		Bundle args = new Bundle();
		args.putInt("queryTypeOrdinal", queryType.ordinal());
		fragment.setArguments(args);
		
		return fragment;
	}

	public void setQueryType(QueryType queryType) {
		this.queryType = queryType;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int queryTypeOrdinal = getArguments().getInt("queryTypeOrdinal", 0);
		queryType = QueryType.fromOrdinal(queryTypeOrdinal);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.validationManager = new ValidationManager<Activity>(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i("ws", "onResume called in PinsFragment of type" + queryType.name());
		Log.i("ws", RefreshRegistry.INSTANCE.toString());
		if (RefreshRegistry.INSTANCE.check(Type.FOLLOW) && queryType == QueryType.FOLLOWED && !gridAdapter.isEmpty()) {
			Log.i("ws", "Resuming PinsGragment FOLLOWED");
			gridAdapter.clearItems();
			gridAdapter.notifyDataSetInvalidated();
			actions.onLoadMore(0);
			RefreshRegistry.INSTANCE.remove(Type.FOLLOW);
		} else if (RefreshRegistry.INSTANCE.check(Type.DELETE) && queryType == QueryType.PRIMARY_BOARD && !gridAdapter.isEmpty()) {
			Log.i("ws", "Resuming PinsGragment PRIMARY_BOARD");
			gridAdapter.clearItems();
			gridAdapter.notifyDataSetInvalidated();
			actions.onLoadMore(0);
			RefreshRegistry.INSTANCE.remove(Type.DELETE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_pins, container, false);

		progressArea = (LinearLayout) rootView.findViewById(R.id.progress_area);
		progressBar = (ProgressBar) progressArea.findViewById(R.id.progress_bar);
		progressTracker = new ProgressTracker();

		grid = (GridView) rootView.findViewById(R.id.gridview_test);
		gridAdapter = new PinGridAdapter(getActivity());
		grid.setAdapter(gridAdapter);
		grid.setOnItemClickListener(actions);
		grid.setOnScrollListener(actions);

		actions.onLoadMore(0);

		return rootView;
	}

	public void adjustTitle() {
		getActivity().setTitle(queryType.titleResource);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (OnPinDetailSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnPinDetailSelectedListener");
		}
	}

	private class Actions implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			PinResponse pr = (PinResponse) gridAdapter.getItem(position);
			listener.onPinSelected(pr);
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			if (!loading) {
				int totalItemScroll = firstVisibleItem + visibleItemCount;
				// Log.i("ws", "Total item scroll: " + totalItemScroll
				// + ", item limit: " + itemQueryLimit + ", modulus: "
				// + totalItemScroll % itemQueryLimit);
				if (totalItemScroll > 1 && totalItemCount == totalItemScroll
						&& totalItemScroll % itemQueryLimit == 0) {
					// Log.e("ws", "Scroll MaxId : " + gridAdapter.getCount());
					loading = true;
					onLoadMore(totalItemScroll);
				}
			}
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		public void onLoadMore(int totalItemsCount) {
			Log.i("ws", "loading more pins");
			Log.i("ws", "total items: " + totalItemsCount);

			switch (queryType) {
			case PRIMARY_BOARD:
				if (totalItemsCount == 0) {
					getPrimaryBoard();
				} else {
					getPinsByBoard(totalItemsCount);
				}
				break;
			case PUBLIC:
				getPublicPins(totalItemsCount);
				break;
			case FOLLOWED:
				getFollowedPins(totalItemsCount);
			}
		}
	}

	private void getPrimaryBoard() {
		final long ts = System.currentTimeMillis();
		progressTracker.startProgressBar(ts);
		progressTracker.updateProgressBar(10, ts);
		App.getRestClient().getAPIService().primaryBoard(new Callback<BaseRespone<BoardResponse>>() {

			@Override
			public void success(BaseRespone<BoardResponse> br, Response r) {
				progressTracker.updateProgressBar(50, ts);
				loading = false;
				if (br.getData() != null) {
					BoardResponse board = br.getData();
					PinsFragment.this.referenceBoard = board;

					getPinsByBoard(0);
				}
				progressTracker.stopProgressBar(ts);
			}

			@Override
			public void failure(RetrofitError re) {
				validationManager.handleError(re);
				progressTracker.stopProgressBar(ts);
			}
		});
	}

	private void getPinsByBoard(int offset) {
		final long ts = System.currentTimeMillis();
		if (this.referenceBoard != null) {
			progressTracker.startProgressBar(ts);
			PinRequest.ByBoard request = new PinRequest.ByBoard();
			request.setBoardId(this.referenceBoard.getBoardId());
			request.setLimit(itemQueryLimit);
			request.setOffset(offset);
			request.setMostFollowed(null);
			request.setLatest(true);
			progressTracker.updateProgressBar(10, ts);
			App.getRestClient().getAPIService().pinsByBoard(request, new Callback<BaseRespone<List<PinResponse>>>() {

				@Override
				public void success(BaseRespone<List<PinResponse>> br, Response r) {
					progressTracker.updateProgressBar(50, ts);
					loading = false;
					if (br.getData() != null) {
						List<PinResponse> prs = br.getData();
						appendPins(prs, 50, ts);
					}
					progressTracker.stopProgressBar(ts);
				}

				@Override
				public void failure(RetrofitError re) {
					validationManager.handleError(re);
					progressTracker.stopProgressBar(ts);
				}
			});
		} else {
			Toast.makeText(getActivity(), "Reference board not yet set", Toast.LENGTH_LONG).show();
		}
	}

	private void getPublicPins(int offset) {
		final long ts = System.currentTimeMillis();
		progressTracker.startProgressBar(ts);
		PinRequest.Public request = new PinRequest.Public();
		request.setLimit(itemQueryLimit);
		request.setOffset(offset);
		request.setMostFollowed(null);
		request.setLatest(true);
		progressTracker.updateProgressBar(10, ts);
		App.getRestClient()
				.getAPIService()
				.pinsPublic(request, new Callback<BaseRespone<List<PinResponse>>>() {

					@Override
					public void success(BaseRespone<List<PinResponse>> br, Response r) {
						progressTracker.updateProgressBar(50, ts);
						loading = false;
						if (br.getData() != null) {
							List<PinResponse> prs = br.getData();
							appendPins(prs, 50, ts);
						}
						progressTracker.stopProgressBar(ts);
					}

					@Override
					public void failure(RetrofitError re) {
						validationManager.handleError(re);
						progressTracker.stopProgressBar(ts);
					}
				});
	}

	private void getFollowedPins(int offset) {
		final long ts = System.currentTimeMillis();
		progressTracker.startProgressBar(ts);
		PinRequest.Followed request = new PinRequest.Followed();
		request.setLimit(itemQueryLimit);
		request.setOffset(offset);
		progressTracker.updateProgressBar(10, ts);
		App.getRestClient()
				.getAPIService()
				.pinsFollowed(request, new Callback<BaseRespone<List<PinResponse>>>() {

					@Override
					public void success(BaseRespone<List<PinResponse>> br, Response r) {
						progressTracker.updateProgressBar(50, ts);
						loading = false;
						if (br.getData() != null) {
							List<PinResponse> prs = br.getData();
							appendPins(prs, 50, ts);
						}
						progressTracker.stopProgressBar(ts);
					}

					@Override
					public void failure(RetrofitError re) {
						validationManager.handleError(re);
						progressTracker.stopProgressBar(ts);
					}
				});
	}

	private void appendPins(List<PinResponse> prs, int startingProgress, long callerTimestamp) {
		int progress = startingProgress;
		int size = prs.size();
		if (size > 0) {
			int progressStep = 100 - progress / size;
			for (PinResponse pr : prs) {
				gridAdapter.addItem(pr);
				progress += progressStep;
				progressTracker.updateProgressBar(progress, callerTimestamp);
			}
			gridAdapter.notifyDataSetChanged();
		}
	}
}
