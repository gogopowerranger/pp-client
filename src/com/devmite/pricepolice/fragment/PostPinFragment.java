package com.devmite.pricepolice.fragment;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.adapter.CurrencyAdapter;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.fragment.PinsFragment.OnPinDetailSelectedListener;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.Config.RequiresDesktop;
import com.devmite.pricepolice.helper.PriceService;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.model.Currency;
import com.devmite.pricepolice.model.Link;
import com.devmite.pricepolice.model.PinParseResult;
import com.devmite.pricepolice.model.Price;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.model.http.PostPinRequest;
import com.devmite.pricepolice.util.ImageButtonLoader;
import com.devmite.pricepolice.util.Utils;
import com.devmite.pricepolice.view.CheckableImageButton;
import com.devmite.pricepolice.view.CheckableImageButton.OnCheckedChangeListener;
import com.devmite.pricepolice.view.FlowLayout;

@SuppressLint({ "NewApi", "ShowToast" })
public class PostPinFragment extends Fragment implements OnClickListener {
	// UI elements
	private ProgressDialog progressDialog;
	private LinearLayout imagesLayout, hiddenLayout;
	private FlowLayout flowLayoutPrice;
	private EditText urlEdit, titleEdit, priceEdit;
	private Button processButton;
	private RadioGroup rgPrices, rgUpdate;
	private Spinner currencySpinner, updateSpinner;
	private List<CheckableImageButton> imageButtons;

	private ValidationManager validationManager;
	private PriceService priceService;

	private boolean processed = false;
	private OnPinDetailSelectedListener listener;

	private CurrencyAdapter currencyAdapter;

	private int[] updateValues = Config.UPDATE_VALUES;

	private int selectedImage = -1;

	private static final int MAX_IMAGE_NUMBER = 10;
	private static final String[] IMG_ATTRS = { "src", "l_src", "gd_src", "data-original" };

	String urlShared = "";

	public static PostPinFragment newInstance(String url) {
		PostPinFragment fragment = new PostPinFragment();
		Bundle args = new Bundle();
		args.putString("url", url);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			urlShared = getArguments().getString("url", "");
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);

		validationManager = new ValidationManager<Context>(getActivity());
		priceService = new PriceService(getActivity());
		priceService.initialize();

		View rootView = inflater.inflate(R.layout.fragment_pin, container,
				false);

		urlEdit = (EditText) rootView.findViewById(R.id.url);
		titleEdit = (EditText) rootView.findViewById(R.id.title);

		imagesLayout = (LinearLayout) rootView.findViewById(R.id.images_layout);
		hiddenLayout = (LinearLayout) rootView.findViewById(R.id.hidden_layout);
		hiddenLayout.setVisibility(View.INVISIBLE);

		flowLayoutPrice = (FlowLayout) rootView
				.findViewById(R.id.flow_layout_pin);
		processButton = (Button) rootView.findViewById(R.id.process_button);
		processButton.setOnClickListener(this);

		currencySpinner = (Spinner) rootView
				.findViewById(R.id.currency_spinner);
		TreeSet<String> allCurrenciesSorted = new TreeSet<String>(priceService.getCurrencies());
		currencyAdapter = new CurrencyAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item,
				allCurrenciesSorted.toArray(new String[allCurrenciesSorted.size()]));
		currencySpinner.setAdapter(currencyAdapter);

		updateSpinner = (Spinner) rootView
				.findViewById(R.id.update_interval_spinner);
		updateSpinner.setSelection(2);

		rgUpdate = (RadioGroup) rootView.findViewById(R.id.rg_update);
		rgUpdate.check(rgUpdate.getChildAt(YES).getId());
		rgUpdate.setOnCheckedChangeListener(new android.widget.RadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.radio_yes) {
					updateSpinner.setEnabled(true);
					updateSpinner.setClickable(true);
				} else if (checkedId == R.id.radio_no) {
					updateSpinner.setEnabled(false);
					updateSpinner.setClickable(false);
				}
			}
		});

		try {
			if (urlShared != null && Patterns.WEB_URL.matcher(urlShared).matches()) {
				urlEdit.setText(urlShared);
				processButton.performClick();
				urlShared = null;
			} else {
				ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(Activity.CLIPBOARD_SERVICE);

				ClipData clipData = clipboardManager.getPrimaryClip();
				Item clipItem = clipData.getItemAt(clipData.getItemCount() - 1);
				if (Patterns.WEB_URL.matcher(clipItem.getText()).matches()) {
					urlEdit.setText(clipItem.getText());
				}
			}
		} catch (Exception e) {
			Log.w("ws", e);
		}
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (OnPinDetailSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}

	}

	@Override
	public void onClick(View v) {
		if (v == processButton) {
			if (processed) {
				if (rgPrices == null) { // price not found
					if ("".equals(priceEdit.getText().toString())) {
						new AlertDialog.Builder(getActivity()).setTitle(R.string.incomplete_data)
								.setMessage(R.string.provide_price)
								.setIcon(android.R.drawable.ic_dialog_alert).show();
					} else {
						makePostPinRequest();
					}
				} else {
					makePostPinRequest();
				}
			} else {
				emptyOutElements();
				parse();

			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_post_pin, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_reset_post:
			resetUI();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void parse() {
		if (Utils.isBlank(urlEdit.getText().toString())) {
			validationManager.addError(hashCode(), getString(R.string.prompt_url_empty));
			validationManager.displayErrors();
		} else {
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setMessage(getResources().getString(
					R.string.dialog_processing));
			progressDialog.show();

			String url = urlEdit.getText().toString();
			new JSoupTask().execute(url);
		}
	}

	Document doc;
	String htmlText;
	PinParseResult masterResult = new PinParseResult();

	private class JSoupTask extends AsyncTask<String, Void, PinParseResult> {

		@SuppressLint("DefaultLocale")
		@Override
		protected PinParseResult doInBackground(String... params) {
			try {
				PinParseResult ppr = new PinParseResult();
				// Connect to the web site

				// HttpClient client = new DefaultHttpClient();
				// HttpGet request = new HttpGet(params[0]);
				// HttpResponse response = client.execute(request);
				// String html = EntityUtils.toString(response.getEntity());

				RequiresDesktop rd = RequiresDesktop.fromUrl(params[0]);
				Link link = new Link();
				link.setUrl(params[0]);
				link.setUserAgent(Config.USER_AGENT);
				if (rd != null) {
					link = rd.modify(link);
				}

				doc = Jsoup.connect(link.getUrl())
						.header("Proxy-Client-IP", "204.79.197.200")
						.header("X-Forwarded-For", "204.79.197.200")
						.header("WL-Proxy-Client-IP", "204.79.197.200")
						.header("HTTP_CLIENT_IP", "204.79.197.200")
						.header("HTTP_X_FORWARDED_FOR", "204.79.197.200")
						.userAgent(link.getUserAgent()).timeout(10000).followRedirects(true).get();
				// doc = Jsoup.parse(html);
				htmlText = doc.text();

				// Title
				ArrayList<String> titles = new ArrayList<String>(1);
				titles.add(doc.title());
				ppr.setTitles(titles);

				List<String> imageUrls = new ArrayList<String>();
				for (String imgAttr : IMG_ATTRS) {
					imageUrls.addAll(getImages(imgAttr));
					if (imageUrls.size() > 2) {
						break;
					}
				}
				ppr.setImageURLs(imageUrls);

				Set<Price> priceSet = new TreeSet<Price>(PriceComparator.INSTANCE);
				priceSet.addAll(searchPriceId());
				priceSet.addAll(searchPriceAtrribute());
				
				// if (priceSet.isEmpty()) {
				// priceSet.addAll(searchPriceNumericRegex());
				// }
				Iterator<Price> priceIterator = priceSet.iterator();
				while (priceIterator.hasNext()) {
					Price price = priceIterator.next();
					if (price.getValue().compareTo(BigDecimal.ZERO) <= 0) {
						priceIterator.remove();
					}
				}
				
				List<Price> priceList = new ArrayList<Price>(priceSet);
				ppr.setPrices(priceList);

				// searchFinal();

				return ppr;

			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(PinParseResult result) {
			try {
				if (result != null) {
					urlEdit.setEnabled(false);
					masterResult = result;
					hiddenLayout.setVisibility(View.VISIBLE);

					// Title
					titleEdit.setText(result.getTitles().get(0));

					// Price
					RadioButton rb;

					List<Price> prices = result.getPrices();
					if (!prices.isEmpty()) {
						rgPrices = new RadioGroup(getActivity());
						// rgPrices.setOrientation(RadioGroup.VERTICAL);

						boolean currencyFound = false;
						for (Price price : prices) {
							rb = new RadioButton(getActivity());

							rb.setBackgroundResource(R.drawable.radio_bg);
							// rb.setBackground(getResources().getDrawable(R.drawable.radio_bg));
							final int scale = Math.round(getResources().getDisplayMetrics().density);
							final boolean isHackNeeded = Build.VERSION.SDK_INT < 17;
							if (isHackNeeded) {
								rb.setPadding(30 * scale, 7 * scale, 5 * scale, 7 * scale);
							} else {
								rb.setPadding(0, 7 * scale, 5 * scale, 7 * scale);
							}

							String displayed = price.getDisplayed();
							if (displayed != null && !displayed.equals("")) {
								String priceStr = price.getValue().toPlainString();
								if (price.getCurrency() != null) {
									rb.setText(
											getString(R.string.price_line_with_currency,
													price.getCurrency().getSymbol(),
													priceStr,
													displayed));
								} else {
									rb.setText(getString(R.string.price_line, priceStr, displayed));
								}
							} else {
								rb.setText(String.valueOf(price.getValue()));
							}
							rgPrices.addView(rb);

							if (!currencyFound) { // find at least one currency,
													// set spinner to it, no
													// further attempts when
													// found
								int defaultCurrencyPosition;
								Currency currency = price.getCurrency();
								if (currency != null) {
									if ((defaultCurrencyPosition = currencyAdapter.findIndex(currency.getCodeISO(), Integer.MIN_VALUE)) != Integer.MIN_VALUE) {
										currencySpinner.setSelection(defaultCurrencyPosition);
										currencyFound = true;
									}
								} else {
									String defaultCurrencyKey = priceService.getDefaultCurrencyKey();
									if (defaultCurrencyKey != null) {
										if ((defaultCurrencyPosition = currencyAdapter.findIndex(defaultCurrencyKey, Integer.MIN_VALUE)) != Integer.MIN_VALUE) {
											currencySpinner.setSelection(defaultCurrencyPosition);
											currencyFound = true;
										}
									}
								}
							}
						}
						rgPrices.check(rgPrices.getChildAt(0).getId());
						flowLayoutPrice.addView(rgPrices);
					} else { // couldn't detect price
						if (priceEdit == null) {
							priceEdit = new EditText(getActivity());
							priceEdit.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
							TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
									1);
							priceEdit.setLayoutParams(params);
							// priceEdit.setBackground(getResources().getLayout(R.layout.textbox_set_middle));
							flowLayoutPrice.addView(priceEdit);
						}
					}

					imageButtons = new ArrayList<CheckableImageButton>();

					// Images
					ImageButtonLoader imageLoader = ImageButtonLoader
							.getInstance(getActivity());

					CheckableImageButton imageButton = null;
					if (result.getImageURLs() != null
							&& result.getImageURLs().size() > 0) {
						for (int i = 0; i < result.getImageURLs().size(); i++) {
							imageButton = new CheckableImageButton(getActivity());
							imageButton.setBackgroundResource(R.drawable.checkable_image_selector);
							// imageButton.setBackground(getResources().getDrawable(R.drawable.checkable_image_selector));
							imageButton.setScaleType(ScaleType.FIT_CENTER);
							imageButton.setPadding(10, 10, 10, 10);

							if (i == 0) {
								imageButton.performClick();
								selectedImage = 0;
							}
							imagesLayout.addView(imageButton);
							imageButtons.add(imageButton);

							android.view.ViewGroup.LayoutParams layoutParams = imageButton
									.getLayoutParams();
							layoutParams.width = 200;
							layoutParams.height = 200;
							imageButton.setLayoutParams(layoutParams);

							imageButton.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_launcher));
							imageLoader.displayImage(result.getImageURLs().get(i),
									imageButton);
						}
					}

					for (CheckableImageButton button : imageButtons) {
						button.setOnCheckedChangeListener(new OnCheckedChangeListener() {
							@Override
							public void onCheckedChanged(
									CheckableImageButton button, boolean isChecked) {
								if (isChecked)
									processCheckableImageButtonClick(button);
							}
						});
					}

					processed = true;
					processButton.setText(getActivity().getString(R.string.pin));
				} else {
					new AlertDialog.Builder(getActivity()).setTitle(R.string.search_no_result)
							.setMessage(R.string.unable_process)
							.setIcon(android.R.drawable.ic_dialog_alert).show();

				}
			} finally {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		}
	}

	private ArrayList<String> getImages(String imgAttr) {
		ArrayList<String> imageURLs = new ArrayList<String>();

		// Why height? because banners mostly are landscape (width >
		// height) so by choosing height as parameter we want to make
		// sure banner doesn't count in this image parsing

		// int[] largestHeights = new int[] { 0, 0, 0 };
		int largestHeight = 0;

		// Get all elements with img tag
		// Elements img = doc.getElementsByTag("img");

		// Get all elements with img tag for .jpg items
		Log.i("ws", "getImages - " + imgAttr);
		Elements img = doc.select("img[" + imgAttr + "*=.jpg]");

		// Get the largest jpg
		int x = 0, pos = -1;
		for (Element el : img) {
			String src = el.absUrl(imgAttr);
			Log.i("ws", "imgsrc: " + src);
			if (x < MAX_IMAGE_NUMBER) {
				imageURLs.add(src);
			}
			String height = el.attr("height");
			if (height != null && !height.equals("")) {
				int hgtInt = 0;
				if (height.matches("\\d+")) {
					hgtInt = Integer.parseInt(height);
				} else if (height.toLowerCase().endsWith("px")) {
					height = height.substring(0, height.length()
							- new String("px").length());
					hgtInt = Integer.parseInt(height);
				}
				if (hgtInt > largestHeight) {
					largestHeight = hgtInt;
					pos = x;
				}
			}
			x++;
		}
		if (pos != -1) {
			String src = img.get(pos).absUrl(imgAttr);
			if (!imageURLs.contains(src))
				imageURLs.add(0, src);
		}

		return imageURLs;
	}

	private enum PriceComparator implements Comparator<Price> {
		INSTANCE;
		@Override
		public int compare(Price lhs, Price rhs) {
			if (lhs.getValue() != null && rhs.getValue() != null) {
				return lhs.getValue().compareTo(rhs.getValue());
			} else {
				return lhs.getDisplayed().compareTo(rhs.getDisplayed());
			}
		}
	}

	private void describePriceSet(String prefix, Set<Price> priceSet) {
		StringBuilder sb = new StringBuilder(prefix + " results: ");
		for (Price price : priceSet) {
			sb.append("\n").append(" --").append(price.toString());
		}
		Log.i("ws", sb.toString());
	}

	private Set<Price> searchPriceId() {
		Set<Price> priceSet = new TreeSet<Price>(PriceComparator.INSTANCE);
		Log.i("ws", "searchPriceId");
		Elements elements = doc.body().select("*[id*=price]:not(:empty)");
		for (Element element : elements) {
			String text = element.text();
			Log.i("ws", "-- Candidate: " + text);
			try {
				Price price = priceService.find2(text);
				if (price != null) {
					price.setDisplayed(text);
					price.setSelector(element.cssSelector());

					priceSet.add(price);
				}
			} catch (Exception e) {
				Log.w("ws", e);
			}
		}
		describePriceSet("searchPriceId", priceSet);
		return priceSet;
	}

	private Set<Price> searchPriceAtrribute() {
		Set<Price> priceSet = new TreeSet<Price>(PriceComparator.INSTANCE);
		Log.i("ws", "searchPriceAtrribute");
		for (Element element : doc.body().select("*:not(:empty)")) {
			for (Attribute attribute : element.attributes()) {
				String attValue = attribute.getValue().toLowerCase();

				if (attValue.contains("price")) {

					String text = element.text();
					Log.i("ws", "-- Candidate: " + text);
					// if (Utils.isNumericRegex(text)) {
					// // avoid duplicates
					// if (!listResult.contains(text)) {
					// listResult.add(text);
					// Log.i("ws",
					// "CSS Selector: "
					// + element.cssSelector());
					// listSelector.add(element.cssSelector());
					// }
					// }
					try {
						Price price = priceService.find2(text);
						if (price != null) {
							price.setDisplayed(text);
							price.setSelector(element.cssSelector());

							priceSet.add(price);
						}
					} catch (Exception e) {
						Log.w("ws", e);
					}

					// for (int j = 0; j < currencyNotations.length; j++) {
					// try {
					// if
					// (text.toLowerCase().startsWith(currencyNotations[j].toLowerCase()))
					// {
					// int currencyPosition =
					// text.toLowerCase().indexOf(currencyNotations[j].toLowerCase());
					// text = text.substring(currencyPosition);
					// ParseResult pr = new ParseResult(text,
					// element.cssSelector());
					// Log.i("ws", pr.toString());
					// results.add(pr);
					// } else if
					// (text.toLowerCase().startsWith(currencyCodes[j].toLowerCase()))
					// {
					// int currencyPosition =
					// text.toLowerCase().indexOf(currencyCodes[j].toLowerCase());
					// text = text.substring(currencyPosition);
					// ParseResult pr = new ParseResult(text,
					// element.cssSelector());
					// Log.i("ws", pr.toString());
					// results.add(pr);
					// }
					// } catch (Exception e) {
					// Log.w("ws", e);
					// }
					// }
				}
			}
		}
		describePriceSet("searchPriceAtrribute", priceSet);
		return priceSet;
	}

	private Set<Price> searchPriceNumericRegex() {
		Set<Price> priceSet = new TreeSet<Price>(PriceComparator.INSTANCE);
		Log.i("ws", "searchPriceNumericRegex");
		// -------------------- cari elemen yang match regex numerik
		// START
		String cssQuery = "*:matchesOwn(([a-z|A-Z|\\s]){0,4}\\d+([.,]\\d{1,2})?)";
		Elements elements = doc.select(cssQuery);

		for (Element element : elements) {
			String text = element.text();
			Log.i("ws", "-- Candidate: " + element.toString());

			try {
				Price price = priceService.find2(text);
				if (price != null) {
					price.setDisplayed(text);
					price.setSelector(element.cssSelector());

					priceSet.add(price);
				}
			} catch (Exception e) {
				Log.w("ws", e);
			}
		}
		describePriceSet("searchPriceAtrribute", priceSet);
		return priceSet;
	}

	// private void searchFinal() {
	// if (!priceSet.isEmpty())
	// return;
	//
	// for (int j = 0; j < currencyNotations.length; j++) {
	//
	// if (htmlText.indexOf(currencyNotations[j]) != -1) {
	// int currencyPosition = htmlText
	// .indexOf(currencyNotations[j]);
	// String suspect = htmlText.substring(currencyPosition,
	// currencyPosition + 20);
	// char curChar = suspect.charAt(0);
	// // while(curChar!=)
	// } else if (htmlText.indexOf(currencyCodes[j]) != -1) {
	// int currencyPosition = htmlText
	// .indexOf(currencyNotations[j]);
	// String suspect = htmlText.substring(currencyPosition,
	// currencyPosition + 20);
	// }
	// }
	// }

	private String removeTrailingPeriod(String currency, String text) {
		// remove (.) that sometimes trailing currency

		char c = text.charAt(currency.length());
		if (c == '.') {
			text = text.substring(currency.length() + 1);
		} else {
			text = text.substring(currency.length());
		}
		return text;
	}

	final int YES = 0;
	final int NO = 1;

	private void makePostPinRequest() {
		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setMessage(getResources().getString(
				R.string.dialog_sending));
		progressDialog.show();

		String url = urlEdit.getText().toString();
		RequiresDesktop rd = RequiresDesktop.fromUrl(url);
		Link link = new Link();
		link.setUrl(url);
		link.setUserAgent(Config.USER_AGENT);
		if (rd != null) {
			link = rd.modify(link);
		}

		PostPinRequest request = new PostPinRequest();

		request.setLabel(titleEdit.getText().toString());
		request.setPage_url(link.getUrl());
		if (masterResult.getImageURLs().size() > 0) {
			if (selectedImage != -1) {
				request.setImage_url(masterResult.getImageURLs().get(selectedImage));
			}
		}

		int rbUpdateId = rgUpdate.getCheckedRadioButtonId();
		int idxUpdate = rgUpdate.indexOfChild(rgUpdate.findViewById(rbUpdateId));
		if (idxUpdate == YES) {
			request.setReceive_update(true);
			request.setUpdate_interval(updateValues[updateSpinner.getSelectedItemPosition()]);
		} else {
			request.setReceive_update(false);
		}

		String currencyCode = currencyAdapter.getItem(currencySpinner.getSelectedItemPosition());
		if (currencyCode != null) {
			request.setCurrency_code(currencyCode.toUpperCase());
		}
		request.setUser_agent(link.getUserAgent());

		if (rgPrices != null) {
			// get checked radio button's index
			int rbCheckedId = rgPrices.getCheckedRadioButtonId();
			View rbChecked = rgPrices.findViewById(rbCheckedId);
			int idxPrice = rgPrices.indexOfChild(rbChecked);

			Log.i("ws", "idxPrice: " + idxPrice);
			Price checkedPrice = masterResult.getPrices().get(idxPrice);
			request.setPrice(checkedPrice.getValue().toPlainString());
			request.setPrice_selector(checkedPrice.getSelector());
		} else {
			request.setPrice(priceEdit.getText().toString());
		}

		long boardId = Utils.getLongPreferences(getActivity(),
				Config.KEY_PREF_PRIMARY_BOARD, -1L);
		if (boardId != -1L) {
			request.setBoard_id(boardId);
			postPin(request);
		} else {
			getPrimaryBoardAndPost(request);
		}
	}

	@SuppressLint("ShowToast")
	private void postPin(PostPinRequest request) {
		App.getRestClient().getAPIService()
				.postPin(request, new Callback<BaseRespone<PinResponse>>() {

					@Override
					public void success(BaseRespone<PinResponse> br, Response r) {
						progressDialog.dismiss();
						PinResponse pr = br.getData();
						Toast.makeText(getActivity(),
								getResources().getString(R.string.pin_posted),
								Toast.LENGTH_SHORT);

						resetUI();
						listener.onPinSelected(pr);
					}

					@Override
					public void failure(RetrofitError re) {
						progressDialog.dismiss();
						try {
							CommonResponse error = (CommonResponse) re
									.getBodyAs(CommonResponse.class);
							validationManager.addError(error.getInternalCode(),
									error.getMessage());
							validationManager.displayErrors();
						} catch (Exception e) {
							Log.e("ws", re.getMessage(), e);
						}
					}
				});
	}

	private void getPrimaryBoardAndPost(final PostPinRequest request) {
		App.getRestClient().getAPIService()
				.primaryBoard(new Callback<BaseRespone<BoardResponse>>() {
					@Override
					public void success(BaseRespone<BoardResponse> br,
							Response r) {
						if (br.getData() != null) {
							BoardResponse board = br.getData();
							Utils.saveIntPreferences(getActivity(),
									Config.KEY_PREF_PRIMARY_BOARD,
									(int) board.getBoardId());
							request.setBoard_id(board.getBoardId());
							postPin(request);
						}
					}

					@Override
					public void failure(RetrofitError re) {
						validationManager.handleError(re);
					}
				});
	}

	private void processCheckableImageButtonClick(
			CheckableImageButton buttonView) {
		for (CheckableImageButton button : imageButtons) {
			if (button != buttonView)
				button.setChecked(false);
		}
		selectedImage = imageButtons.indexOf(buttonView);
	}

	private void resetUI() {
		processed = false;

		urlEdit.setText("");
		urlEdit.setEnabled(true);
		titleEdit.setText("");
		emptyOutElements();
		hiddenLayout.setVisibility(View.INVISIBLE);
		processButton.setText(R.string.process);
	}

	private void emptyOutElements() {
		flowLayoutPrice.removeAllViews();
		imagesLayout.removeAllViews();
	}
}
