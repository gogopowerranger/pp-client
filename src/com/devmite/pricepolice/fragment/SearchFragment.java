package com.devmite.pricepolice.fragment;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.adapter.PinGridAdapter;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.fragment.PinsFragment.OnPinDetailSelectedListener;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.PinRequest;
import com.devmite.pricepolice.model.http.PinResponse;

@SuppressLint("ShowToast")
public class SearchFragment extends Fragment implements OnClickListener {
	private GridView grid;
	private PinGridAdapter gridAdapter;
	private ImageButton buttonSearch;
	private EditText searchEdit;
	private final Actions actions = new Actions();
	private OnPinDetailSelectedListener listener;
	private boolean loading = false;
	private static final int itemQueryLimit = 20;

	private View progressArea;
	private ProgressBar progressBar;
	private ProgressTracker progressTracker;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_search, container,
				false);

		searchEdit = (EditText) rootView.findViewById(R.id.search_edit);

		buttonSearch = (ImageButton) rootView.findViewById(R.id.button_search);
		buttonSearch.setOnClickListener(this);

		progressArea = rootView.findViewById(R.id.progress_area);
		progressBar = (ProgressBar) progressArea
				.findViewById(R.id.progress_bar);
		progressTracker = new ProgressTracker();

		progressArea.setVisibility(View.INVISIBLE);

		grid = (GridView) rootView.findViewById(R.id.search_gridview);
		gridAdapter = new PinGridAdapter(getActivity());
		grid.setAdapter(gridAdapter);
		grid.setOnItemClickListener(actions);
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (OnPinDetailSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnPinDetailSelectedListener");
		}
	}

	private class Actions implements AdapterView.OnItemClickListener,
			AbsListView.OnScrollListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			PinResponse pr = (PinResponse) gridAdapter.getItem(position);
			listener.onPinSelected(pr);
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			if (!loading) {
				int totalItemScroll = firstVisibleItem + visibleItemCount;
				if (totalItemScroll > 1 && totalItemCount == totalItemScroll
						&& totalItemScroll % itemQueryLimit == 0) {
					// Log.e("ws", "Scroll MaxId : " + gridAdapter.getCount());
					loading = true;
					onLoadMore(totalItemScroll);
				}
			}
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		public void onLoadMore(int totalItemsCount) {
			Log.i("ws", "loading more pins");
			Log.i("ws", "total items: " + totalItemsCount);

		}
	}

	private void searchPins(int offset, String keyword) {
		progressArea.setVisibility(View.VISIBLE);

		final long ts = System.currentTimeMillis();
		progressTracker.startProgressBar(ts);
		progressTracker.updateProgressBar(10, ts);

		PinRequest.Search request = new PinRequest.Search();
		request.setLimit(itemQueryLimit);
		request.setOffset(offset);
		request.setKeyword(keyword);

		App.getRestClient()
				.getAPIService()
				.pinsSearch(request,
						new Callback<BaseRespone<List<PinResponse>>>() {

							@Override
							public void success(
									BaseRespone<List<PinResponse>> br,
									Response r) {
								progressTracker.updateProgressBar(50, ts);
								loading = false;
								if (br.getData() != null) {
									List<PinResponse> prs = br.getData();
									appendPins(prs, 50, ts);
								}
								progressTracker.stopProgressBar(ts);
							}

							@Override
							public void failure(RetrofitError re) {
								// validationManager.handleError(re);
								progressTracker.stopProgressBar(ts);
							}
						});
	}

	private void appendPins(List<PinResponse> prs, int startingProgress,
			long callerTimestamp) {
		int progress = startingProgress;
		int size = prs.size();
		if (size > 0) {
			int progressStep = 100 - progress / size;
			for (PinResponse pr : prs) {
				gridAdapter.addItem(pr);
				progress += progressStep;
				progressTracker.updateProgressBar(progress, callerTimestamp);
			}
			gridAdapter.notifyDataSetChanged();
		} else {
			new AlertDialog.Builder(getActivity()).setTitle(R.string.search_no_result)
					.setMessage(R.string.search_zero_result)
					.setIcon(android.R.drawable.ic_dialog_alert).show();
		}
	}

	@Override
	public void onClick(View v) {
		if (v == buttonSearch) {
			gridAdapter.clearItems();
			gridAdapter.notifyDataSetChanged();
			searchPins(0, searchEdit.getText().toString());
		}
	}

	private class ProgressTracker {
		private AtomicLong callerTimestamp = new AtomicLong(0L);

		private void startProgressBar(long callerTimestamp) {
			if (this.callerTimestamp.compareAndSet(0L, callerTimestamp)) {
				progressBar.setProgress(0);
				try {
					Animation animation = AnimationUtils.loadAnimation(
							getActivity(), R.anim.top_down);
					animation.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							progressArea.setVisibility(View.VISIBLE);
						}
					});
					progressArea.startAnimation(animation);
				} catch (Exception e) {
					Log.w("ws", e);
				}
			}
		}

		private void stopProgressBar(long callerTimestamp) {
			if (this.callerTimestamp.get() == callerTimestamp) {
				progressBar.setProgress(100);
				try {
					Animation animation = AnimationUtils.loadAnimation(
							getActivity(), R.anim.top_up);
					animation.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							progressArea.setVisibility(View.GONE);
							ProgressTracker.this.callerTimestamp.set(0L);
						}
					});
					SearchFragment.this.progressArea.startAnimation(animation);
				} catch (Exception e) {
					Log.w("ws", e);
				}
			}
		}

		private void updateProgressBar(int percentage, long callerTimestamp) {
			if (this.callerTimestamp.get() == callerTimestamp) {
				progressBar.setProgress(percentage);
			}
		}
	}
}
