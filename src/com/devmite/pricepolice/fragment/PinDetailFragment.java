package com.devmite.pricepolice.fragment;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.RefreshRegistry;
import com.devmite.pricepolice.helper.RefreshRegistry.Type;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PinDeleteRequest;
import com.devmite.pricepolice.model.http.PinFollowRequest;
import com.devmite.pricepolice.model.http.PinFollowResponse;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.model.http.UserResponse;
import com.devmite.pricepolice.rest.ResponseHandler;
import com.devmite.pricepolice.rest.ResponseHandlerWithCleanup;
import com.devmite.pricepolice.rest.RestClient;
import com.devmite.pricepolice.rest.service.APIService;
import com.devmite.pricepolice.util.ImageLoader;
import com.devmite.pricepolice.util.Utils;
import com.devmite.pricepolice.view.TwoLineEntry;

public class PinDetailFragment extends Fragment {
	private static final DateFormat dateFormat = new SimpleDateFormat("h a, d MMMM yyyy");
	private static AtomicLong lastPriceUpdateClick = new AtomicLong(0L);

	private View rootView;
	private PinResponse pin;
	private ImageLoader imageLoader;
	private ImageView imagePin;
	private Button followUnfollowButton;
	private Button updatePriceButton;

	private TwoLineEntry ownerEntry;

	private ValidationManager<Activity> validationManager;
	private APIService apiService;

	private UnfollowClickListener unfollowClickListener;
	private FollowClickListener followClickListener;

	public static Fragment newInstance(PinResponse pin) {
		PinDetailFragment fragment = new PinDetailFragment();

		Bundle args = new Bundle();
		args.putParcelable("pin", pin);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		Log.i("ws", "onCreateOptionsMenu in PinDetailFragment called");
		if (validationManager.isOwned(pin)) {
			inflater.inflate(R.menu.menu_pin_detail, menu);
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_pin_delete:
			deletePin();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.i("ws", "onAttach in PinDetailFragment called");

		Bundle args = getArguments();
		pin = (PinResponse) args.getParcelable("pin");

		this.validationManager = new ValidationManager<Activity>(activity);
		this.followClickListener = new FollowClickListener();
		this.unfollowClickListener = new UnfollowClickListener();
		this.apiService = App.getRestClient().getAPIService();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i("ws", "onActivityCreated in PinDetailFragment called");

		this.validationManager = new ValidationManager<Activity>(getActivity());
		this.followClickListener = new FollowClickListener();
		this.unfollowClickListener = new UnfollowClickListener();
		this.apiService = App.getRestClient().getAPIService();

		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i("ws", "onCreateView in PinDetailFragment called");
		rootView = inflater.inflate(R.layout.fragment_pin_detail2, container, false);

		imagePin = (ImageView) rootView.findViewById(R.id.image_pin_detail);
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator());
		fadeIn.setDuration(300L);
		imagePin.setAnimation(fadeIn);
		rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				imageLoader = ImageLoader.getInstance(getActivity());
				if (!Utils.isBlank(pin.getImageUrl())) {
					Bitmap imageBmp = imageLoader.getBitmap(pin.getImageUrl());
					float imageWidth = imageBmp.getWidth();
					float imageHeight = imageBmp.getHeight();
					float max = rootView.getMeasuredWidth();
					float maxHalf = max / 2;

					Log.i("ws", "max width: " + max);
					Log.i("ws", "width: " + imageWidth);
					Log.i("ws", "height: " + imageHeight);

					if (imageHeight > max || imageWidth > max) {
						float newHeight = max;
						float newWidth = (max / imageHeight) * imageWidth;
						imageBmp = Bitmap.createScaledBitmap(imageBmp, (int) newWidth, (int) newHeight, true);
					} else if (imageHeight < maxHalf || imageWidth < maxHalf) {
						float newWidth = maxHalf;
						float newHeight = (maxHalf / imageWidth) * imageHeight;
						imageBmp = Bitmap.createScaledBitmap(imageBmp, (int) newWidth, (int) newHeight, true);
					}

					imagePin.setImageBitmap(imageBmp);
				} else {
					imageLoader.displayImage("", imagePin);
				}
				rootView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
		// wrapper.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
		//
		// @Override
		// public void onLayoutChange(View v, int left, int top, int right, int
		// bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
		// int imageWidth = imagePin.getMeasuredWidth();
		// int imageHeight = imagePin.getMeasuredHeight();
		// int max = wrapper.getMeasuredWidth();
		//
		// Log.i("ws", "max width: " + wrapper.getMeasuredWidth());
		// Log.i("ws", "width: " + imagePin.getMeasuredWidth());
		// Log.i("ws", "height: " + imagePin.getMeasuredHeight());
		//
		// if (imageHeight > max || imageWidth > max) {
		// imagePin.getLayoutParams().height = max;
		// imagePin.requestLayout();
		// }
		//
		// int maxHalf = max/2;
		// if (imageHeight < maxHalf || imageWidth < maxHalf) {
		// imagePin.getLayoutParams().height = maxHalf;
		// imagePin.requestLayout();
		// }
		//
		// wrapper.removeOnLayoutChangeListener(this);
		// }
		// });

		TwoLineEntry labelEntry = (TwoLineEntry) rootView.findViewById(R.id.label);
		labelEntry.getLayoutView().setGravity(Gravity.CENTER);
		labelEntry.getLabelView().setGravity(Gravity.CENTER);
		labelEntry.getSubtitleView().setGravity(Gravity.CENTER);
		labelEntry.setLabel(pin.getLabel());
		labelEntry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pin.getPageUrl()));
				startActivity(intent);
			}
		});

		TwoLineEntry noteEntry = (TwoLineEntry) rootView.findViewById(R.id.note);
		noteEntry.setLabel(pin.getNote());
		if (Utils.isBlank(pin.getNote())) {
			noteEntry.setVisibility(View.GONE);
		}

		String latestPrice = pin.getLatestPrice().trim();
		String initialPrice = pin.getPrice().trim();

		BigDecimal latestPriceBd = BigDecimal.ZERO;
		BigDecimal initialPriceBd = BigDecimal.ZERO;

		TwoLineEntry priceLatestEntry = (TwoLineEntry) rootView.findViewById(R.id.price_latest);

		if (!Utils.isBlank(latestPrice)) {
			latestPriceBd = new BigDecimal(latestPrice);
			latestPrice = Utils.formatPrice(pin.getCurrency(), latestPriceBd);
			priceLatestEntry.setLabel(latestPrice);
			// if (!Utils.isBlank(pin.getCurrency()) &&
			// Character.isDigit(latestPrice.charAt(0))) {
			// priceLatestEntry.setLabel(pin.getCurrency() + " " + latestPrice);
			// } else {
			// priceLatestEntry.setLabel(latestPrice);
			// }
			if (pin.getLatestPriceTs() != 0) {
				priceLatestEntry.setSubtitle("Last update: " + dateFormat.format(new Date(pin.getLatestPriceTs())));
			}
		}

		TwoLineEntry priceInitialEntry = (TwoLineEntry) rootView.findViewById(R.id.price_initial);

		if (!Utils.isBlank(initialPrice)) {
			// Toast.makeText(getActivity(), "Initial price: [" + initialPrice +
			// "], latest price: [" + latestPrice + "]",
			// Toast.LENGTH_LONG).show();
			initialPriceBd = new BigDecimal(initialPrice);
			if (initialPriceBd.compareTo(latestPriceBd) != 0) {
				initialPrice = Utils.formatPrice(pin.getCurrency(), initialPriceBd);
				priceInitialEntry.setLabel(initialPrice);
				// if (!Utils.isBlank(pin.getPrice()) &&
				// Character.isDigit(initialPrice.charAt(0))) {
				// priceInitialEntry.setLabel(pin.getCurrency() + " " +
				// initialPrice);
				// } else {
				// priceInitialEntry.setLabel(initialPrice);
				// }
				if (pin.getCreateTs() != 0) {
					priceInitialEntry.setSubtitle("Initially: " + dateFormat.format(new Date(pin.getCreateTs())));
				}
			} else {
				priceInitialEntry.setVisibility(View.GONE);
			}
		}

		TwoLineEntry boardEntry = (TwoLineEntry) rootView.findViewById(R.id.label_board);
		adjustBoard(boardEntry);

		this.ownerEntry = (TwoLineEntry) rootView.findViewById(R.id.label_user);
		adjustPinOwner(this.ownerEntry);

		TwoLineEntry updateIntervalEntry = (TwoLineEntry) rootView.findViewById(R.id.label_update_interval);
		int updateIntervalHr = pin.getUpdateIntervalHr();
		if (updateIntervalHr > 0) {
			updateIntervalEntry.setLabel(updateIntervalHr + " hour(s)");
		} else {
			updateIntervalEntry.setVisibility(View.GONE);
		}

		if (pin.getUpdateFailureCount() > 0) {
			TextView updateFailureView = (TextView) rootView.findViewById(R.id.msg_update_failure);
			String msg = String.format(getString(R.string.msg_pin_update_failure), pin.getUpdateFailureCount());
			updateFailureView.setText(msg);
			updateFailureView.setVisibility(View.VISIBLE);
		}

		followUnfollowButton = (Button) rootView.findViewById(R.id.button_follow_unfollow);
		adjustFollowUnfollow();

		updatePriceButton = (Button) rootView.findViewById(R.id.button_update_price);
		updatePriceButton.setOnClickListener(new UpdatePriceClickListener());
		if (pin.getUserId() == SessionManager.getInstance(getActivity()).getUserId()) {
			updatePriceButton.setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	private void adjustBoard(final TwoLineEntry boardEntry) {
		boardEntry.setLabel(getString(R.string.loading));
		apiService.board(pin.getBoardId(),
				validationManager.getHandledErrorCallback(new ResponseHandler<BaseRespone<BoardResponse>>() {
					@Override
					public void handle(BaseRespone<BoardResponse> response) {
						if (response.getData() != null) {
							BoardResponse bor = response.getData();
							boardEntry.setLabel(bor.getLabel());
						}
					}
				}));
	}

	private void adjustPinOwner(final TwoLineEntry ownerEntry) {
		if (validationManager.isOwned(pin)) {
			ownerEntry.setLabel(getString(R.string.pin_owned));
		} else {
			ownerEntry.setLabel(getString(R.string.loading));
			apiService.user(pin.getUserId(),
					validationManager.getHandledErrorCallback(new ResponseHandler<BaseRespone<UserResponse>>() {
						@Override
						public void handle(BaseRespone<UserResponse> response) {
							if (response.getData() != null) {
								UserResponse ur = response.getData();
								ownerEntry.setLabel(ur.getUsername());
							}
						}
					}));
		}

		apiService.avatar(pin.getUserId(), new Callback<BaseRespone<String>>() {

			@Override
			public void success(BaseRespone<String> br, Response r) {
				String avatar = br.getData();
				if (!Utils.isBlank(avatar)) {
					new RefreshProfileImageTask().execute(avatar);
				}
			}

			@Override
			public void failure(RetrofitError re) {
				Log.w("ws", re);
			}
		});
	}

	private void adjustFollowUnfollow() {
		if (pin.getUserId() == SessionManager.getInstance(getActivity()).getUserId()) {
			followUnfollowButton.setVisibility(View.GONE);
		} else {
			followUnfollowButton.setText(getString(R.string.loading));
			apiService.pinIsFollowed(pin.getPinId(),
					validationManager.getHandledErrorCallback(new ResponseHandler<BaseRespone<Boolean>>() {
						@Override
						public void handle(BaseRespone<Boolean> br) {
							if (br.getData() != null) {
								boolean followed = br.getData();
								if (followed) {
									followUnfollowButton.setText(getString(R.string.unfollow));
									followUnfollowButton.setOnClickListener(unfollowClickListener);
								} else {
									followUnfollowButton.setText(getString(R.string.follow));
									followUnfollowButton.setOnClickListener(followClickListener);
								}
							}
						}
					}));
		}
	}

	private void deletePin() {
		{
			String title = getString(R.string.delete_catch_really_title, pin.getLabel());
			String message = getString(R.string.delete_catch_really);
			if (pin.getStatistics() != null) {
				PinResponse.Statistics stats = pin.getStatistics();
				if (stats.getFollowers() > 1) {
					message = getString(R.string.delete_catch_really_follower_count, stats.getFollowers());
				}
			}
			new AlertDialog.Builder(getActivity()).setTitle(title)
					.setMessage(message)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setCancelable(true)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton(R.string.ok, new AlertDialog.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							final ProgressDialog progressDialog = new ProgressDialog(getActivity());
							progressDialog.setCancelable(false);
							progressDialog.setCanceledOnTouchOutside(false);
							progressDialog.setMessage(getResources().getString(R.string.dialog_processing));
							progressDialog.show();

							PinDeleteRequest request = new PinDeleteRequest();
							request.setPinId(pin.getPinId());
							apiService.pinDelete(request,
									validationManager.getHandledErrorCallback(new ResponseHandlerWithCleanup<CommonResponse>() {
										@Override
										public void handle(CommonResponse response) {
											Toast.makeText(getActivity(), getString(R.string.deleted_catch, pin.getLabel()),
													Toast.LENGTH_LONG).show();
										}

										@Override
										public void cleanup(boolean successful) {
											progressDialog.dismiss();
											if (successful) {
												RefreshRegistry.INSTANCE.add(Type.DELETE);
												getFragmentManager().popBackStack();
											}
										}
									}));
						}
					})
					.show();
		}
	}

	private class FollowClickListener implements View.OnClickListener {

		@Override
		public void onClick(final View v) {
			PinFollowRequest request = new PinFollowRequest();
			request.setPinId(pin.getPinId());
			apiService.follow(request,
					validationManager.getHandledErrorCallback(new ResponseHandler<BaseRespone<PinFollowResponse>>() {
						@Override
						public void handle(BaseRespone<PinFollowResponse> br) {
							followUnfollowButton.setText(getString(R.string.unfollow));
							followUnfollowButton.setOnClickListener(unfollowClickListener);
							RefreshRegistry.INSTANCE.add(Type.FOLLOW);
						}
					}));
		}
	}

	private class UnfollowClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			PinFollowRequest request = new PinFollowRequest();
			request.setPinId(pin.getPinId());
			apiService.unfollow(request,
					validationManager.getHandledErrorCallback(new ResponseHandler<CommonResponse>() {
						@Override
						public void handle(CommonResponse cr) {
							followUnfollowButton.setText(getString(R.string.follow));
							followUnfollowButton.setOnClickListener(followClickListener);
							RefreshRegistry.INSTANCE.add(Type.FOLLOW);
						}
					}));
		}
	}

	private class UpdatePriceClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			long leastAllowedTs = PinDetailFragment.lastPriceUpdateClick.get() + Config.LIMIT_ELAPSED_SINCE_LAST_UPDATE;
			long elapsed = System.currentTimeMillis() - leastAllowedTs;
			if (elapsed > 0) {
				apiService.updatePrice(pin.getPinId(), validationManager.getHandledErrorCallback(new ResponseHandler<CommonResponse>() {
					@Override
					public void handle(CommonResponse response) {
						PinDetailFragment.lastPriceUpdateClick.set(System.currentTimeMillis());
						Toast.makeText(PinDetailFragment.this.getActivity(), "Please wait, you will receive an update in a bit",
								Toast.LENGTH_LONG).show();
					}
				}));
			} else {
				long waitSeconds = Math.abs(elapsed / 1000);
				waitSeconds = waitSeconds < 1 ? 1 : waitSeconds;
				Toast.makeText(PinDetailFragment.this.getActivity(),
						"Wait another " + waitSeconds + " second(s) before you request another update", Toast.LENGTH_LONG).show();
			}
		}
	}

	private class RefreshProfileImageTask extends AsyncTask<String, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... params) {
			if (params.length > 0) {
				ImageLoader loader = App.getImageLoader();
				Bitmap bitmap = loader.getBitmap(RestClient.BASE_URL + "upload/find/" + params[0]);
				return bitmap;
			} else {
				return null;
			}
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null) {
				ownerEntry.setImageBitmap(result);
			}
		};
	}
}
