package com.devmite.pricepolice.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.TreeSet;

import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.adapter.CurrencyAdapter;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.dialog.UpdatePasswordDialog;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.helper.PriceService;
import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.SessionManager.CredentialsException;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.listener.CustomDialogListener;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.rest.ResponseHandler;
import com.devmite.pricepolice.rest.RestClient;
import com.devmite.pricepolice.util.ImageLoader;
import com.devmite.pricepolice.util.Utils;
import com.devmite.pricepolice.view.MaskImage;
import com.devmite.pricepolice.view.TwoLineEntry;
import com.devmite.pricepolice.view.TwoLineSpinner;

public class SettingsFragment extends Fragment {
	public static final int SELECT_PHOTO = 777;

	private MaskImage maskImage;
	private TextView usernameView, emailView;
	private TwoLineEntry passwordEntry, languageEntry, questionEntry, tncEntry;

	private TwoLineSpinner defaultCurrencySpinner;
	private CurrencyAdapter currencyAdapter;

	private String password;
	private ValidationManager<Activity> validationManager;
	private final Actions actions = new Actions();
	private PriceService priceService;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		priceService = new PriceService(getActivity());
		priceService.initialize();

		final SharedPreferences sp = getActivity().getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);

		View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

		maskImage = (MaskImage) rootView.findViewById(R.id.maskimage);
		usernameView = (TextView) rootView.findViewById(R.id.username_text);
		emailView = (TextView) rootView.findViewById(R.id.email_text);

		passwordEntry = (TwoLineEntry) rootView.findViewById(R.id.password);
		languageEntry = (TwoLineEntry) rootView.findViewById(R.id.language);
		questionEntry = (TwoLineEntry) rootView.findViewById(R.id.question);
		tncEntry = (TwoLineEntry) rootView.findViewById(R.id.tnc);

		defaultCurrencySpinner = (TwoLineSpinner) rootView.findViewById(R.id.default_currency);
		TreeSet<String> allCurrenciesSorted = new TreeSet<String>(priceService.getCurrencies());

		currencyAdapter = new CurrencyAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item,
				allCurrenciesSorted.toArray(new String[allCurrenciesSorted.size()]));
		defaultCurrencySpinner.setAdapter(currencyAdapter);
		defaultCurrencySpinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String currencyCode = currencyAdapter.getItem(position);
				sp.edit().putString(Config.SP_KEY_CURRENCY_KEY_DEFAULT, currencyCode).commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		{
			String currencyKeyDefault;
			int defaultCurrencyPosition;
			if ((currencyKeyDefault = sp.getString(Config.SP_KEY_CURRENCY_KEY_DEFAULT, null)) != null
					&& (defaultCurrencyPosition = currencyAdapter.findIndex(currencyKeyDefault, Integer.MIN_VALUE)) != Integer.MIN_VALUE)
				defaultCurrencySpinner.getSpinner().setSelection(defaultCurrencyPosition);
		}

		String username = sp.getString(Config.SP_KEY_USER_USERNAME, null);
		String email = sp.getString(Config.SP_KEY_USER_EMAIL, null);

		if (username == null || email == null) {
			validationManager.handleError(new CredentialsException("An error occurred processing your information."));
		}

		final String avatar = sp.getString(Config.SP_KEY_USER_AVATAR, null);
		if (avatar != null) {
			boolean existsAsFile = false;
			File file = new File(getActivity().getExternalFilesDir(null).toString(), avatar);
			if (file.isFile() && file.exists()) {
				Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
				if (bm != null) {
					maskImage.setImageBitmap(bm);
					existsAsFile = true;
				}
			}

			if (!existsAsFile) {
				new RefreshProfileImageTask().execute(avatar);
			}
		}

		maskImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.setType("image/*");
				startActivityForResult(intent, SELECT_PHOTO);
			}
		});
		usernameView.setText(username);
		emailView.setText(email);

		passwordEntry.setOnClickListener(actions);
		languageEntry.setOnClickListener(actions);
		questionEntry.setOnClickListener(actions);
		tncEntry.setOnClickListener(actions);

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		validationManager = new ValidationManager<Activity>(activity);
	}

	private class Actions implements View.OnClickListener, CustomDialogListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.password:
				showUpdatePasswordDialog();
				break;
			case R.id.language:
				break;
			case R.id.question:
				showEmailChooser();
				break;
			case R.id.tnc:
				showTnc();
				break;
			}
		}

		private void showEmailChooser() {
			Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "devmite@gmail.com", null));
			intent.putExtra(Intent.EXTRA_SUBJECT, "HelloDevmite: Enter subject after the colon");
			getActivity().startActivity(Intent.createChooser(intent, "Send email..."));
		}

		private void showTnc() {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.devmite.com"));
			startActivity(intent);
		}

		private void showUpdatePasswordDialog() {
			String tag = Config.KEY_PREF_PASSWORD;
			FragmentTransaction ft = getFragmentManager().beginTransaction();

			Fragment prev = getFragmentManager().findFragmentByTag(tag);
			if (prev != null) {
				ft.remove(prev);
			}
			ft.addToBackStack(null);

			Bundle args = new Bundle();

			DialogFragment newFragment = null;
			if (tag.equals(Config.KEY_PREF_PASSWORD)) {
				args.putString(Config.KEY_PREF_PASSWORD, password);
				newFragment = new UpdatePasswordDialog();
			}
			newFragment.setArguments(args);
			newFragment.show(ft, tag);
		}

		@Override
		public void updateDialogResult(Object object) {
			String newPassword = (String) object;
			Toast.makeText(getActivity(), "New password: " + newPassword, Toast.LENGTH_SHORT).show();
			// change password via API
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("ws", "onActivityResult called in SettingsFragment");
		if (resultCode != Activity.RESULT_OK)
			return;
		switch (requestCode) {
		case SELECT_PHOTO:
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA, MediaStore.Images.Media.MIME_TYPE };

			Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
			try {
				if (cursor.moveToFirst()) {
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					String filePath = cursor.getString(columnIndex);

					columnIndex = cursor.getColumnIndex(filePathColumn[1]);
					String mimeType = cursor.getString(columnIndex);

					uploadImage(filePath, mimeType);
				}
			} catch (Exception e) {
				Log.w("ws", e);
			} finally {
				cursor.close();
			}
			break;
		}
	}

	private void uploadImage(String imageUri, String mimeType) throws CredentialsException {
		final SessionManager sm = SessionManager.getInstance(getActivity());
		MultipartTypedOutput multipart = new MultipartTypedOutput();
		TypedString userIdTs = new TypedString(String.valueOf(sm.getUserId()));
		TypedString userTokenTs = new TypedString(sm.getToken());
		TypedString userPlatformTs = new TypedString("ANDROID");

		File file = new File(imageUri);
		TypedFile f = new TypedFile(mimeType, file);

		multipart.addPart("user_id", userIdTs);
		multipart.addPart("user_token", userTokenTs);
		multipart.addPart("user_platform", userPlatformTs);
		multipart.addPart("upload", f);

		App.getRestClient()
				.getAPIService()
				.uploadAvatarTraditional(f,
						validationManager.getHandledErrorCallback(new ResponseHandler<CommonResponse>() {
							@Override
							public void handle(CommonResponse response) {
								App.getRestClient()
										.getAPIService()
										.avatar(sm.getUserId(),
												validationManager.getHandledErrorCallback(new ResponseHandler<BaseRespone<String>>() {
													@Override
													public void handle(BaseRespone<String> response) {
														String avatar = response.getData();
														if (avatar != null) {
															Utils.saveStringPreferences(getActivity(), Config.SP_KEY_USER_AVATAR, avatar);
															new RefreshProfileImageTask().execute(avatar);
														}
													}
												}));
							}
						}));
	}

	private class RefreshProfileImageTask extends AsyncTask<String, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... params) {
			if (params.length > 0) {
				ImageLoader loader = App.getImageLoader();
				Bitmap bitmap = loader.getBitmap(RestClient.BASE_URL + "upload/find/" + params[0]);
				try {
					String path = getActivity().getExternalFilesDir(null).toString();
					File file = new File(path, params[0]);
					OutputStream out = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
					out.flush();
					out.close();
				} catch (Exception e) {
					Log.w("ws", e);
				}
				return bitmap;
			} else {
				return null;
			}
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null) {
				maskImage.setImageBitmap(result);
			}
		};
	}
}
