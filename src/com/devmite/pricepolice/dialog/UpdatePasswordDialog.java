package com.devmite.pricepolice.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.helper.Config;
import com.devmite.pricepolice.listener.CustomDialogListener;

public class UpdatePasswordDialog extends DialogFragment implements
		OnClickListener {

	EditText editOldPassword, editNewPassword, editConfirmNewPassword;
	Button buttonOK, buttonCancel;

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = getActivity().getLayoutInflater().inflate(
				R.layout.dialog_update_password, null);
		builder.setView(view);

		Bundle mArgs = getArguments();
		String pw = mArgs.getString(Config.KEY_PREF_PASSWORD);
		
		editOldPassword = (EditText) view.findViewById(R.id.edit_password_old);
		editOldPassword.setText(pw);
		editOldPassword.setFocusable(false);
		editOldPassword.setClickable(false);
		
		editNewPassword = (EditText) view.findViewById(R.id.edit_password_new);
		editNewPassword.setText("abcd1234");

		editConfirmNewPassword = (EditText) view
				.findViewById(R.id.edit_password_new_confirm);
		editConfirmNewPassword.setText("abcd1234");

		buttonOK = (Button) view.findViewById(R.id.btn_ok_pw);
		buttonOK.setOnClickListener(this);
		
		buttonCancel = (Button) view.findViewById(R.id.btn_cancel_pw);
		buttonCancel.setOnClickListener(this);
		
		builder.setTitle(R.string.update_password);
		return builder.create();
	}

	private void updateResult(String newPassword) {
		CustomDialogListener<String> listener = (CustomDialogListener<String>) getActivity();
		listener.updateDialogResult(newPassword);
		dismiss();
	}

	@SuppressLint("ShowToast")
	@Override
	public void onClick(View v) {
		if (v == buttonOK){
			String password = editNewPassword.getText().toString();
			String confirmPassword = editConfirmNewPassword.getText().toString();
			
			if (!password.equals(confirmPassword)){
				// toast doesnt work
				Toast.makeText(getActivity(), "password confirmation doesnt match", Toast.LENGTH_SHORT);
			} else {
				updateResult(password);
			}
		} else {
			dismiss();
		}
	}

}
