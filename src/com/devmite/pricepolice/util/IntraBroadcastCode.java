package com.devmite.pricepolice.util;

public enum IntraBroadcastCode {
	GCM_UNSUPPORTED_RECOVERABLE(100), GCM_UNSUPPORTED_FIRST_CHECK(101), GCM_UNSUPPORTED(102),
	GCM_REGISTERED(103), GCM_REGISTRATION_FAILURE(104), GCM_REGISTRATION_SERVER_FAILURE(105),
	LOGGED_OUT(106), LOGOUT_UNSUCCESSFUL(107), GCM_UNREGISTERED(108),
	UNKNOWN(Integer.MAX_VALUE);

	private final int code;

	private IntraBroadcastCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static IntraBroadcastCode fromCode(int code) {
		for (IntraBroadcastCode ibc : values()) {
			if (ibc.code == code) {
				return ibc;
			}
		}
		return UNKNOWN;
	}
}
