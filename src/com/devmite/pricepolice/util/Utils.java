package com.devmite.pricepolice.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.helper.Config;

public class Utils {

	public static void saveIntPreferences(Context context, String key, int value) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void saveLongPreferences(Context context, String key, long value) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public static int getIntPreferences(Context context, String key, int defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		return sp.getInt(key, defaultValue);
	}

	public static long getLongPreferences(Context context, String key, long defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		return sp.getLong(key, defaultValue);
	}

	public static void saveStringPreferences(Context context, String key, String value) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getStringPreferences(Context context, String key, String defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		return sp.getString(key, defaultValue);
	}

	public static void saveBooleanPreferences(Context context, String key, boolean value) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static boolean getBooleanPreferences(Context context, String key, boolean defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		return sp.getBoolean(key, defaultValue);
	}

	private final static Pattern emailPattern = Pattern
			.compile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");

	public Utils() {
		// TODO Auto-generated constructor stub
	}

	public static boolean isEmail(String sample) {
		if (emailPattern.matcher(sample).find()) {
			return true;
		} else {
			return false;
		}
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {

			byte[] bytes = new byte[buffer_size];
			for (;;) {
				// Read byte from input stream

				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;

				// Write byte from output stream
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	// allows number, space and "."
	public static boolean isNumber(String s) {
		boolean isNumber = true;
		for (int i = 0; i < s.length() && isNumber; i++) {
			char c = s.charAt(i);
			isNumber = isNumber
					& ((c >= '0' && c <= '9') || c == '.' || c == ' ');
		}
		return isNumber;
	}

	public static boolean isNumericRegex(String value) {
		// allows non-numeric chars in front of numbers
		Pattern PATTERN = Pattern.compile("([a-z|A-Z|\\s])*\\d*\\.?\\d+");
		return value != null && PATTERN.matcher(value).matches();
	}

	public static BigDecimal extractNumeric(String s) {
		StringBuilder sb = new StringBuilder();

		int count = 0;

		s = s.replaceAll("[^\\d.,]+", "");

		for (int i = s.length() - 1; i >= 0; i--) {
			char c = s.charAt(i);
			if (c == '.' || c == ',') {
				if (count == 2) {
					sb.append('.');
				}
				// else if (count == 3) {
				//
				// }
				count = 0;
			}

			if (c >= '0' && c <= '9') {
				sb.append(c);
			}
			count++;
		}

		sb = sb.reverse();
		return new BigDecimal(sb.toString());
	}

	/****
	 * Method for Setting the Height of the ListView dynamically. Hack to fix
	 * the issue of not showing all the items of the ListView when placed inside
	 * a ScrollView
	 ****/
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void applySerifTypeface(TextView... views) {
		Typeface typeface = App.getSanchezTypeface();
		if (typeface != null) {
			for (TextView view : views) {
				view.setTypeface(typeface);
			}
		} else {
			Log.w("ws", "Font not found");
		}
	}

	public static void overrideFont(String defaultFontNameToOverride, Typeface typeface) {
		try {
			final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
			defaultFontTypefaceField.setAccessible(true);
			defaultFontTypefaceField.set(null, typeface);
		} catch (Exception e) {
			Log.e("ws", "Can not set custom font " + typeface.toString() + " instead of " + defaultFontNameToOverride);
		}
	}

	public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
		try {
			final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

			final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
			defaultFontTypefaceField.setAccessible(true);
			defaultFontTypefaceField.set(null, customFontTypeface);
		} catch (Exception e) {
			Log.e("ws", "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
		}
	}

	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	@SuppressLint("DefaultLocale")
	public static String formatPrice(String currency, BigDecimal bd) {
		// added thousand separator
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(' ');
		String str = formatter.format(bd.doubleValue());
		return formatPrice(currency, str);
		
//		return formatPrice(currency, bd.stripTrailingZeros().toPlainString());
	}

	public static String formatPrice(String currency, String price) {
		if (isBlank(currency)) {
			return price;
		} else {
			return currency + " " + price;
		}
	}

	public static String getCssPath(Element el) {
		if (el == null)
			return "";

		if (!el.id().isEmpty())
			return "#" + el.id();

		StringBuilder selector = new StringBuilder(el.tagName());
		String classes = StringUtil.join(el.classNames(), ".");
		if (!classes.isEmpty())
			selector.append('.').append(classes);

		if (el.parent() == null)
			return selector.toString();

		selector.insert(0, " > ");
		if (el.parent().select(selector.toString()).size() > 1)
			selector.append(String.format(
					":nth-child(%d)", el.elementSiblingIndex() + 1));

		return getCssPath(el.parent()) + selector.toString();
	}
}