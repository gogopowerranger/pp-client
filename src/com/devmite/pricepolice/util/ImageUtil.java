package com.devmite.pricepolice.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.util.Log;

public class ImageUtil {

	public static final int THUMBNAIL_SIZE = 200;

	public static Bitmap resizeBitmap(Bitmap b, int reqWidth, int reqHeight) {
		Matrix m = new Matrix();
		m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()),
				new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.FILL);
		return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m,
				true);
	}

	public static Bitmap resizeBitmap2(Bitmap b, int reqWidth, int reqHeight) {
		Matrix m = new Matrix();
		int x = 0, y = 0;
		Log.d("ws", "b height: " + b.getHeight() + ", width: " + b.getWidth());
		Log.d("ws", "req height: " + reqHeight + ", width: " + reqWidth);

		double bWidth = Integer.valueOf(b.getWidth()).doubleValue();
		double bHeight = Integer.valueOf(b.getHeight()).doubleValue();

		double max = Integer.valueOf(reqHeight > reqWidth ? reqHeight : reqWidth).doubleValue();
		if (b.getHeight() > b.getWidth()) {
			double w = max / bWidth;
			x = (int) Math.round(bWidth * w);
			y = (int) Math.round(bHeight * w);
			Log.d("ws", "w, x, y: " + w + ", " + x + ", " + y);
		} else {
			double h = max / bHeight;
			x = (int) Math.round(bWidth * h);
			y = (int) Math.round(bHeight * h);
			Log.d("ws", "h, x, y: " + h + ", " + x + ", " + y);
		}
		m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()),
				new RectF(0, 0, x, y), Matrix.ScaleToFit.START);
		return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m,
				true);
	}

	public static Bitmap getThumbnail(Uri uri, Context context) throws FileNotFoundException, IOException {
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither = true;// optional
		onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight
				: onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither = true;// optional
		bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio) {
		int k = Integer.highestOneBit((int) Math.floor(ratio));
		if (k == 0)
			return 1;
		else
			return k;
	}

}
