package com.devmite.pricepolice.constant;

public interface AuthHeader {
	String NAME_ID = "X-Devmite-UserID";
	String NAME_TOKEN = "X-Devmite-UserToken";
}
