package com.devmite.pricepolice.constant;

/**
 *
 * @author William
 */
public enum Gender {

	MALE, FEMALE, UNKNOWN;

	public static Gender safeFromName(String name) {
		try {
			return Gender.valueOf(name.toUpperCase());
		} catch (IllegalArgumentException e) {
			return Gender.UNKNOWN;
		}
	}
}
