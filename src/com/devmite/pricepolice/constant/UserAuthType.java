package com.devmite.pricepolice.constant;

public enum UserAuthType implements HasFlag<UserAuthType> {
	EMAIL(1), FACEBOOK(2);

	private final int flag;

	private UserAuthType(int flag) {
		this.flag = flag;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	public static UserAuthType fromFlag(int flag) {
		switch (flag) {
		case 2:
			return FACEBOOK;
		default:
			return EMAIL;
		}
	}
}
