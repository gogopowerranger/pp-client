package com.devmite.pricepolice.constant;

public interface HasFlag<I> {
	abstract int getFlag();
}
