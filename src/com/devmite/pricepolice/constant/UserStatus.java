package com.devmite.pricepolice.constant;

public enum UserStatus implements HasFlag<UserStatus> {
	OK(0), DISABLED(1), BANNED(2);

	private final int flag;

	private UserStatus(int flag) {
		this.flag = flag;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	public static UserStatus fromFlag(int flag) {
		switch (flag) {
		case 1:
			return DISABLED;
		case 2:
			return BANNED;
		default:
			return OK;
		}
	}
}
