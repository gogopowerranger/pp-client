//package com.devmite.pricepolice.rest.model;
//
//import java.util.ArrayList;
//
//import com.devmite.pricepolice.model.Coord;
//import com.devmite.pricepolice.model.MainInfos;
//import com.devmite.pricepolice.model.Parcel;
//import com.devmite.pricepolice.model.Sys;
//import com.devmite.pricepolice.model.Weather;
//import com.devmite.pricepolice.model.Wind;
//import com.devmite.pricepolice.model.http.Board;
//import com.devmite.pricepolice.model.http.LoginRequest;
//import com.devmite.pricepolice.model.http.LoginResponse;
//import com.devmite.pricepolice.model.http.Pin;
//import com.devmite.pricepolice.model.http.RegisterRequest;
//import com.google.gson.annotations.SerializedName;
//
//@Parcel
//public class ApiResponse {
//	@SerializedName("Board")
//	private Board board;
//
//	@SerializedName("LoginResponse")
//	private LoginResponse loginResponse;
//
//	@SerializedName("Pin")
//	private Pin pin;
//
//	@SerializedName("RegisterRequest")
//	private RegisterRequest registerRequest;
//
//	public Board getBoard() {
//		return board;
//	}
//
//	public LoginRequest getLoginRequest() {
//		return loginRequest;
//	}
//
//	public LoginResponse getLoginResponse() {
//		return loginResponse;
//	}
//
//	public Pin getPin() {
//		return pin;
//	}
//
//	public RegisterRequest getRegisterRequest() {
//		return registerRequest;
//	}
//
//	// ----------------------------
//	
//	
//	@SerializedName("coord")
//	private Coord coord;
//
//	@SerializedName("sys")
//	private Sys sys;
//
//	@SerializedName("weather")
//	private ArrayList<Weather> weather;
//
//	@SerializedName("main")
//	private MainInfos mainInfos;
//
//	@SerializedName("wind")
//	private Wind wind;
//
//	@SerializedName("name")
//	private String strCityName;
//
//	public Coord getCoord() {
//		return coord;
//	}
//
//	public Sys getSys() {
//		return sys;
//	}
//
//	public ArrayList<Weather> getWeather() {
//		return weather;
//	}
//
//	public MainInfos getMainInfos() {
//		return mainInfos;
//	}
//
//	public Wind getWind() {
//		return wind;
//	}
//
//	public String getStrCityName() {
//		return strCityName;
//	}
//}
