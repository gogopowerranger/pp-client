package com.devmite.pricepolice.rest.service;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;

import com.devmite.pricepolice.model.http.AuthRequest;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.BoardResponse;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PinDeleteRequest;
import com.devmite.pricepolice.model.http.PinFollowRequest;
import com.devmite.pricepolice.model.http.PinFollowResponse;
import com.devmite.pricepolice.model.http.PinRequest;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.model.http.PostPinRequest;
import com.devmite.pricepolice.model.http.PushRegistrationRequest;
import com.devmite.pricepolice.model.http.PushTestRequest;
import com.devmite.pricepolice.model.http.UserResponse;

public interface APIService
{
	@POST("/account/register/email")
	public void register(@Body AuthRequest.ByEmail request, Callback<BaseRespone<UserResponse>> callback);

	@POST("/account/login/email")
	public void login(@Body AuthRequest.ByEmail request, Callback<BaseRespone<UserResponse>> callback);

	@GET("/account/{id}")
	public void user(@Path("id") long userId, Callback<BaseRespone<UserResponse>> callback);

	@POST("/account/push/set")
	public void pushSet(@Body PushRegistrationRequest request, Callback<CommonResponse> callback);

	@POST("/account/push/test")
	public void pushTest(@Body PushTestRequest request, Callback<BaseRespone<Object>> callback);

	@GET("/pin/{id}")
	public void pin(@Path("id") long pinId, Callback<BaseRespone<PinResponse>> callback);

	@POST("/pin/add")
	public void postPin(@Body PostPinRequest request, Callback<BaseRespone<PinResponse>> callback);
	
	@POST("/pin/delete")
	public void pinDelete(@Body PinDeleteRequest request, Callback<CommonResponse> callback);

	@POST("/pin/follow")
	public void follow(@Body PinFollowRequest request, Callback<BaseRespone<PinFollowResponse>> callback);

	@POST("/pin/unfollow")
	public void unfollow(@Body PinFollowRequest request, Callback<CommonResponse> callback);

	@GET("/pin/is_followed/{id}")
	public void pinIsFollowed(@Path("id") long pinId, Callback<BaseRespone<Boolean>> callback);

	@POST("/pin/public")
	public void pinsPublic(@Body PinRequest.Public request, Callback<BaseRespone<List<PinResponse>>> callback);

	@POST("/pin/search")
	public void pinsSearch(@Body PinRequest.Search request, Callback<BaseRespone<List<PinResponse>>> callback);

	@POST("/pin/by_board")
	public void pinsByBoard(@Body PinRequest.ByBoard request, Callback<BaseRespone<List<PinResponse>>> callback);

	@POST("/pin/followed")
	public void pinsFollowed(@Body PinRequest.Followed request, Callback<BaseRespone<List<PinResponse>>> callback);

	@POST("/pin/statistics")
	public void pinsStatistics(@Body PinRequest.ByPinIds request, Callback<BaseRespone<Map<Long, PinResponse.Statistics>>> callback);

	@GET("/board")
	public void primaryBoard(Callback<BaseRespone<BoardResponse>> callback);

	@GET("/board")
	public BaseRespone<BoardResponse> primaryBoard();

	@GET("/board/{id}")
	public void board(@Path("id") long boardId, Callback<BaseRespone<BoardResponse>> callback);

	@GET("/pin/update_price/{id}")
	public void updatePrice(@Path("id") long pinId, Callback<CommonResponse> callback);

	// @Multipart
	@POST("/upload/avatar/traditional")
	public void uploadAvatarTraditional(@Body MultipartTypedOutput multipart, Callback<CommonResponse> callback);

	@Multipart
	@POST("/upload/avatar")
	public void uploadAvatarTraditional(@Part("upload") TypedFile upload, Callback<CommonResponse> callback);
	
	@GET("/upload/avatar/user/{id}")
	public void avatar(@Path("id") long userId, Callback<BaseRespone<String>> callback);
}
