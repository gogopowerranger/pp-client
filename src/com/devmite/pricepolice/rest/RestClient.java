package com.devmite.pricepolice.rest;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import android.content.Context;

import com.devmite.pricepolice.helper.SessionManager;
import com.devmite.pricepolice.helper.SessionManager.CredentialsException;
import com.devmite.pricepolice.helper.ValidationManager;
import com.devmite.pricepolice.rest.service.APIService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RestClient {
	private static final String BASE_URL = "http://119.81.21.56:12128/";
//	public static final String BASE_URL = "http://10.0.3.2:12128/";
	private final APIService apiService;
	private final ValidationManager<Context> validationManager;

	public RestClient(final Context context) {
		this.validationManager = new ValidationManager<Context>(context);
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.registerTypeAdapterFactory(new ItemTypeAdapterFactory()).create();

		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader("User-Agent", "Android");
				request.addHeader("X-Devmite-UserOS", "Android");
				request.addHeader("X-Devmite-Platform", "ANDROID");
				
				SessionManager sessionManager = SessionManager.getInstance(context);

				try {
					if (sessionManager.isUserLoggedIn()) {
						String userId = String.valueOf(sessionManager.getUserId());
						String token = sessionManager.getToken();
						request.addHeader("X-Devmite-UserID", userId);
						request.addHeader("X-Devmite-UserToken", token);
					}
				} catch (CredentialsException ce) {
					validationManager.handleError(ce);
				}
			}
		};

		//@formatter:off
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(BASE_URL)
				.setConverter(new GsonConverter(gson))
				.setRequestInterceptor(requestInterceptor)
				.build();
		//@formatter:on

		apiService = restAdapter.create(APIService.class);

	}

	public APIService getAPIService() {
		return apiService;
	}
}
