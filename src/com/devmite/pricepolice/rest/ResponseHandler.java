package com.devmite.pricepolice.rest;

public interface ResponseHandler<T> {
	void handle(T response);
}
