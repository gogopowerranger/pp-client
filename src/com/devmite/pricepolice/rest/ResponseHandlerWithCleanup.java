package com.devmite.pricepolice.rest;

public interface ResponseHandlerWithCleanup<T> extends ResponseHandler<T> {
	void cleanup(boolean successful);
}
