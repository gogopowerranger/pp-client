package com.devmite.pricepolice.exception;

import com.devmite.pricepolice.util.IntraBroadcastCode;

public class NoSupportException extends Exception {
	private static final long serialVersionUID = -6214232672158342324L;
	
	private final IntraBroadcastCode ibc;

	public NoSupportException(String detailMessage, Throwable throwable, IntraBroadcastCode ibc) {
		super(detailMessage, throwable);
		this.ibc = ibc;
	}

	public NoSupportException(String detailMessage, IntraBroadcastCode ibc) {
		super(detailMessage);
		this.ibc= ibc;
	}

	public IntraBroadcastCode getIbc() {
		return ibc;
	}
}
