package com.devmite.pricepolice.helper.http;

import java.io.File;

import android.os.AsyncTask;
import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;
import com.github.kevinsawicki.http.HttpRequest.HttpRequestException;

public class DownloadTask extends AsyncTask<String, Long, File> {
	protected File doInBackground(String... urls) {
		try {
			HttpRequest request = HttpRequest.get(urls[0]);
			File file = null;
			if (request.ok()) {
				// file = File.createTempFile("download", ".tmp");
				request.receive(file);
				publishProgress(file.length());
			}
			return file;
		} catch (HttpRequestException exception) {
			return null;
		}
	}

	protected void onProgressUpdate(Long... progress) {
		Log.d("MyApp", "Downloaded bytes: " + progress[0]);
	}

	protected void onPostExecute(File file) {
		if (file != null)
			Log.d("MyApp", "Downloaded file to: " + file.getAbsolutePath());
		else
			Log.d("MyApp", "Download failed");
	}
}