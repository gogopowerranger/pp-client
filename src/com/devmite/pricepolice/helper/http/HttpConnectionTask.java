package com.devmite.pricepolice.helper.http;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Hashtable;

import android.os.AsyncTask;
import android.util.Log;

public class HttpConnectionTask extends AsyncTask<String, Void, String> {

	private Hashtable<String, String> params, headers;
	private IHttpResponseListener listener;
	private int type = 0;
	private final String strConnectionError = "Http Connection error";

	public final static String GET = "GET";
	public final static String POST = "POST";
	
	private String method = GET;
	private String body;

	public HttpConnectionTask(IHttpResponseListener listener, String body, Hashtable<String, String> headers, int type) {
		this.listener = listener;
		this.body = body;
		this.headers = headers;
		this.type = type;
		this.method = POST;
	}

	public HttpConnectionTask(IHttpResponseListener listener, String body, int type) {
		this(listener, body, null, type);
	}

	public HttpConnectionTask(IHttpResponseListener listener, String body, Hashtable<String, String> headers) {
		this(listener, body, headers, 0);
	}
	
	public HttpConnectionTask(IHttpResponseListener listener, Hashtable<String, String> params, int type) {
		this.params = params;
		this.listener = listener;
		this.type = type;
		this.method = POST;
	}
	
	public HttpConnectionTask(IHttpResponseListener listener, Hashtable<String, String> params) {
		this(listener, params, 0);
	}

	public HttpConnectionTask(IHttpResponseListener listener, int type, String method) {
		this(listener, null);
		this.type = type;
		this.method = method;
	}

	public HttpConnectionTask(IHttpResponseListener listener, int type) {
		this(listener, type, GET);
	}

	public HttpConnectionTask(IHttpResponseListener listener) {
		this(listener, null);
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected String doInBackground(String... sUrls) {
		String result = null;
		Log.d(this.getClass().getName(), "doInBackground");

		if (body != null) {
			try {
				if (headers!=null){
					result = HttpRequestOld.post(sUrls[0], body, headers);
				} else {
					result = HttpRequestOld.post(sUrls[0], body);
				}
				
			} catch (UnsupportedEncodingException e) {
				result = strConnectionError + ", " + e.getMessage();
			} catch (MalformedURLException e) {
				result = strConnectionError + ", " + e.getMessage();
			} catch (IOException e) {
				result = strConnectionError + ", " + e.getMessage();
			}
		} else if (params != null) {// POST with key/value pairs params
			try {
				result = HttpRequestOld.post(sUrls[0], params);
			} catch (UnsupportedEncodingException e) {
				result = strConnectionError + ", " + e.getMessage();
			} catch (MalformedURLException e) {
				result = strConnectionError + ", " + e.getMessage();
			} catch (IOException e) {
				result = strConnectionError + ", " + e.getMessage();
			}
		} else { // GET
			try {
				result = HttpRequestOld.get(sUrls[0]);
			} catch (MalformedURLException e) {
				result = strConnectionError + ", " + e.getMessage();
			} catch (IOException e) {
				result = strConnectionError + ", " + e.getMessage();
			}
		}

		Log.d(this.getClass().getName(), "returning " + result);
		return result;
	}

	@Override
	protected void onPostExecute(String result) {

		if (listener != null) {
			if (result.startsWith(strConnectionError)) {
				Log.d(this.getClass().getName(), "call resultFailed");
				listener.resultFailed(type, result);
			} else {

				Log.d(this.getClass().getName(), "call resultSuccess");
				listener.resultSuccess(type, result);
			}
		} else {
			Log.d(this.getClass().getName(), "listener is null, how is that?");
		}

	}

}
