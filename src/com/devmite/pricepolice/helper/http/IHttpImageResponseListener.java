package com.devmite.pricepolice.helper.http;

public interface IHttpImageResponseListener {
	public void resultSuccess(int type, byte[] result);
	public void resultFailed(int type, String strError);
}
