package com.devmite.pricepolice.helper;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.devmite.pricepolice.activity.LoginActivity;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PushRegistrationRequest;
import com.devmite.pricepolice.util.EncryptionUtils;
import com.devmite.pricepolice.util.IntraBroadcastCode;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class SessionManager {

	public static class CredentialsException extends Exception {
		private static final long serialVersionUID = 331016225810006555L;

		public CredentialsException(String message) {
			super(message);
			Log.e("ws", message);
		}
	}

	public static final String TOKEN = "token";
	public static final String USER_ID = "user_id";

	private final String key;
	private Context context;
	private final SharedPreferences customSharedPreference;

	private static SessionManager instance;

	public static SessionManager getInstance(Context context) {
		if (instance == null) {
			instance = new SessionManager(context);
		}

		return instance;
	}

	private SessionManager(Context context) {
		this.customSharedPreference = context.getSharedPreferences(
				Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		this.key = "000102030405060708090A0B0C0D0E0F";
		this.context = context;
	}

	public void setToken(String token) throws CredentialsException {
		try {
			String encryptedToken = EncryptionUtils.encrypt(this.key, token);
			Log.i("ws", "Encrypted token: " + encryptedToken);

			SharedPreferences.Editor editor = customSharedPreference.edit();
			editor.putString(TOKEN, encryptedToken);
			editor.commit();
		} catch (Exception e) {
			Log.e("ws", e.getMessage(), e);
			throw new CredentialsException("Unable to encrypt token");
		}
	}

	public String getToken() throws CredentialsException {
		String candidate = customSharedPreference.getString(TOKEN, null);
		if (candidate != null) {
			Log.i("ws", "Key: " + this.key);
			Log.i("ws", "Encrypted token: " + candidate);
			try {
				String token = EncryptionUtils.decrypt(this.key, candidate);
				Log.i("ws", "Token: " + token);
				return token;
			} catch (Exception e) {
				Log.e("ws", e.getMessage(), e);
				throw new CredentialsException("Unable to decrypt token");
			}
		} else {
			throw new CredentialsException("Token not found");
		}
	}

	public void setUserId(long userId) {
		SharedPreferences.Editor editor = customSharedPreference.edit();
		editor.putLong(USER_ID, userId);
		editor.commit();
	}

	public long getUserId() {
		return customSharedPreference.getLong(USER_ID, -1);
	}

	public boolean isUserLoggedIn() {

		return customSharedPreference.getString(TOKEN, null) != null
				&& !(getUserId() == -1);
	}

	@Deprecated
	public void logout(final boolean force) throws IOException {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					GoogleCloudMessaging.getInstance(context).unregister();
					Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
					intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, IntraBroadcastCode.GCM_UNREGISTERED.getCode());
					LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
				} catch (IOException e) {
					if (force) {
						Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
						intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, IntraBroadcastCode.GCM_UNREGISTERED.getCode());
						LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
					} else {
						Log.w("ws", e);
						Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
						intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, IntraBroadcastCode.LOGOUT_UNSUCCESSFUL.getCode());
						LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
					}
				}
				return null;
			}
		}.execute();
	}

	public void logout2() {
		PushRegistrationRequest prr = new PushRegistrationRequest();
		prr.setRegId("");
		App.getRestClient().getAPIService().pushSet(prr, new Callback<CommonResponse>() {

			@Override
			public void success(CommonResponse cr, Response r) {
				Log.i("ws", cr.toString());
				finish();
			}

			@Override
			public void failure(RetrofitError re) {
				Log.w("ws", re);
				finish();
			}

			private void finish() {
				try {
					SharedPreferences.Editor editor = customSharedPreference.edit();
					editor.clear();
					editor.commit();

					Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
					intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, IntraBroadcastCode.LOGGED_OUT.getCode());
					LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
				} catch (Exception e) {
					Log.w("ws", e);
					Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
					intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, IntraBroadcastCode.LOGOUT_UNSUCCESSFUL.getCode());
					LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
				}
			}
		});
	}

	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public void checkLogin() {
		// Check login status
		if (!this.isUserLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);
		}

	}
}
