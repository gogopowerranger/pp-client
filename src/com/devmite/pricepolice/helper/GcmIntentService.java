package com.devmite.pricepolice.helper;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Patterns;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.activity.PrimaryActivity;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.model.NotificationAction;
import com.devmite.pricepolice.model.http.PinUpdateResultResponse;
import com.devmite.pricepolice.model.http.PinUpdateSuccessfulResultResponse;
import com.devmite.pricepolice.rest.ItemTypeAdapterFactory;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GcmIntentService extends IntentService {

	public static AtomicInteger NOTIFICATION_ID = new AtomicInteger(1);
	private NotificationCompat.Builder builder;
	private Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
			.registerTypeAdapterFactory(new ItemTypeAdapterFactory()).create();

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " +
						extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				String action = extras.getString("action");
				NotificationAction na = null;
				if (action != null && (na = NotificationAction.fromString(action)) != null) {
					String reason = extras.getString("reason");
					String content = extras.getString("content");

					Log.i("ws", "action: " + action + ", reason: " + reason);

					processNotificationAction(na, content, reason);
				}

				// sendNotification("Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void processNotificationAction(NotificationAction na, String content, String reason) {
		switch (na) {
		case WONT_UPDATE:
			PinUpdateResultResponse purr = gson.fromJson(content, PinUpdateResultResponse.class);
			String title = getString(R.string.notification_title_wont_update, purr.getLabel());
			String text = getString(R.string.notification_msg_wont_update);

			Intent intent = new Intent(this, PrimaryActivity.class);
			intent.putExtra("action", na.name());
			intent.putExtra("pin_id", purr.getPinId());

			sendNotification(title, text, intent, purr.getImageUrl());
			break;
		case NO_PRICE_UPDATE:
			purr = gson.fromJson(content, PinUpdateResultResponse.class);
			if (reason != null && reason.equals("PRICE_NOT_FOUND")) {
				title = getString(R.string.notification_title_error, purr.getLabel());
				text = getString(R.string.notification_msg_price_no_update, purr.getInitialPrice(), purr.getLatestPrice());
			} else {
				title = getString(R.string.notification_title_no_update, purr.getLabel());
				text = getString(R.string.notification_msg_price_no_update, purr.getInitialPrice(), purr.getLatestPrice());
			}
			
			intent = new Intent(this, PrimaryActivity.class);
			intent.putExtra("action", na.name());
			intent.putExtra("pin_id", purr.getPinId());

			sendNotification(title, text, intent, purr.getImageUrl());
			break;
		case PRICE_UPDATE:
			PinUpdateSuccessfulResultResponse pusrr = gson.fromJson(content, PinUpdateSuccessfulResultResponse.class);
			title = getString(R.string.notification_title_price_update, pusrr.getLabel());
			try {
				BigDecimal secondlatestPrice = new BigDecimal(pusrr.getSecondLatestPrice());
				BigDecimal latestPrice = new BigDecimal(pusrr.getLatestPrice());
				if (latestPrice.compareTo(secondlatestPrice) < 0) {
					title = getString(R.string.notification_title_price_update_down, pusrr.getLabel());
				}
			} catch (NumberFormatException e) {
				Log.w("ws", e);
			}
			text = getString(R.string.notification_msg_price_update, pusrr.getSecondLatestPrice(), pusrr.getLatestPrice());

			intent = new Intent(this, PrimaryActivity.class);
			intent.putExtra("action", na.name());
			intent.putExtra("pin_id", pusrr.getPinId());

			sendNotification(title, text, intent, pusrr.getImageUrl());
			break;
		}
	}

	private void sendNotification(String title, String text, Intent intent, String imageUrl) {
		NotificationManager mNotificationManager = (NotificationManager)
				this.getSystemService(Context.NOTIFICATION_SERVICE);

		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		PendingIntent contentIntent = PendingIntent.getActivity(this,
				0, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
				.setContentIntent(contentIntent)
				.setSmallIcon(R.drawable.ic_launcher)
				.setSound(alarmSound)
				.setContentTitle(title)
				.setAutoCancel(true);

		boolean bigTextStyle = true;
		if (imageUrl != null && Patterns.WEB_URL.matcher(imageUrl).matches()) {
			try {
				Bitmap bitmap = App.getImageLoader().getBitmap(imageUrl);
				nb.setContentText("Swipe down for further details");
				nb.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setBigContentTitle(title).setSummaryText(text));
				bigTextStyle = false;
			} catch (Exception e) {
				Log.w("ws", e);
			}
		}
		if (bigTextStyle) {
			nb.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
		}

		mNotificationManager.notify(NOTIFICATION_ID.getAndIncrement(), nb.build());
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String msg) {
		NotificationManager mNotificationManager = (NotificationManager)
				this.getSystemService(Context.NOTIFICATION_SERVICE);

		Uri alarmSound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle("GCM Notification")
						.setStyle(new NotificationCompat.BigTextStyle()
								.bigText(msg))
						.setContentText(msg);

		mNotificationManager.notify(NOTIFICATION_ID.getAndIncrement(), mBuilder.build());
	}

}
