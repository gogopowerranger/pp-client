package com.devmite.pricepolice.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.model.Currency;
import com.devmite.pricepolice.model.Price;
import com.devmite.pricepolice.util.Utils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class PriceService {
	public static class CurrencyData {
		private String code;
		private String symbol;
		private String name;
		private String symbolNative;
		private String namePlural;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getSymbol() {
			return symbol;
		}

		public void setSymbol(String symbol) {
			this.symbol = symbol;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSymbolNative() {
			return symbolNative;
		}

		public void setSymbolNative(String symbolNative) {
			this.symbolNative = symbolNative;
		}

		public String getNamePlural() {
			return namePlural;
		}

		public void setNamePlural(String namePlural) {
			this.namePlural = namePlural;
		}

		@Override
		public String toString() {
			return "CurrencyData [code=" + code + ", symbol=" + symbol + ", name=" + name + ", symbolNative="
					+ symbolNative
					+ ", namePlural=" + namePlural + "]";
		}

		public void uppercase(Locale locale) {
			if (code != null) {
				code = code.toUpperCase(locale);
			}
			if (name != null) {
				name = name.toUpperCase(locale);
			}
			if (namePlural != null) {
				namePlural = namePlural.toUpperCase(locale);
			}
			if (symbol != null) {
				symbol = symbol.toUpperCase(locale);
			}
			if (symbolNative != null) {
				symbolNative = symbolNative.toUpperCase(locale);
			}
		}

	}

	private Set<String> currencyKeys;
	private Set<String> priorityCurrencyKeys = new HashSet<String>();
	private final Activity activity;
	private final SharedPreferences sp;
	private static final Pattern currencySymbolPattern = Pattern.compile("\\p{Sc}");
	private static final Pattern asciiPattern = Pattern.compile("\\A\\p{ASCII}*\\z");
	private static final Pattern alphaPattern = Pattern.compile("\\p{L}++(?:\\.|\\b)");
	private static final Pattern numbersPattern = Pattern.compile("\\d+(?:[.,\\s]\\d+)*(?![\\d.,])|\\d+(?:[.,\\s]\\d+)");
	// private static final Pattern numbersPattern =
	// Pattern.compile("\\d+(?:[.,\\s]\\d+)*(?![\\d.,])");
	private static final Pattern whitespacePattern = Pattern.compile("\\s+");
	private static final Pattern nonAlphaPattern = Pattern.compile("\\W+");

	public PriceService(final Activity activity) {
		this.sp = activity.getSharedPreferences(Config.KEY_SHARED_PREF, Activity.MODE_PRIVATE);
		this.activity = activity;
		this.currencyKeys = sp.getStringSet(Config.SP_KEY_CURRENCY_KEYS, null);
		this.priorityCurrencyKeys = new HashSet<String>();
		priorityCurrencyKeys.add("IDR");
		priorityCurrencyKeys.add("USD");
		priorityCurrencyKeys.add("RUB");
	}

	public void initialize() {
		if (this.currencyKeys == null) {
			final ProgressDialog pg = new ProgressDialog(activity);
			pg.setCancelable(false);
			pg.setCanceledOnTouchOutside(false);
			pg.setMessage(activity.getString(R.string.dialog_please_wait));
			pg.show();
			new AsyncTask<Void, Void, Map<String, CurrencyData>>() {

				@Override
				protected Map<String, CurrencyData> doInBackground(Void... params) {
					try {
						Map<String, CurrencyData> result = PriceService.populate(activity);
						return result;
					} catch (Exception e) {
						Log.w("ws", "Unable to populate currency backend", e);
						return null;
					}
				}

				@Override
				protected void onPostExecute(Map<String, CurrencyData> result) {
					try {
						if (result == null)
							return;

						SharedPreferences.Editor spe = sp.edit();
						Set<String> currencies = new TreeSet<String>();
						Set<String> currencyKeys = new TreeSet<String>();
						for (Map.Entry<String, CurrencyData> e : result.entrySet()) {
							CurrencyData cd = e.getValue();
							cd.uppercase(locale);

							String currency = e.getKey().toUpperCase(locale);
							String currencyKey = "currency." + currency;
							String metaKey = "meta.currency." + currency;

							Matcher csMatcher = currencySymbolPattern.matcher(cd.symbol);
							String symbolNormalized;
							if (csMatcher.find()) {
								symbolNormalized = "\\" + csMatcher.group();
							} else if (asciiPattern.matcher(cd.symbol).find()) {
								symbolNormalized = String.format("\\b(%s)\\b", cd.symbol);
							} else {
								symbolNormalized = cd.symbol;
							}

							String symbolNativeNormalized;
							csMatcher = currencySymbolPattern.matcher(cd.symbolNative);
							if (csMatcher.find()) {
								symbolNativeNormalized = "\\" + csMatcher.group();
							} else if (asciiPattern.matcher(cd.symbolNative).find()) {
								symbolNativeNormalized = String.format("\\b(%s)\\b", cd.symbolNative);
							} else {
								symbolNativeNormalized = cd.symbolNative;
							}

							spe.putString(currencyKey, toCompact(cd));
							spe.putString(
									metaKey,
									String.format("\\b(%s)\\b|%s|%s|%s|%s", cd.code, cd.name.replace(" ", "\\s"),
											cd.namePlural.replace(" ", "\\s"), symbolNormalized, symbolNativeNormalized));
							currencies.add(currency);
							currencyKeys.add(currencyKey);
						}
						spe.putStringSet(Config.SP_KEY_CURRENCIES, currencies);
						spe.putStringSet(Config.SP_KEY_CURRENCY_KEYS, currencyKeys);

						StringBuilder sb = new StringBuilder("Added currencies: ");
						for (String currency : currencies) {
							sb.append(currency).append(", ");
						}
						Log.i("ws", sb.toString());

						spe.commit();

						PriceService.this.currencyKeys = currencyKeys;
					} finally {
						pg.dismiss();
					}
				}
			}.execute();
		} else {
			StringBuilder sb = new StringBuilder("Currency keys: ");
			for (String ck : currencyKeys) {
				sb.append(ck).append(", ");
			}
			Log.i("ws", sb.toString());
		}
	}

	public Set<String> getCurrencies() {
		return sp.getStringSet(Config.SP_KEY_CURRENCIES, Collections.<String> emptySet());
	}

	public Price find2(final String textRaw) {
		String text = textRaw.replaceAll("\\u00A0", " ").toUpperCase(locale);
		// it is impossible to have Rp 15000 USD anyway. Even if it were, it
		// would be like Rp 15000 USD 1.5, and then we can safely use the Rp
		// instead of USD
		// which means it is mostly linear left to right, until proven otherwise

		Matcher nm = numbersPattern.matcher(text);

		int lastNmEnd = 0;
		BigDecimal firstKnownPriceBd = null;
		if (nm.find()) {
			Log.i("ws", "nm found: " + nm.group());

			Set<String> candidates = new HashSet<String>();
			if (nm.start() != 0) {
				String candidate = text.substring(lastNmEnd, nm.start()).trim().split("\\s+")[0];
				if (!Utils.isBlank(candidate))
					candidates.add(candidate);
			}
			if (!nm.hitEnd()) {
				String candidate = text.substring(nm.end()).trim().split("\\s+")[0];
				if (!Utils.isBlank(candidate))
					candidates.add(candidate);
			}

			CurrencyData cd = findCurrencyData(candidates);
			BigDecimal priceBd = toDecimal(nm.group());
			if (firstKnownPriceBd == null) {
				firstKnownPriceBd = priceBd;
			}

			if (cd != null) {
				Currency currency = new Currency();
				currency.setCodeISO(cd.code);
				currency.setSymbol(cd.symbolNative);

				Price price = new Price();
				price.setCurrency(currency);

				if (priceBd != null) {
					price.setValue(priceBd);
					return price;
				}
			}
			lastNmEnd = nm.end();
		}

		if (firstKnownPriceBd != null) {
			Price price = new Price();
			price.setValue(firstKnownPriceBd);

			String currencyKeyDefault, currencyDataCompact;
			CurrencyData defaultCurrencyData;
			if ((currencyKeyDefault = getDefaultCurrencyKey()) != null
					&& (currencyDataCompact = sp.getString(currencyKeyDefault, null)) != null
					&& (defaultCurrencyData = fromCompact(currencyDataCompact, null)) != null) {
				Currency currency = new Currency();
				currency.setCodeISO(defaultCurrencyData.code);
				currency.setSymbol(defaultCurrencyData.symbolNative);
				price.setCurrency(currency);
			}

			return price;
		}
		return null;
	}

	private CurrencyData findCurrencyData(Set<String> candidates) {
		for (String candidate : candidates) {
			Log.i("ws", "findCurrencyData candidate: " + candidate);
		}
		String currencyKeyDefault = getDefaultCurrencyKey();
		CurrencyData cd = findCurrencyData(currencyKeyDefault, candidates);
		if (cd != null)
			return cd;

		cd = findCurrencyData(priorityCurrencyKeys, candidates);
		if (cd != null)
			return cd;

		for (String currencyKey : currencyKeys) {
			cd = findCurrencyData(currencyKey, candidates);
			if (cd != null)
				return cd;
		}

		return null;
	}

	private CurrencyData findCurrencyData(Set<String> currencyKeys, Set<String> candidates) {
		for(String currencyKey : currencyKeys) {
			CurrencyData cd = findCurrencyData(currencyKey, candidates);
			if(cd != null) return cd;
		}
		return null;
	}

	private CurrencyData findCurrencyData(String currencyKey, Set<String> candidates) {
		if (currencyKey == null)
			return null;

		String meta = sp.getString("meta." + currencyKey, null);
		if (meta == null)
			return null;

		Pattern metaPattern = Pattern.compile(meta);
		for (String candidate : candidates) {
			Matcher currencyMatcher = metaPattern.matcher(candidate);
			if (currencyMatcher.find()) {
				Log.i("ws", "Match [" + meta + "] against [" + candidate + "]");
				String currencyDataCompact = sp.getString(currencyKey, null);
				return fromCompact(currencyDataCompact, null);
			} else {
				Log.i("ws", "NO Match [" + meta + "] against [" + candidate + "]");
			}
		}

		return null;
	}
	
	public String getDefaultCurrencyKey() {
		return sp.getString(Config.SP_KEY_CURRENCY_KEY_DEFAULT, null);
	}

	public Price find(final String text) {
		String _text = text.trim().toUpperCase(locale);
		Price price = new Price();
		String numbers = null;
		Matcher alphaMatcher = alphaPattern.matcher(_text);
		if (alphaMatcher.find()) {
			String nonNumbers = alphaMatcher.group();
			if (alphaMatcher.start() == 0) {
				// USD 100000 [USD][10000]
				numbers = _text.substring(alphaMatcher.end());
			} else {
				// 10000 USD [10000][USD]
				numbers = _text.substring(alphaMatcher.regionStart(), alphaMatcher.start());
			}

			boolean found = false;
			String currencyKeyDefault = getDefaultCurrencyKey();
			if (currencyKeyDefault != null) {
				String meta = sp.getString("meta." + currencyKeyDefault, null);
				if (meta != null) {
					Pattern metaPattern = Pattern.compile(meta);
					Matcher currencyMatcher = metaPattern.matcher(nonNumbers);

					if (currencyMatcher.find()) {
						String currencyDataCompact = sp.getString(currencyKeyDefault, null);
						CurrencyData cd = fromCompact(currencyDataCompact, null);
						if (cd != null) {
							try {
								Currency currency = new Currency();
								currency.setCodeISO(cd.code);
								currency.setSymbol(cd.symbolNative);
								price.setCurrency(currency);

								Log.i("ws", "Using default currency: " + currency);

								found = true;
							} catch (Exception e) {
								Log.w("ws", e);
							}
						}
					}
				}
			}

			for (String currencyKey : currencyKeys) {
				if (found) {
					break;
				}
				String meta = sp.getString("meta." + currencyKey, null);
				if (meta != null) {
					Pattern metaPattern = Pattern.compile(meta);
					Matcher currencyMatcher = metaPattern.matcher(nonNumbers);
					if (currencyMatcher.find()) {
						String currencyDataCompact = sp.getString(currencyKey, null);
						CurrencyData cd = fromCompact(currencyDataCompact, null);
						if (cd != null) {
							try {
								Currency currency = new Currency();
								currency.setCodeISO(cd.code);
								currency.setSymbol(cd.symbolNative);
								price.setCurrency(currency);

								Log.i("ws", "Currency found: " + currency);

								found = true;
							} catch (Exception e) {
								Log.w("ws", e);
							}
						}
					}
				}
			}
		}

		if (numbers != null) {
			BigDecimal priceBd = toDecimal(numbers);
			if (priceBd != null) {
				price.setValue(priceBd);
				return price;
			}
		}

		return null;
	}

	private static BigDecimal toDecimal(final String priceRaw) {
		final String _priceRaw = whitespacePattern.matcher(priceRaw).replaceAll("");
		Matcher numbersMatcher = numbersPattern.matcher(_priceRaw);
		if (numbersMatcher.find()) {
			String priceCandidate = numbersMatcher.group();
			int commaLastPos = priceCandidate.lastIndexOf(',');
			int dotLastPos = priceCandidate.lastIndexOf('.');
			int fractionSepPos = commaLastPos > dotLastPos ? commaLastPos : dotLastPos;
			int fractionLength = priceCandidate.length() - fractionSepPos;
			if (fractionLength <= 3 && fractionSepPos != -1) {
				String left = priceCandidate.substring(0, fractionSepPos);
				String right = priceCandidate.substring(fractionSepPos + 1);
				left = nonAlphaPattern.matcher(left).replaceAll("");

				String price = left + "." + right;
				return new BigDecimal(price);
			} else {
				String price = nonAlphaPattern.matcher(priceCandidate).replaceAll("");
				return new BigDecimal(price);
			}
		}

		return null;
	}

	private static Map<String, CurrencyData> populate(Context context) throws IOException {
		InputStream is = context.getAssets().open("currencies.json");
		InputStreamReader isr = new InputStreamReader(is, "UTF-8");
		try {
			Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
			Type type = new TypeToken<Map<String, CurrencyData>>() {
			}.getType();
			return gson.fromJson(isr, type);
		} finally {
			try {
				isr.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static Locale locale = Locale.ROOT;
	private static String compactFormat = "%s___%s___%s___%s___%s";

	private static String toCompact(CurrencyData cd) {

		String compact = String.format(compactFormat, cd.code, cd.name, cd.namePlural, cd.symbol, cd.symbolNative);
		return compact;
	}

	private static CurrencyData fromCompact(String compact, CurrencyData defaultValue) {
		if (compact == null)
			return defaultValue;
		String[] split = compact.split("___");
		CurrencyData cd = new CurrencyData();
		cd.setCode(split[0]);
		cd.setName(split[1]);
		cd.setNamePlural(split[2]);
		cd.setSymbol(split[3]);
		cd.setSymbolNative(split[4]);
		return cd;
	}

}
