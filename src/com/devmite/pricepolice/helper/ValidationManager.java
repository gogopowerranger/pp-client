package com.devmite.pricepolice.helper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.helper.SessionManager.CredentialsException;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PinResponse;
import com.devmite.pricepolice.rest.ResponseHandler;
import com.devmite.pricepolice.rest.ResponseHandlerWithCleanup;

public class ValidationManager<C extends Context> {

	private static AtomicBoolean displayingAlert = new AtomicBoolean(false);

	private final Map<Integer, String> errorMap;
	private final C context;

	public ValidationManager(C context) {
		this.errorMap = new HashMap<Integer, String>();
		this.context = context;
	}

	public void addError(int errorId, String message) {
		this.errorMap.put(errorId, message);
	}

	public void handleError(final CredentialsException ce) {
		addError(ce.hashCode(), ce.getMessage());
		displayErrors(new Runnable() {
			@Override
			public void run() {
				ValidationManager.this.handleCredentialsError();
			}
		});
	}

	private void handleCredentialsError() {
		if (context instanceof Activity) {
			ProgressDialog pg = new ProgressDialog(context);
			pg.setCancelable(false);
			pg.setCanceledOnTouchOutside(false);
			pg.setMessage(context.getString(R.string.dialog_please_wait));
			pg.show();
		}
		SessionManager.getInstance(context).logout2();
	}

	public void handleError(final RetrofitError re) {
		Log.w("ws", re);
		try {
			switch (re.getKind()) {
			case HTTP:
				CommonResponse er = (CommonResponse) re.getBodyAs(CommonResponse.class);
				Log.w("ws", er.toString());
				switch (er.getInternalCode()) {
				case 8:
					addError(er.getInternalCode(), "Unexpected error occurred");
					break;
				default:
					addError(er.getInternalCode(), er.getMessage());
					break;

				}
				
				if (re.getResponse().getStatus() == 401) {
					displayErrors(new Runnable() {

						@Override
						public void run() {
							ValidationManager.this.handleCredentialsError();
						}
					});
				} else {
					displayErrors();
				}
				break;
			case NETWORK:
				addError(Integer.MIN_VALUE, "Network unreachable");
				displayErrors();
				break;
			default:
				addError(Integer.MIN_VALUE, "Unexpected error occurred");
				displayErrors();
				break;
			}
		} catch (Exception e) {
			Log.e("ws", re.getMessage(), e);
			addError(Integer.MIN_VALUE, "Unexpected error occurred");
			displayErrors();
		}
	}

	public boolean displayErrors() {
		return this.displayErrors(null);
	}

	public boolean displayErrors(final Runnable additionalDialogPositiveButtonOnClickListener) {
		if (!errorMap.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			Iterator<String> messages = errorMap.values().iterator();
			while (messages.hasNext()) {
				sb.append(messages.next());
				if (messages.hasNext()) {
					sb.append("\n");
				}
			}

			if (context instanceof Activity) {
				boolean ok = displayingAlert.compareAndSet(false, true);
				if (ok) {
					new AlertDialog.Builder(this.context).setMessage(sb.toString())
							.setCancelable(true)
							.setPositiveButton("OK", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
									if (additionalDialogPositiveButtonOnClickListener != null) {
										additionalDialogPositiveButtonOnClickListener.run();
									}
									displayingAlert.set(false);
								}
							}).create().show();
				}
			} else {
				if (additionalDialogPositiveButtonOnClickListener != null) {
					additionalDialogPositiveButtonOnClickListener.run();
				}
			}
			errorMap.clear();

			return false;
		} else {
			return true;
		}
	}

	public <T> HandledErrorCallback<T> getHandledErrorCallback(ResponseHandler<T> responseHandler) {
		return new HandledErrorCallback<T>(responseHandler);
	}

	protected class HandledErrorCallback<T> implements Callback<T> {

		private final ResponseHandler<T> responseHandler;

		public HandledErrorCallback(ResponseHandler<T> responseHandler) {
			this.responseHandler = responseHandler;
		}

		@Override
		public void failure(RetrofitError re) {
			handleError(re);
			if (this.responseHandler instanceof ResponseHandlerWithCleanup) {
				try {
					ResponseHandlerWithCleanup<T> cleanup = (ResponseHandlerWithCleanup<T>) this.responseHandler;
					cleanup.cleanup(false);
				} catch (Exception e) {
					Log.w("ws", e);
				}
			}
		}

		@Override
		public void success(T br, Response r) {
			try {
				this.responseHandler.handle(br);
			} catch (Exception e) {
				Log.w("ws", r.toString(), e);
			} finally {
				if (this.responseHandler instanceof ResponseHandlerWithCleanup) {
					try {
						ResponseHandlerWithCleanup<T> cleanup = (ResponseHandlerWithCleanup<T>) this.responseHandler;
						cleanup.cleanup(true);
					} catch (Exception e) {
						Log.w("ws", e);
					}
				}
			}
		}
	}

	public boolean isOwned(PinResponse pd) {
		long userId = SessionManager.getInstance(context).getUserId();
		return userId == pd.getUserId();
	}

}
