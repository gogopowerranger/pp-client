package com.devmite.pricepolice.helper;

import java.io.IOException;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.devmite.pricepolice.R;
import com.devmite.pricepolice.app.App;
import com.devmite.pricepolice.exception.NoSupportException;
import com.devmite.pricepolice.model.http.BaseRespone;
import com.devmite.pricepolice.model.http.CommonResponse;
import com.devmite.pricepolice.model.http.PushRegistrationRequest;
import com.devmite.pricepolice.model.http.PushTestRequest;
import com.devmite.pricepolice.util.IntraBroadcastCode;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmRegistrationService extends IntentService {
	
	public static class Receiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			SharedPreferences prefs = Config.getSharedPreferences(context);
			Log.i("ws", "Broadcast received");
			String action = intent.getAction();
			if (!action.equals(Config.INTENT_BROADCAST_ACTION)) {
				return;
			}
			NotificationManager nm = (NotificationManager) App.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
			SharedPreferences.Editor prefsEditor = prefs.edit();

			int code = intent.getIntExtra(Config.INTENT_KEY_BROADCAST_EXTRA, 0);
			IntraBroadcastCode ibc = IntraBroadcastCode.fromCode(code);
			Log.i(Config.INTENT_BROADCAST_ACTION, "Received: " + ibc.name());
			switch (ibc) {
			case GCM_UNSUPPORTED_RECOVERABLE:
				break;
			case GCM_UNSUPPORTED_FIRST_CHECK:
				Notification n = new Notification.Builder(context)
						.setContentTitle("No Google Play Services, limited functionality")
						.setContentText("You can still use the app, but not receive notifications on price updates and other events.")
						.setSmallIcon(R.drawable.ic_launcher)
						.setAutoCancel(true).getNotification();
				nm.notify(0, n);
				prefsEditor.putLong(Config.SP_KEY_GCM_UNSUPPORTED_LAST_NOTIFICATION_TS, System.currentTimeMillis());
				break;
			case GCM_UNSUPPORTED:
				long lastTs = prefs.getLong(Config.SP_KEY_GCM_UNSUPPORTED_LAST_NOTIFICATION_TS, 0L);
				Calendar cal = Calendar.getInstance();
				Calendar calClone = (Calendar) cal.clone();
				calClone.roll(Calendar.DATE, -7);
				long minimumTs = calClone.getTimeInMillis();
				if (minimumTs < lastTs) {
					prefsEditor.putLong(Config.SP_KEY_GCM_UNSUPPORTED_LAST_NOTIFICATION_TS, System.currentTimeMillis());
					n = new Notification.Builder(context)
							.setContentTitle("No Google Play Services, limited functionality")
							.setContentText("You can still use the app, but not receive notifications on price updates and other events.")
							.setSmallIcon(R.drawable.ic_launcher)
							.setAutoCancel(true).getNotification();
					nm.notify(0, n);
				}
				break;
			case GCM_REGISTRATION_FAILURE:
				int count = prefs.getInt(Config.SP_KEY_GCM_REGISTRATION_FAILURE_COUNT, 0) + 1;
				prefsEditor.putInt(Config.SP_KEY_GCM_REGISTRATION_FAILURE_COUNT, count);
				break;
			case GCM_REGISTERED:
				prefsEditor.putInt(Config.SP_KEY_GCM_REGISTRATION_FAILURE_COUNT, 0);
				break;
			}

			prefsEditor.commit();
		}
	}

	private SharedPreferences prefs;
	public GcmRegistrationService() {
		super("GcmRegistrationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i("ws", "Handling intent: " + intent);
		prefs = Config.getSharedPreferences(this);
		setupGcm();
	}

	public void setupGcm() {
		try {
			boolean supported = checkPlayServices();
			if (supported) {
				String regId = getRegistrationId();
				if (regId.isEmpty()) {
					register();
				} else {
					String regIdServer = prefs.getString(Config.SP_KEY_GCM_REG_ID_SERVER, "");
					if(!regId.equals(regIdServer)) {
						pushSet(regId);
					} else {
						PushTestRequest ptr = new PushTestRequest();
						ptr.setMsg("Hello Notifications!");
						App.getRestClient().getAPIService().pushTest(ptr, new Callback<BaseRespone<Object>>() {
							
							@Override
							public void success(BaseRespone<Object> arg0, Response arg1) {
								Log.i("ws", arg0.getData().toString());
							}
							
							@Override
							public void failure(RetrofitError arg0) {
								Log.w("ws", arg0);
							}
						});
					}
				}
			} else {
				sendBroadcast(IntraBroadcastCode.GCM_UNSUPPORTED_RECOVERABLE);
			}
		} catch (NoSupportException e) {
			sendBroadcast(e.getIbc());
		}
	}

	public boolean checkPlayServices() throws NoSupportException {
		boolean permanentlyUnsupported = prefs.getBoolean(Config.SP_KEY_GCM_UNSUPPORTED, false);
		if (permanentlyUnsupported) {
			throw new NoSupportException("This device is not supported", IntraBroadcastCode.GCM_UNSUPPORTED);
		}

		int registrationFailureCount = prefs.getInt(Config.SP_KEY_GCM_REGISTRATION_FAILURE_COUNT, 0);
		if (registrationFailureCount > 20) {
			return false;
		}

		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				Log.i("ws", "User doesn't have Google Play Services, but may get it somehow");
			} else {
				Log.i("ws", "This device is not supported.");
				prefs.edit().putBoolean(Config.SP_KEY_GCM_UNSUPPORTED, true).commit();

				throw new NoSupportException("This device is not supported", IntraBroadcastCode.GCM_UNSUPPORTED_FIRST_CHECK);
			}
			return false;
		}
		return true;
	}

	private void register() {
		String msg = "";
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
			final String regId = gcm.register(Config.PROJECT_NUMBER);
			msg = "Device registered, registration ID=" + regId;

			// You should send the registration ID to your server over
			// HTTP, so it
			// can use GCM/HTTP or CCS to send messages to your app.
			// sendRegistrationIdToBackend();
			
			pushSet(regId);
			
			// For this demo: we don't need to send it because the
			// device will send
			// upstream messages to a server that echo back the message
			// using the
			// 'from' address in the message.

			// Persist the regID - no need to register again.
			// storeRegistrationId(context, regid);
			storeRegistrationId(regId);

			sendBroadcast(IntraBroadcastCode.GCM_REGISTERED);
		} catch (IOException ex) {
			msg = "Error :" + ex.getMessage();
			sendBroadcast(IntraBroadcastCode.GCM_REGISTRATION_FAILURE);
		}
	}

	private void sendBroadcast(IntraBroadcastCode ibc) {
		Log.i("ws", "Sending broadcast: " + ibc.name());
		Intent intent = new Intent(Config.INTENT_BROADCAST_ACTION);
		intent.putExtra(Config.INTENT_KEY_BROADCAST_EXTRA, ibc.getCode());
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	private void storeRegistrationId(String regId) {
		int appVersion = getAppVersion();
		Log.i("ws", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Config.SP_KEY_GCM_REG_ID, regId);
		editor.putInt(Config.SP_KEY_GCM_APP_VERSION, appVersion);
		editor.commit();
	}
	
	private void pushSet(final String regId) {
		PushRegistrationRequest prr = new PushRegistrationRequest();
		prr.setRegId(regId);
		App.getRestClient().getAPIService().pushSet(prr, new Callback<CommonResponse>() {
			
			@Override
			public void success(CommonResponse cr, Response r) {
				prefs.edit().putString(Config.SP_KEY_GCM_REG_ID_SERVER, regId).commit();
			}
			
			@Override
			public void failure(RetrofitError re) {
				Log.w("ws", re);
				sendBroadcast(IntraBroadcastCode.GCM_REGISTRATION_SERVER_FAILURE);
			}
		});
	}

	private String getRegistrationId() {
		String registrationId = prefs.getString(Config.SP_KEY_GCM_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("ws", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Config.SP_KEY_GCM_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion();
		if (registeredVersion != currentVersion) {
			Log.i("ws", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private int getAppVersion() {
		try {
			PackageInfo packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

}
