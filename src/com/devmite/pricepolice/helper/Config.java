package com.devmite.pricepolice.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.devmite.pricepolice.model.Link;

public class Config {
	public static final String PROJECT_NUMBER = "328894234000";
	public static final long SPLASH_SLEEP_TIME = 1000; // in miliseconds

	public static final int API_LOGIN = 1001;
	public static final int API_REGISTER = 1002;
	public static final int API_UPDATE_PASSWORD = 1003;
	public static final int API_GET_PRIMARY_BOARD = 2001;
	public static final int API_GET_BOARD = 2002;
	public static final int API_GET_ALL_BOARDS = 2003;
	public static final int API_ADD_BOARD = 2004;
	public static final int API_UPDATE_BOARD = 2005;
	public static final int API_REMOVE_BOARD = 2006;

	public final static String KEY_CONTENT_TYPE = "Content-Type";
	public final static String KEY_ACCEPT = "Accept";
	public final static String VALUE_APPLICATION_JSON = "application/json";

	public static String PACKAGE_NAME_SUFFIX = "com.devmite";

	public static final String GENERAL_ERROR_MESSAGE = "I'm sorry. We're experiencing problem processing your request.\nPlease try again later.";

	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final int LIMIT_ATTEMPT_GCM_SETUP = 3;
	public static final long LIMIT_ELAPSED_SINCE_LAST_UPDATE = 1000 * 30;
	public static final String USER_AGENT = "Chrome 27/Android: Mozilla/5.0 (Linux; Android 4.2.2; Nexus 4 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.90 Mobile Safari/537.";
	public static final String USER_AGENT_DESKTOP = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; ASU2JS; rv:11.0) like Gecko";
	public static final int IMAGE_COMPRESSION_LEVEL = 85;

	public static final int[] UPDATE_VALUES = {
			1, 12, 24, 48, 168, 336, 672
	};

	public static enum RequiresDesktop {
		BHINNEKA(true, "bhinneka.com"),
		QOO10(true, "qoo10."),
		RAKUTEN_ID(true, "rakuten.co.id", new LinkFactory() {
			public Link modify(Link link) {
				link.setUserAgent(Config.USER_AGENT_DESKTOP);
				if (link.getUrl().contains("?")) {
					link.setUrl(link.getUrl() + "&ua=pc");
				} else {
					link.setUrl(link.getUrl() + "?ua=pc");
				}
				return link;
			};
		}),
		YANDEX(true, "market.yandex.ru"),
		STEAM_STORE(false, "store.steampowered.com", new LinkFactory() {
			@Override
			public Link modify(Link link) {
				if (!link.getUrl().contains("?cc=")) {
					if (link.getUrl().contains("?"))
						link.setUrl(link.getUrl() + "&cc=us");
					else
						link.setUrl(link.getUrl() + "?cc=us");
				}
				return link;
			}
		});

		private static interface LinkFactory {
			Link modify(Link link);
		}

		private final boolean requiresDesktop;
		private final String urlMatcher;
		private final LinkFactory linkFactory;

		private RequiresDesktop(boolean requiresDesktop, String urlMatcher, LinkFactory linkFactory) {
			this.requiresDesktop = requiresDesktop;
			this.urlMatcher = urlMatcher;
			this.linkFactory = linkFactory;
		}

		private RequiresDesktop(boolean requiresDesktop, String urlMatcher) {
			this.requiresDesktop = requiresDesktop;
			this.urlMatcher = urlMatcher;
			this.linkFactory = new LinkFactory() {
				@Override
				public Link modify(Link link) {
					link.setUserAgent(Config.USER_AGENT_DESKTOP);
					return link;
				}
			};
		}

		public LinkFactory getLinkFactory() {
			return linkFactory;
		}

		public String getUrlMatcher() {
			return urlMatcher;
		}

		public Link modify(Link link) {
			return linkFactory.modify(link);
		}

		public static RequiresDesktop fromUrl(String url) {
			for (RequiresDesktop rd : values()) {
				if (url.contains(rd.urlMatcher)) {
					return rd;
				}
			}
			return null;
		}
	}

	// private static final String IMAGE_CACHE_PATH = "/sdcard/";
	// private static final String IMAGE_AVATAR_CACHE_PATH = "/avatar/";
	// private static final String IMAGE_FORUM_CACHE_PATH = "/forum/";
	// private static final String IMAGE_OFFICIAL_FORUM_CACHE_PATH =
	// "/official/";
	// private static final String IMAGE_GROUP_CACHE_PATH = "/group/";

	// Shared Preferences keys
	public static String KEY_SHARED_PREF = "PPSharedPrefs";
	public static String KEY_PREF_PASSWORD = "password";
	public static String KEY_PREF_NOTIFICATION = "notification";
	public static String KEY_PREF_LANGUAGE = "language";
	public static String KEY_PREF_LANGUAGE_ID = "language_id";
	public static String KEY_PREF_PRIMARY_BOARD = "primary_board";

	public static String SP_KEY_GUIDE_VIEWED = "guide.viewed";
	public static String SP_KEY_GCM_REG_ID = "gcm.reg_id";
	public static String SP_KEY_GCM_REG_ID_SERVER = "gcm.reg_id.server";
	public static String SP_KEY_GCM_APP_VERSION = "gcm.app_version";
	public static String SP_KEY_GCM_REGISTRATION_FAILURE_COUNT = "gcm.registration.failure.count";
	public static String SP_KEY_GCM_UNSUPPORTED = "gcm.unsupported";
	public static String SP_KEY_GCM_UNSUPPORTED_LAST_NOTIFICATION_TS = "gcm.unsupported.last_ts";
	public static String SP_KEY_CREDENTIALS_TOKEN = "token";
	public static String SP_KEY_CREDENTIALS_USER_ID = "user_id";
	public static String SP_KEY_USER_USERNAME = "user.username";
	public static String SP_KEY_USER_EMAIL = "user.email";
	public static String SP_KEY_USER_CREATE_TS = "user.create_ts";
	public static String SP_KEY_USER_UPDATE_TS = "user.update_ts";
	public static String SP_KEY_USER_AVATAR = "user.avatar";
	public static String SP_KEY_CURRENCIES = "currencies";
	public static String SP_KEY_CURRENCY_KEY_DEFAULT = "currency.key.default";
	public static String SP_KEY_CURRENCY_KEYS = "currency.keys";
	public static String SP_KEY_CURRENCY_META = "currency.meta";

	public static String INTENT_BROADCAST_ACTION = "IntraBroadcastAction";
	public static String INTENT_KEY_BROADCAST_EXTRA = "broadcast";
	public static String INTENT_KEY_CALLER = "caller";

	// public static final String[][] CURRENCY = { { "$", "USD" }, { "$", "AUD"
	// },
	// { "$", "ARS" }, { "$", "CAD" }, { "$", "NZD" }, { "$", "SGD" },
	// { "$", "HKD" }, { "元", "CNY" }, { "kr", "DKK" }, { "€", "EUR" },
	// { "£", "GBP" }, { "¥", "JPY" }, { "Rp", "IDR" }, { "RM", "MYR" },
	// { "฿", "THB" }, { "₩", "KRW" }, { "Rs", "INR" } };
	//
	public static final SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(KEY_SHARED_PREF, Context.MODE_PRIVATE);
	}
}
