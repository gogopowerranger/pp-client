package com.devmite.pricepolice.helper;

import java.util.HashSet;
import java.util.Set;

public enum RefreshRegistry {
	INSTANCE;
	public static enum Type {
		FOLLOW, ADD, DELETE
	}

	private final Set<Type> classList = new HashSet<Type>();

	public boolean check(Type type) {
		return classList.contains(type);
	}
	
	public void remove(Type type) {
		classList.remove(type);
	}

	public void add(Type type) {
		classList.add(type);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Types in registry: ");
		for(Type t : classList) {
			sb.append("\n").append(t.name());
		}
		return sb.toString();
	}
}